<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\PrototypeIdHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Helpers\SpanishDateHelper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */




    public function index()
    {
        if(Auth::user()->type==3){
            $student = DB::table("student")
                ->select("student.*")->where("student.id_users","=",Auth::user()->id)->first();

            $institution = DB::table("institution")
                ->join("student","student.id_institution","=","institution.id")
                ->select("institution.*")->where("student.id_users","=",Auth::user()->id)->first();

            $count_test = DB::table("test_application")
                ->join("student","test_application.id_student","=","student.id")
                ->where("student.id_users","=",Auth::user()->id)
                ->where("test_application.state","!=",2)
                ->count();

            return view('home', compact('institution','student',"count_test"));

        }else if(Auth::user()->type==2){
            $admin = DB::table("institution_admin")->where("id_user","=",Auth::user()->id)->first();
            $examenes = DB::table("test")->where("id_institution","=",$admin->id_institution)->count();
            $estudiantes = DB::table("student")->where("id_institution","=",$admin->id_institution)->count();
            $institution = DB::table("institution")->where("id","=",$admin->id_institution)->first();

            $data_examenes = DB::table("test")->where("id_institution","=",$admin->id_institution)->get();

            foreach($data_examenes as $data){
                $data->asignados = DB::table("test_application")
                    ->where("test_application.id_test","=",$data->id)
                    ->count();
                
                $data->calificados = DB::table("test_application")
                    ->where("test_application.state","=",2)
                    ->where("test_application.id_test","=",$data->id)
                    ->count();
                
                $data->faltantes = DB::table("test_application")
                    ->where("test_application.state","!=",2)
                    ->where("test_application.id_test","=",$data->id)
                    ->count();
            }

            return view('home',compact("examenes","estudiantes","institution","data_examenes"));

        }else if(Auth::user()->type==1){
            $instituciones = DB::table("institution")->count();
            $data_instituciones = DB::table("institution")->get();
            $estudiantes = DB::table("student")->count();
            $admins = DB::table("institution_admin")->where("deleted","=",0)->count();
            
            foreach($data_instituciones as $data){
                $data->estudiantes = DB::table("student")->where("id_institution","=",$data->id)->count();
                $data->admins = DB::table("institution_admin")->where("id_institution","=",$data->id)->count();
                $data->examenes = DB::table("test")->where("id_institution","=",$data->id)->count();
            }
            return view('home',compact("instituciones","estudiantes","admins","data_instituciones"));
        }
    }
}
