<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Helpers\DeleteHelper;
use App\Helpers\SpanishDateHelper;
use Illuminate\Support\Facades\Input;
use App\Examenes;
use App\Competencias;
use App\Administradores_institucionales;
use App\User;
use App\Estudiante;
use App\Helpers\LogHelper;
use Alert;

class ResultsController extends Controller
{

    public function results_student(){
        $admin = DB::table("institution_admin")->where("id_user","=",Auth::user()->id)->first();

        $data_students = DB::table("student")
            ->join("test_application","student.id","=","test_application.id_student")
            ->join("test","test_application.id_test","=","test.id")
            ->select("student.full_name as student_name",
                    "student.CURP as matricula",
                    "test_application.id as application",
                    "test.description as test_name")
            ->where("test_application.state","=",2)
            ->where("student.id_institution","=",$admin->id_institution)->get();

        $students = array();
        foreach($data_students as $data){
            
            $deleted_str='<a id="downloadLink" href="'.route('reports.results_student', ['id' => $data->application]).'" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Generar PDF de resultados del estudiante '.$data->student_name.' para el examen '.$data->test_name.'" style="margin: 0px;"><span class="fas fa-file-pdf">&nbsp;Generar PDF</span></a>';
            
            /*if($data->deleted==0){
                $deleted_str='<a href="'.route('ies.edit', ['id' => $data->id]).'" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Editar institución educativa con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-ui-edit"></span></a>
                <button type="button" class="btn btn-danger"  style="margin: 0px;" id="eliminar" name="eliminar" data-toggle="tooltip"  data-placement="bottom" onclick="archiveFunctionCapturista(\''.route('ies.destroy', ['id' => $data->id]).'\')" title="Eliminar capturista con el id '.$data->id.'"><span class="icofont icofont-ui-delete"></span></button>';
            }else{
              $deleted_str ="";
              $deleted_style=' style="color:red"';
            }*/
          
            //<a href="'.route('ies.show', ['id' => $data->id]).'" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ver detalles de institución educativa con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-eye-alt"></span></a>
            array_push($students,
            [
                '<div>'.$data->matricula.'</div>',
                '<div>'.$data->student_name.'</div>',
                '<div>'.$data->test_name.'</div>',
                '<center>
                  '.$deleted_str.'
                </center>'
            ]);
        }
      
        return view("results.student")->with("students",$students);
    }

    public function results_test(){
        $admin = DB::table("institution_admin")->where("id_user","=",Auth::user()->id)->first();

        $data_test = DB::table("test")
            ->where("id_institution","=",$admin->id_institution)
            ->get();

        $test = array();
        foreach($data_test as $data){
            $data->presentaron = DB::table("test_application")->where("id_test","=",$data->id)->where("state","=",2)->count();

            $deleted_str='<a id="downloadLink" href="'.route('reports.results_test', ['id' => $data->id]).'" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Generar PDF de resultados del examen '.$data->description.'" style="margin: 0px;"><span class="fas fa-file-pdf">&nbsp;Generar PDF</span></a>';
            
            array_push($test,
            [
                '<div>'.$data->description.'</div>',
                '<div>'.$data->presentaron.'</div>',
                '<center>
                  '.$deleted_str.'
                </center>'
            ]);
        }
      
        return view("results.test")->with("test",$test);
    }
}
