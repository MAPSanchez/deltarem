<?php

namespace App\Http\Controllers;

use Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Helpers\DeleteHelper;
use App\Helpers\SpanishDateHelper;
use Illuminate\Support\Facades\Input;
use App\Examenes;
use App\Competencias;
use App\Institucion_educativa;
use App\Helpers\LogHelper;

class StudentTestController extends Controller
{
    public function checkValidityOfTime($id_area, $test_application_id){

        $area = DB::table("area")
            ->select(
                'area.description',
                'area.time'
            )
        ->where("area.id", $id_area)->first();

        $started_datetime = DB::table("area_application")
            ->select(
                'area_application.started_datetime'
            )
        ->where("area_application.id_area", $id_area)
        ->where("area_application.id_application", $test_application_id)->first()->started_datetime;

        $date_current_date=date('Y-m-d H:i:s');
        $date_started_date = date($started_datetime);

        if($date_started_date!=""){
            $limit_date =date("Y-m-d H:i:s", strtotime($date_started_date)+$area->time*60);

            $remaining = strtotime($limit_date)-strtotime($date_current_date);

            if($remaining<=0){
                $this->getScoreOfArea($test_application_id, $id_area);

                DB::table('area_application')
                ->where('id_application', $test_application_id)
                ->where('id_area', $id_area)
                ->update([
                    'state_id' => 2,
                ]);

                return false;
            }else{
                return true;
            }
        }

        return true;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $available_tests = DB::table("test_application")
            ->join('student','student.id','test_application.id_student')
            ->join('test', 'test.id', 'test_application.id_test')
            ->join('institution', 'institution.id','test.id_institution')
        ->select(
            "test_application.id as test_application_id",
            "test.description",
            'test.id as test_id',
            "test_application.state",
            'institution.name as institution_name',
            'institution.logo_img as institution_logo_img'
        )
        ->where('student.id_users',"=",Auth::user()->id)
            ->where(function ($query) {
                $query->whereNull('test_application.state')
                ->orWhere('test_application.state','=',1);
            })->get();


        for($i=0;$i<sizeof($available_tests);$i++){
            $available_tests[$i]->number_of_questions=DB::table("area")
                ->join('questions','questions.id_area','area.id')
            ->select(
                "questions.id"
            )
            ->where('area.id_test',"=",$available_tests[$i]->test_id)->count();

            #In minutes
            $areas=DB::table("area")
            ->select(
                "area.description",
                "area.time"
            )
            ->where('area.id_test',"=",$available_tests[$i]->test_id)->get();

            $total_time=0;
            $areas_str="";
            for($j=0;$j<sizeof($areas);$j++){
                $total_time += $areas[$j]->time;
                if($j==sizeof($areas)-1){
                    $areas_str=$areas_str." ".$areas[$j]->description.".";
                }else{
                    $areas_str=$areas_str." ".$areas[$j]->description.",";
                }
            }

            $hours = floor($total_time / 60);
            $min = $total_time - ($hours * 60);

            if($hours!=0 && $hours>1){
                $horas_str=$hours. " horas ";
            }else if($hours != 0){
                $horas_str=$hours. " hora ";
            }else{
                $horas_str="";
            }


            if($min!=0)
                $horas_str=$horas_str.$min." minuto";
                if($min>1)
                    $horas_str=$horas_str."s";

            $available_tests[$i]->areas=$areas_str;
            $available_tests[$i]->total_time =$horas_str;
        }

        return view("student_test.list", compact('available_tests'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function takeTest($test_application_id)
    {
        $test = DB::table("test_application")
            ->join('student','student.id','test_application.id_student')
            ->join('test', 'test.id', 'test_application.id_test')
            ->join('institution', 'institution.id','test.id_institution')
        ->select(
            "test_application.id as test_application_id",
            "test.description",
            "test_application.state",
            'test.id as test_id',
            'institution.name as institution_name'
        )
        ->where('student.id_users',"=",Auth::user()->id)
        ->where('test_application.id',"=", $test_application_id)->first();

        //dd($test);
        if($test->state==NULL){
            $test->number_of_questions=DB::table("area")
                ->join('questions','questions.id_area','area.id')
            ->select(
                "questions.id"
            )
            ->where('area.id_test',"=",$test->test_id)->count();

            #In minutes
            $areas=DB::table("area")
            ->select(
                "area.description",
                "area.time"
            )
            ->where('area.id_test',"=",$test->test_id)->get();

            $total_time=0;
            $areas_str="";
            for($j=0;$j<sizeof($areas);$j++){
                $total_time += $areas[$j]->time;
                if($j==sizeof($areas)-1){
                    $areas_str=$areas_str." ".$areas[$j]->description.".";
                }else{
                    $areas_str=$areas_str." ".$areas[$j]->description.",";
                }
            }

            $hours = floor($total_time / 60);
            $min = $total_time - ($hours * 60);

            if($hours!=0 && $hours>1){
                $horas_str=$hours. " horas ";
            }else if($hours != 0){
                $horas_str=$hours. " hora ";
            }else{
                $horas_str="";
            }

            if($min!=0)
                $horas_str=$horas_str.$min." minuto";
                if($min>1)
                    $horas_str=$horas_str."s";

            $test->areas=$areas_str;
            $test->total_time =$horas_str;

            return view("student_test.take_test", compact('test', 'test_application_id','areas'));
        }else{
            return($this->listTestAreas($test_application_id));
        }
    }

    public function listTestAreas($test_application_id){


        if(!$this->checkValidityOfExam($test_application_id)){
            //EXAM IS NOT VALID! User should not query this controller

            return view('errors.404');
        }


        $actual_test_application = DB::table("test_application")
            ->select("test_application.*")
        ->where('test_application.id',  $test_application_id)->first();

        if($actual_test_application->state==2){
            //EXAM IS FINISHED! User should not query this controller
            return view('errors.404');
        }

        $areas = DB::table("area")
            ->where('area.id_test',  $actual_test_application->id_test)->get();

        for($i=0;$i<sizeof($areas);$i++){
            $questions = DB::table("questions")
                ->select("questions.id")
            ->where('questions.id_area', '=', $areas[$i]->id)->get();
            $areas[$i]->questions = $questions;
        }


        if($actual_test_application->state==NULL){  //User is accessing this exam for the first time
            $validation = DB::table("answer_student")
                ->select("questions.id")
            ->where('answer_student.id_test_application', '=', $actual_test_application->id)->count();

            if($validation>0){
                //EXAM IS NOT VALID! User MUST not have answers in this case
                return view('errors.404');
            }else{
                $area_application_to_insert = [];
                for($i=0;$i<sizeof($areas);$i++){
                    $data_to_insert = [];

                    for($j=0;$j<sizeof($areas[$i]->questions);$j++){
                        array_push($data_to_insert, [
                                'id_question' => $areas[$i]->questions[$j]->id,
                                'id_test_application' => $actual_test_application->id
                        ]);
                    }

                    array_push($area_application_to_insert, [
                        'id_area' => $areas[$i]->id,
                        'id_application' => $actual_test_application->id
                    ]);

                    DB::table('answer_student')->insert($data_to_insert);
                }

                DB::table('area_application')->insert($area_application_to_insert);

                DB::table('test_application')
                ->where('id', $actual_test_application->id)
                ->update([
                    'state' => 1,
                ]);
            }

        }


        //User is resuming the exam
        $areas_data = array();

        //$this->listTestAreas($test_application_id)

        $finished_areas=0;

        for($i=0;$i<sizeof($areas);$i++){
            $area_state = DB::table("area_application")
                ->select("area_application.state_id")
            ->where('area_application.id_area', '=', $areas[$i]->id)
            ->where('area_application.id_application', '=', $actual_test_application->id)->first()->state_id;


            if($this->checkValidityOfTime($areas[$i]->id, $actual_test_application->id)==false){

                $area_state = DB::table("area_application")
                    ->select("area_application.state_id")
                ->where('area_application.id_area', '=', $areas[$i]->id)
                ->where('area_application.id_application', '=', $actual_test_application->id)->first()->state_id;
            }

            if($area_state==2){
                $finished_areas+=1;
            }

            $btn_state = 'href="'.route('student_test.areas.answer', ['id_application' => $actual_test_application->id, 'id_area' => $areas[$i]->id]).'" title="Responder esta competencia" ';
            if($area_state==NULL){
                $state_icon = 'Sin Comenzar &nbsp<i style="color:red" class="fas fa-times-circle"></i>';
            }elseif($area_state==1){
                $state_icon = 'Sin Finalizar &nbsp<i style="color:#fad42d" class="fas fa-exclamation-circle"></i>';
            }elseif($area_state==2){
                $state_icon = 'Terminado &nbsp<i style="color:green" class="fas fa-check-circle"></i>';
                $btn_state = 'disabled title="Ya ha contestado esta competencia"';
            }


            array_push($areas_data,
            [
                '<div>'.($i+1).'</div>',
                '<div>'.$areas[$i]->description.'</div>',
                '<div>'.sizeof($areas[$i]->questions).' preguntas </div>',
                '<div>'.$areas[$i]->time.' minutos</div>',
                '<div>'.$state_icon.'</div>',
                '<center>
                    <a '.$btn_state.' class="btn btn-green" data-toggle="tooltip" data-placement="bottom" style="margin: 0px; color:white"><span class="fas fa-arrow-right"></span></a>
                </center>'
            ]);
        }

        if($finished_areas==sizeof($areas)){

            $student_areas = DB::table("area_application")
                ->select(
                    "area_application.id",
                    "area_application.score"
                )
            ->where('area_application.id_application', '=', $actual_test_application->id)->get();

            $exam_score=0;
            for($i=0;$i<sizeof($student_areas);$i++){
                $exam_score+=$student_areas[$i]->score;
            }

            $exam_score=$exam_score/sizeof($student_areas);

            DB::table('test_application')
            ->where('id', $actual_test_application->id)
            ->update([
                'state' => 2,
                'score' => $exam_score,
            ]);

            Alert::success('Exitosamente', 'Examen Finalizado')->autoclose(6000);
            return redirect()->route('dashboard');
        }

        return view("student_test.areas.list", compact('areas_data'));
    }

    public function checkValidityOfExam($test_application_id){
        $test = DB::table("test_application")
            ->join('student','student.id','test_application.id_student')
            ->join('test', 'test.id', 'test_application.id_test')
            ->join('institution', 'institution.id','test.id_institution')
        ->select(
            "test_application.id as test_application_id"
        )
        ->where('student.id_users',"=",Auth::user()->id)
        ->where('test_application.id',"=", $test_application_id)->get();

        if(sizeof($test)>0){
            return true;
        }else{
            return false;
        }
    }

    public function answerTestArea($test_application_id, $id_area){
        $test_application = DB::table("test_application")
            ->select('test_application.id', 'test_application.state', 'test_application.id_test', 'test_application.id_student')
        ->where("test_application.id", $test_application_id)->first();

        if($test_application->state==2){
            return view('errors.404');
        }

        $test = DB::table("test")
            ->select('test.description','test.id_institution')
        ->where("test.id", $test_application->id_test)->first();

        $area = DB::table("area")
            ->select(
                'area.description',
                'area.time'
            )
        ->where("area.id", $id_area)->first();

        $institution = DB::table("institution")
            ->select(
                    'institution.name',
                    'institution.logo_img'
            )
        ->where("institution.id", $test->id_institution)->first();

        $student = DB::table("student")
            ->select(
                    'student.full_name',
                    'student.CURP'
            )
        ->where("student.id", $test_application->id_student)->first();

        $area_application = DB::table("area_application")
            ->select(
                'area_application.state_id',
                'area_application.started_datetime'
            )
        ->where("area_application.id_area", $id_area)
        ->where("area_application.id_application", $test_application_id)->first();

        if($area_application->state_id==2){
            return($this->listTestAreas($test_application_id));
        }

        if($area_application->started_datetime==""){
            DB::table('area_application')
            ->where("area_application.id_area", $id_area)
            ->where("area_application.id_application", $test_application_id)
            ->update([
                'started_datetime' => date('Y-m-d H:i:s'),
            ]);
        }

        $started_datetime = DB::table("area_application")
            ->select(
                'area_application.started_datetime'
            )
        ->where("area_application.id_area", $id_area)
        ->where("area_application.id_application", $test_application_id)->first()->started_datetime;

        if($this->checkValidityOfTime($id_area, $test_application_id)==false){
            Alert::warning('El tiempo disponbile para la competencia que estaba contestando se ha terminado, por favor, continue con su examen.', 'Su tiempo se ha acabado')->autoclose(6000);
            return redirect()->route("student_test.areas.list", ['id'=>$test_application_id]);
        }

        return view("student_test.areas.answer_questions", compact('test_application_id', 'id_area','test','area','institution','student','started_datetime'));
    }

    public function getQuestionInfo(Request $request){
        $id_question = $request->id_question;
        $id_area = $request->id_area;
        $test_application_id = $request->test_application_id;

        $question = DB::table("questions")
        ->select(
            "questions.id",
            "questions.question_txt",
            "questions.img_src"
        )
        ->where('questions.id',"=", $id_question)
        ->where('questions.id_area',"=", $id_area)->first();

        $answers = DB::table("questions_answers")
        ->select(
            "questions_answers.id",
            "questions_answers.num_answer",
            "questions_answers.text"
        )
        ->where('questions_answers.id_question',"=", $question->id)->get();

        $question->answers = $answers;

        $actual_student_answer = DB::table("answer_student")
        ->select(
            "answer_student.answer_letter"
        )
        ->where('answer_student.id_question',"=", $question->id)
        ->where('answer_student.id_test_application','=', $test_application_id)->first()->answer_letter;

        $question->actual_student_answer=$actual_student_answer;

        return response()->json(['response'=>$question]);
    }

    public function getQuestionsList(Request $request){

        $id_area = $request->id_area;
        $test_application_id = $request->test_application_id;

        $questions = DB::table("questions")
        ->select(
            "questions.id"
        )
        ->where('questions.id_area',"=", $id_area)->get();

        for($i=0;$i<sizeof($questions);$i++){
            $questions[$i]->actual_student_answer = DB::table("answer_student")
            ->select(
                "answer_student.answer_letter"
            )
            ->where('answer_student.id_question',"=", $questions[$i]->id)
            ->where('answer_student.id_test_application','=', $test_application_id)->first()->answer_letter;
        }

        return response()->json(['response'=>$questions, ]);
    }

    public function saveAnswer(Request $request){

        $id_area = $request->id_area;
        $actual_answer = $request->actual_answer;
        $id_question = $request->id_question;
        $test_application_id = $request->test_application_id;


        //Question validation --- Question exists
        $actual_question_id = DB::table("questions")
        ->select(
            "questions.id"
        )
        ->where('questions.id_area',"=", $id_area)
        ->where('questions.id',"=", $id_question)->first()->id;

        //Test application validation --- Only student can answer its questions
        $actual_test_application_id = DB::table("test_application")
        ->join('student','student.id','test_application.id_student')
            ->select("test_application.id")
        ->where('student.id_users', '=', Auth::user()->id)
        ->where('test_application.id',  $test_application_id)->first()->id;


        $actual_area_application = DB::table("area_application")
            ->select("area_application.state_id")
        ->where('area_application.id_area', '=', $id_area)
        ->where('area_application.id_application',  $actual_test_application_id)->first()->state_id;

        if($actual_area_application==NULL){
            DB::table('area_application')
                ->where('id_area', $id_area)
                ->where('id_application', $actual_test_application_id)
                ->update([
                    'state_id' => 1
                ]);
        }

        if($actual_area_application==2){
            return response()->json(['response'=>"error"]);
        }




        if($actual_question_id!=NULL && $actual_test_application_id!=NULL){
            if($this->checkValidityOfTime($id_area, $actual_test_application_id)==false){
                return response()->json(['response'=>"finished_time"]);
            }

            DB::table('answer_student')
                ->where('id_question', $actual_question_id)
                ->where('id_test_application', $actual_test_application_id)
                ->update([
                    'answer_letter' => $actual_answer
                ]);

            return response()->json(['response'=>1]);
        }else{
            return response()->json(['response'=>"error"]);
        }
    }

    public function getScoreOfArea($actual_test_application_id, $id_area){
        $actual_questions = DB::table("questions")
                ->select(
                    "questions.id",
                    "questions.correct_option"
                )
            ->where('questions.id_area',  $id_area)->get();

        $answer_student = DB::table("answer_student")
            ->join('questions','questions.id', 'answer_student.id_question')
                ->select(
                    "answer_student.id",
                    "answer_student.id_question",
                    "answer_student.answer_letter",
                    "questions.id_area"
                )
            ->where('questions.id_area','=',$id_area)
            ->where('answer_student.id_test_application',  $actual_test_application_id)->get();

        $correct_answers_count=0;
        for($i=0;$i<sizeof($actual_questions);$i++){
            for($j=0;$j<sizeof($answer_student);$j++){
                if($actual_questions[$i]->id == $answer_student[$j]->id_question){

                    if($actual_questions[$i]->correct_option == $answer_student[$j]->answer_letter){
                        $correct_answers_count+=1;
                    }

                    break;
                }
            }
        }

        $score=($correct_answers_count*100)/sizeof($actual_questions);

        DB::table('area_application')
            ->where('id_area', $id_area)
            ->where('id_application', $actual_test_application_id)
            ->update([
                'score' => $score
            ]);
    }

    public function endArea(Request $request){
        //****Pendiente validaciones
        $id_area = $request->id_area;
        $id_application = $request->test_application_id;

        //Validation
        $actual_test_application_id = DB::table("test_application")
        ->join('student','student.id','test_application.id_student')
            ->select("test_application.id")
        ->where('student.id_users', '=', Auth::user()->id)
        ->where('test_application.id',  $id_application)->first()->id;

        $this->getScoreOfArea($actual_test_application_id, $id_area);

        DB::table('area_application')
            ->where('id_area', $id_area)
            ->where('id_application', $actual_test_application_id)
            ->update([
                'state_id' => 2
            ]);

        return response()->json(['response'=>1]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function finishTime($id_test_application, $id_area){
        Alert::warning('El tiempo disponbile para la competencia que estaba contestando se ha terminado, por favor, continue con su examen.', 'Su tiempo se ha acabado')->autoclose(6000);
        //return($this->listTestAreas($id));

        $this->getScoreOfArea($id_test_application, $id_area);

        DB::table('area_application')
        ->where('id_application', $id_test_application)
        ->where('id_area', $id_area)
        ->update([
            'state_id' => 2,
        ]);

        return redirect()->route("student_test.areas.list", ['id'=>$id_test_application]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
