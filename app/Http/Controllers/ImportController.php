<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Estudiante;
use App\Test_aplication;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Alert;

header('Content-Type: text/html; charset=UTF-8');

class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $is_uploaded="false";
        return view('imports.list', compact('is_uploaded'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin = DB::table("institution_admin")->where("id_user","=",Auth::user()->id)->first();

        $actual_importation=$request->actual_importation;
        $importation_type=$request->importation_type;
        
        $ignore_passwords=$request->ignore_password_value;

        $row_result=[];

        $validator = Validator::make($request->all(), [
            'csv_input' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $file = $request->file('csv_input');
        //dd($file);
        $csvData = file_get_contents($file);
        
        if(mb_detect_encoding($csvData) == false){
            Alert::error('El archivo .csv que intenta importar no posee la codificación requerida UTF-8.','Error')->autoclose(7000);
            $is_uploaded="false";
            return view('imports.list', compact('is_uploaded'));
        }
        //dd($csvData);
        $rows = array_map('str_getcsv', explode("\n", $csvData));

        $headers = array_shift($rows);

        if (sizeof($rows[sizeof($rows)-1])==1) {
            array_pop($rows);
        }

        $original_headers = $headers;
        $correct_count = 0;

        //dd($rows);
        for ($i=0;$i<sizeof($headers);$i++) {
            $headers[$i]=strtolower($headers[$i]);

            switch ($actual_importation) {
                case 'Estudiantes':
                    switch ($headers[$i]) {
                        case 'nombre_completo':
                            $correct_count=$correct_count+1;
                            break;
                        case 'matricula':
                            $correct_count=$correct_count+1;
                            break;
                        case 'email':
                            $correct_count=$correct_count+1;
                            break;
                        case 'username':
                            $correct_count=$correct_count+1;
                            break;
                        case 'password':
                            $correct_count=$correct_count+1;
                            break;
                    }
                    break;
                case "Asignacion":
                    switch ($headers[$i]) {
                        case 'id_examen':
                            $correct_count=$correct_count+1;
                            break;
                        case 'matricula':
                            $correct_count=$correct_count+1;
                            break;
                    }
                    break;
            }
        }

        $csv_has_correct_format=false;
        switch ($actual_importation) {
            case 'Estudiantes':
                $table = "student";
                if($correct_count==5)
                    $csv_has_correct_format=true;
                break;
            case 'Asignacion':
                $table = "test_application";
                if($correct_count==2)
                    $csv_has_correct_format=true;
                break;
        }

        //dd($actual_importation,$importation_type,$ignore_passwords,$csvData,$rows,$headers,$csv_has_correct_format);

        if($csv_has_correct_format){
            foreach ($rows as $row) {

                for($x=0;$x<sizeof($original_headers);$x++){
                    $row_result_values[$i][$x]=[$row[$x],$original_headers[$x]];
                }

                switch ($actual_importation) {
                    case 'Estudiantes':
                        $nombre_estudiante = $row[0];
                        $curp_estudiante = $row[1];
                        $email_estudiante = $row[2];
                        $username_estudiante = $row[3];
                        $password_estudiante = $row[4];

                        if($curp_estudiante == ""){
                            array_push($row_result,["Error","Un estudiante no pudo ser registrado debido a: <br /> Matrícula requerida.",$row_result_values]);
                        }else{
                            if($nombre_estudiante == ""){
                                array_push($row_result,["Error","El estudiante con matrícula {$curp_estudiante} no pudo ser registrado debido a: <br /> Nombre completo requerido.",$row_result_values]);
                            }elseif($email_estudiante == ""){
                                array_push($row_result,["Error","El estudiante con matrícula {$curp_estudiante} no pudo ser registrado debido a: <br /> Correo electrónico requerido.",$row_result_values]);
                            }elseif($username_estudiante == ""){
                                array_push($row_result,["Error","El estudiante con matrícula {$curp_estudiante} no pudo ser registrado debido a: <br /> Nombre de usuario requerido.",$row_result_values]);
                            }elseif($password_estudiante == "") {
                                array_push($row_result,["Error","El estudiante con matrícula {$curp_estudiante} no pudo ser registrado debido a: <br /> Contraseña requerida.",$row_result_values]);
                            }else{
                                
                                $buscar_curp = DB::table("student")->where("CURP","=",$curp_estudiante)->first();
                                $buscar_email = DB::table("users")->where("email","=",$email_estudiante)->first();
                                $buscar_username = DB::table("users")->where("username","=",$username_estudiante)->first();

                                if($buscar_curp != null){
                                    array_push($row_result,["Error","El estudiante con matrícula {$curp_estudiante} no pudo ser registrado debido a: <br /> Ya existe un estudiante con la matrícula {$curp_estudiante}.",$row_result_values]);
                                }elseif($buscar_email != null){
                                    array_push($row_result,["Error","El estudiante con matrícula {$curp_estudiante} no pudo ser registrado debido a: <br /> Ya existe un estudiante con el email {$email_estudiante}.",$row_result_values]);
                                }elseif($buscar_username != null){
                                    array_push($row_result,["Error","El estudiante con matrícula {$curp_estudiante} no pudo ser registrado debido a: <br /> Ya existe un estudiante con el username {$username_estudiante}.",$row_result_values]);
                                }else{
                                    $registrar_usuario = new User;
                                    $registrar_usuario->username = $username_estudiante;
                                    $registrar_usuario->password = bcrypt($password_estudiante);
                                    $registrar_usuario->email = $email_estudiante;
                                    $registrar_usuario->type = 3;
                                    $registrar_usuario->save();

                                    $registrar_estudiante = new Estudiante;
                                    $registrar_estudiante->id_users = $registrar_usuario->id;
                                    $registrar_estudiante->full_name = $nombre_estudiante;
                                    $registrar_estudiante->CURP = $curp_estudiante;
                                    $registrar_estudiante->id_institution = $admin->id_institution;
                                    $registrar_estudiante->save();

                                    array_push($row_result,["Correct","El estudiante con matrícula {$curp_estudiante} fue registrado correctamente.",$row_result_values]);
                                }
                            }
                        }
                        break;
                    
                    case 'Asignacion':
                        $id_examen = $row[0];
                        $curp_estudiante = $row[1];

                        if($id_examen == "" && $curp_estudiante == ""){
                            array_push($row_result,["Error","Una asignación no fue realizada debido a: <br /> Identificador del examen y matrícula faltante.",$row_result_values]);
                        }elseif($id_examen != "" && $curp_estudiante == ""){
                            array_push($row_result,["Error","Una asignación no fue realizada debido a: <br /> Identificador del examen faltante.",$row_result_values]);
                        }elseif($id_examen == "" && $curp_estudiante != ""){
                            array_push($row_result,["Error","Una asignación no fue realizada debido a: <br /> Matrícula faltante.",$row_result_values]);
                        }else{
                            $buscar_curp = DB::table("student")->where("CURP","=",$curp_estudiante)->where("id_institution","=",$admin->id_institution)->first();
                            $buscar_examen = DB::table("test")->where("id","=",$id_examen)->where("id_institution","=",$admin->id_institution)->first();

                            if($buscar_curp == null){
                                array_push($row_result,["Error","Una asignación no fue realizada debido a: <br /> No existe un estudiante registrado con matrícula {$curp_estudiante}.",$row_result_values]);                                
                            }elseif($buscar_examen == null){
                                array_push($row_result,["Error","Una asignación no fue realizada debido a: <br /> No existe un examen registrado con el identificador {$id_examen}.",$row_result_values]);                                
                            }else{
                                $buscar_asignacion = DB::table("test_application")->where("id_test","=",$buscar_examen->id)->where("id_student","=",$buscar_curp->id)->first();
                                if($buscar_asignacion != null){
                                    array_push($row_result,["Error","Una asignación no fue realizada debido a: <br /> El estudiante con matrícula {$curp_estudiante} ya tiene asignado el examen con identificador {$id_examen}.",$row_result_values]);                                
                                }else{
                                    $registrar_asignacion = new Test_aplication;
                                    $registrar_asignacion->id_test = $buscar_examen->id;
                                    $registrar_asignacion->id_student = $buscar_curp->id;
                                    $registrar_asignacion->save();
                                    array_push($row_result,["Correct","Al estudiante con matrícula {$curp_estudiante} se le ha asignado el examen con el identificador {$id_examen}.",$row_result_values]);
                                }
                            }
                        }
                        break;
                }

            }
            $is_uploaded="true";
        }else{
            $is_uploaded="wrong";
        }
        return view('imports.list', compact('is_uploaded','row_result','actual_importation'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}


/*for ($i=0;$i<sizeof($headers);$i++) {

                    $nullable_col = DB::select("SELECT 1 as response FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'u454861895_edu_test' AND TABLE_NAME='".$table."' AND COLUMN_NAME= '".$headers[$i]."' AND IS_NULLABLE = 'YES'");

                    //Si tiene valor la columan es unica y se debe verificar su validez como unica
                    if ($nullable_col==null && $row[$i]=="") {
                        $is_valid_row=false;
                        $error=$error."* [VALOR REQUERIDO] El valor {$row[$i]} de la columna {$original_headers[$i]} necesita insertarse previamente.<br />";
                    }

                    if ($row[$i]!="") {
                        $primary_key = DB::select("SELECT 1 as response FROM information_schema.key_column_usage WHERE table_schema = 'u454861895_edu_test' AND constraint_name = 'PRIMARY' AND table_name = '".$table."' AND column_name='".$headers[$i]."'");

                        //La columna es unica y se debe verificar
                        if ($primary_key!=null) {
                            $is_valid_primary = DB::select("SELECT 1 as response FROM ".$table." WHERE ".$headers[$i]." = '".$row[$i]."'");

                            //Se encontro un registro con valor de columna unica, por lo tanto no se realiza el registro
                            if ($is_valid_primary!=null) {
                                $is_update_row=true;
                            }
                        }

                        //Se obtiene si una columna es de tipo unica(1=SI/NULL=NO)
                        $unique_col = DB::select("SELECT 1 as response FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'u454861895_edu_test' AND TABLE_NAME='".$table."' AND COLUMN_NAME = '".$headers[$i]."' AND COLUMN_KEY = 'UNI'");

                        //Si tiene valor la columan es unica y se debe verificar su validez como unica
                        if ($unique_col!=null) {

                            if($is_update_row){
                                //Se obtiene un registro en la tabla donde se quiera insertar
                                $registered_elements = sizeof(DB::select("SELECT 1 as response FROM ".$table." WHERE ".$headers[$i]." = '".$row[$i]."'"));
                                if($registered_elements==1)
                                    $is_unique=null;
                            }else{
                                //Se obtiene un registro en la tabla donde se quiera insertar
                                $is_unique = DB::select("SELECT 1 as response FROM ".$table." WHERE ".$headers[$i]." = '".$row[$i]."'");
                            }
                            //Se encontro un registro con valor de columna unica, por lo tanto no se realiza el registro
                            if ($is_unique!=null) {
                                $is_valid_row=false;
                                $error=$error."* [VALOR UNICO] El valor {$row[$i]} de la columna {$original_headers[$i]} no puede repetirse en los registros.<br />";
                            }
                        }

                        $foreign_key_ref = DB::select("SELECT REFERENCED_COLUMN_NAME, REFERENCED_TABLE_NAME FROM information_schema.key_column_usage WHERE referenced_table_name is not null AND TABLE_SCHEMA='u454861895_edu_test' AND TABLE_NAME='".$table."' AND COLUMN_NAME='".$headers[$i]."'");


                        if ($foreign_key_ref!=null) {

                            $foreign_key_exists = DB::select("SELECT 1 AS response FROM ".$foreign_key_ref[0]->REFERENCED_TABLE_NAME." WHERE ".$foreign_key_ref[0]->REFERENCED_COLUMN_NAME." = '".$row[$i]."'");

                            if ($foreign_key_exists==null) {
                                $is_valid_row=false;
                                $error=$error."* [FALTA REGISTRO PREVIO] El valor {$row[$i]} de la columna {$original_headers[$i]} requiere que el registro de la columna {$foreign_key_ref[0]->REFERENCED_COLUMN_NAME} de la tabla {$foreign_key_ref[0]->REFERENCED_TABLE_NAME} sea ingresado previamente.<br />";
                            }
                        }
                    }
                }*/