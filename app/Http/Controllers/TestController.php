<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Helpers\DeleteHelper;
use App\Helpers\SpanishDateHelper;
use Illuminate\Support\Facades\Input;
use App\Examenes;
use App\Competencias;
use App\Preguntas;
use App\Respuestas;
use App\Test_aplication;
use Alert;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_institucion = DB::table("institution_admin")->where("id_user","=",Auth::user()->id)->first();

        $data_examenes = DB::table("test")->where("id_institution","=",$id_institucion->id_institution)->get();
        $examenes = array();

        foreach($data_examenes as $data){
            $deleted_style="";

            /*if($data->deleted==0 ){
              $deleted_style='';
            }else{
              $deleted_style=' style="color:red"';
            }*/

            $deleted_str = "";

            array_push($examenes,
            [
                '<div'.$deleted_style.'>'.$data->id.'</div>',
                '<div'.$deleted_style.'>'.$data->description.'</div>',

                '<center>
                  <a href="'.route('test.show', ['id' => $data->id]).'" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ver competencias del examen con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-eye-alt"></span></a>
                  <a href="'.route('test.add_competencies', ['id' => $data->id]).'" class="btn btn-inverse" data-toggle="tooltip" data-placement="bottom" title="Agregar competencia al examen con el id '.$data->id.'" style="margin: 0px;"><span class="fas fa-plus"></span></a>
                  '.$deleted_str.'
                </center>'
            ]);
        }

        return view("test.list")->with("examenes",$examenes);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("test.create");
    }

    public function add_competencies(Examenes $examen)
    {
        $examen = DB::table("test")->where("id","=",$examen->id)->first();
        
        $competencias = DB::table("area")->where("id_test","=",$examen->id)->get();
        
        return view("test.add_competencies",compact("examen","competencias"));
    }

    public function edit_area(Competencias $competencia)
    {
        $examen = DB::table("test")->where("id","=",$competencia->id_test)->first();
        
        $competencias = DB::table("area")->where("id_test","=",$examen->id)->where("id","!=",$competencia->id)->get();
        return view("test.edit_area",compact("competencia","examen","competencias"));
    }

    public function add_question(Competencias $competencia)
    {                
        return view("test.add_question",compact("competencia"));
    }

    public function edit_question(Preguntas $pregunta)
    {
        $competencia = Competencias::find($pregunta->id_area);
        $respuestas = DB::table("questions_answers")->where("id_question","=",$pregunta->id)->get();
        
        return view("test.edit_question",compact("pregunta","competencia","respuestas"));
    }

    public function update_question(Request $request, Preguntas $pregunta)
    {
        $pregunta->question_txt = Input::get('pregunta');
        $pregunta->correct_option = Input::get('respuesta');
        
        //Imagen nueva(mandada atraves del file input)
        $image = Input::file('image');
        //Imagen actual(registrada en la base de datos)
        $image2 = $pregunta->img_src;
        if ($image!=null) {
          if($image2 != null){
            unlink(public_path()."/".$image2);
          }
            //Se realiza el almacenado de la nueva imagen(cargada en el file input)
            $path=Input::file('image')->store('/public/img_src_questions');
            //Se obtiene el nombre de la imagen
            $image_url = 'storage/img_src_questions/'.Input::file('image')->hashName();
          
            DB::update('UPDATE questions SET img_src = ? WHERE id = ?', [$image_url, $pregunta->id]);
        }
        $pregunta->update();
        
        DB::update("UPDATE questions_answers SET questions_answers.text = ? WHERE id_question = ? and num_answer = ?", [Input::get('a'), $pregunta->id, 0]);
        DB::update("UPDATE questions_answers SET questions_answers.text = ? WHERE id_question = ? and num_answer = ?", [Input::get('b'), $pregunta->id, 1]);
        DB::update("UPDATE questions_answers SET questions_answers.text = ? WHERE id_question = ? and num_answer = ?", [Input::get('c'), $pregunta->id, 2]);
        DB::update("UPDATE questions_answers SET questions_answers.text = ? WHERE id_question = ? and num_answer = ?", [Input::get('d'), $pregunta->id, 3]);


        Alert::success('Exitosamente', 'Pregunta Actualizada')->autoclose(6000);
        return redirect()->route('test.show_questions', ['id' => $pregunta->id_area]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_institucion = DB::table("institution_admin")->where("id_user","=",Auth::user()->id)->first();

        $examen = new Examenes;
        $examen->description = filter_var(Input::get('nombre'), FILTER_SANITIZE_STRING);
        $examen->id_institution = $id_institucion->id_institution;
        $examen->show_score = Input::get('mostrar_puntuacion');
        $examen->save();

        Alert::success('Exitosamente', 'Examen Creado')->autoclose(6000);
        return redirect()->route('test.list');
    }

    public function store_question(Request $request){

        $data = request()->validate([
            'pregunta' => 'required',
            'a' => 'required',
            'b' => 'required',
            'c' => 'required',
            'd' => 'required',
          ],[
            'pregunta.required' => ' * Este campo es obligatorio.',
            'a.required' => ' * Este campo es obligatorio.',
            'c.required' => ' * Este campo es obligatorio.',
            'b.required' => ' * Este campo es obligatorio.',
            'd.required' => ' * Este campo es obligatorio.',
          ]);
        

        $competencia = Competencias::find(Input::get('id_competencia'));
        
        $pregunta = new Preguntas;
        $pregunta->question_txt = Input::get('pregunta');

        $file = Input::file('file');
        if ($file!=null) {
            //Almacenando la imagen del alumno
            $path=$request->file('file')->store('/public/img_src_questions');
            $pregunta->img_src = 'storage/img_src_questions/'.$request->file('file')->hashName();
        }

        $pregunta->correct_option = Input::get('respuesta');
        $pregunta->id_area = $competencia->id;
        $pregunta->save();
        
        $a = new Respuestas;
        $a->id_question = $pregunta->id;
        $a->num_answer = 0;
        $a->text = Input::get('a');
        $a->save();

        $b = new Respuestas;
        $b->id_question = $pregunta->id;
        $b->num_answer = 1;
        $b->text = Input::get('b');
        $b->save();

        $c = new Respuestas;
        $c->id_question = $pregunta->id;
        $c->num_answer = 2;
        $c->text = Input::get('c');
        $c->save();

        $d = new Respuestas;
        $d->id_question = $pregunta->id;
        $d->num_answer = 3;
        $d->text = Input::get('d');
        $d->save();

        Alert::success('Exitosamente', 'Pregunta Creada')->autoclose(6000);
        return redirect()->route('test.show', ['id' => $competencia->id_test]);
    }

    public function store_competencia(Request $request)
    {
        $area = new Competencias;
        $area->description = filter_var(Input::get('description'), FILTER_SANITIZE_STRING);
        $area->time = filter_var(Input::get('time'), FILTER_SANITIZE_STRING);
        $area->id_test = filter_var(Input::get('id_examen'), FILTER_SANITIZE_STRING);
        $area->save();

        Alert::success('Exitosamente', 'Competencia Creada')->autoclose(6000);
        return redirect()->route('test.show', ['id' => $area->id_test]);
    }

    public function update_area(Request $request, Competencias $competencia)
    {
        $competencia->description = filter_var(Input::get('description'), FILTER_SANITIZE_STRING);
        $competencia->time = filter_var(Input::get('time'), FILTER_SANITIZE_STRING);
        $competencia->update();

        Alert::success('Exitosamente', 'Competencia Actualizada')->autoclose(6000);
        return redirect()->route('test.show', ['id' => $competencia->id_test]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $examen = Examenes::find($id);
        $competencias = DB::table("area")->where("id_test","=",$examen->id)->get();

        $tiempo_limite = 0;
        $cantidad_preguntas = 0;
        foreach($competencias as $competencia){
            $preguntas = DB::table("questions")->where("id_area","=",$competencia->id)->count();
            $tiempo_limite+= $competencia->time;
            $competencia->num_preguntas = $preguntas;
            $cantidad_preguntas += $preguntas;
        }

        $flag = DB::table("test_application")->where("id_test","=",$examen->id)->first();

        if($flag == null){
            $permitir_cambios = "si";
        }else{
            $permitir_cambios = "no";
        }


        return view("test.show")->with("competencias",$competencias)
                                ->with("examen",$examen)
                                ->with("tiempo_limite",$tiempo_limite)
                                ->with("permitir_cambios",$permitir_cambios)
                                ->with("cantidad_preguntas",$cantidad_preguntas);
    }

    public function show_questions($id)
    {
        $competencia = Competencias::find($id);
        $examen = Examenes::find($competencia->id_test);

        $preguntas = DB::table("questions")->where("id_area","=",$id)->get();
        
        foreach($preguntas as $pregunta){
            $respuestas = DB::table("questions_answers")->where("id_question","=",$pregunta->id)->get(); 
            $pregunta->respuestas = $respuestas;
        }


        $flag = DB::table("test_application")->where("id_test","=",$examen->id)->first();

        if($flag == null){
            $permitir_cambios = "si";
        }else{
            $permitir_cambios = "no";
        }
        
        return view("test.show_questions")->with("competencia",$competencia)
                                ->with("permitir_cambios",$permitir_cambios)
                                ->with("examen",$examen)
                                ->with("preguntas",$preguntas);
    }

    public function obtener_datos_para_asignar_examen_estudiante(){
     
        $admin = DB::table("institution_admin")->where("id_user","=",Auth::user()->id)->first();

        $students = DB::table("student")->where("id_institution","=",$admin->id_institution)->get();
        $tests = DB::table("test")->where("id_institution","=",$admin->id_institution)->get();
        return response()->json(['students'=>$students,'tests'=>$tests]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function store_asignacion(Request $request){

        $examen = DB::table("test")->where("id","=", Input::get('select_examen'))->first();
        $estudiante = DB::table("student")->where("id","=", Input::get('select_estudiante'))->first();

        $validar = DB::table("test_application")
            ->where("id_test","=",Input::get('select_examen'))
            ->where("id_student","=",Input::get('select_estudiante'))
            ->first();
        
        if($validar != null){
            Alert::error('El estudiante '.$estudiante->full_name.' ya posee asignado el examen '.$examen->description,'Proceso anulado')->autoclose(6000);
            return redirect()->route('assign.list');
        }

        $asignacion = new Test_aplication;
        $asignacion->id_test = Input::get('select_examen');
        $asignacion->id_student = Input::get('select_estudiante');
        $asignacion->save();

        Alert::success('Se ha asignado el examen '.$examen->description.' al estudiante '.$estudiante->full_name,'Exitosamente')->autoclose(6000);
        return redirect()->route('assign.list');

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function list_asignaciones(){
        $admin = DB::table("institution_admin")->where("id_user","=",Auth::user()->id)->first();

        $data_asignaciones = DB::table("test")
            ->join("test_application","test.id","=","test_application.id_test")
            ->join("student","student.id","test_application.id_student")
            ->where("test.id_institution","=",$admin->id_institution)
            ->select("test_application.id","test_application.state","student.CURP","student.full_name","test.description")
            ->get();
        $asignaciones = array();

        foreach($data_asignaciones as $data){
            $deleted_style="";

            /*if($data->deleted==0 ){
              $deleted_style='';
            }else{
              $deleted_style=' style="color:red"';
            }*/

            if($data->state == null){
                $deleted_str='<button type="button" class="btn btn-primary" style="margin: 0px;" data-toggle="tooltip" data-placement="bottom" onclick="assign_destroy(\''.route('assign.destroy', ['id' => $data->id]).'\')" title="Eliminar asignación"><span class="icofont icofont-ui-delete"></span></button>';
            }else{
                $deleted_str='<button type="button" class="btn btn-disabled" style="margin: 0px;" data-toggle="tooltip" data-placement="bottom" title="No es posible eliminar la asignación. Examen presentado."><span class="icofont icofont-ui-delete"></span></button>';
            }
            array_push($asignaciones,
            [
                '<div'.$deleted_style.'>'.$data->CURP.'</div>',
                '<div'.$deleted_style.'>'.$data->full_name.'</div>',
                '<div'.$deleted_style.'>'.$data->description.'</div>',
                '<center>'.$deleted_str.'</center>'
            ]);
        }

        
        return view("assign.list")->with("asignaciones",$asignaciones);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function destroy_assign(Test_aplication $test_application)
    {
        Test_aplication::destroy($test_application->id);
        //Alert::success('La pregunta ha sido eliminada','Exitosamente')->autoclose(6000);
        return redirect()->route('assign.list');
    }


    public function destroy_area(Competencias $competencia)
    {
        Competencias::destroy($competencia->id);
        //Alert::success('La pregunta ha sido eliminada','Exitosamente')->autoclose(6000);
        return redirect()->route('test.show', ['id' => $competencia->id]);
    }

    public function destroy_question(Preguntas $pregunta)
    {
        $id = $pregunta->id_area;
        Preguntas::destroy($pregunta->id);
        //Alert::success('La pregunta ha sido eliminada','Exitosamente')->autoclose(6000);
        return redirect()->route('test.show_questions', ['id' => $id]);
    }



}

