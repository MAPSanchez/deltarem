<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Helpers\DeleteHelper;
use App\Helpers\SpanishDateHelper;
use Illuminate\Support\Facades\Input;
use App\Examenes;
use App\Competencias;
use App\Institucion_educativa;
use App\Helpers\LogHelper;


use Alert;


class IesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_ies = DB::table("institution")->get();
        
        $ies = array();
        foreach($data_ies as $data){
            $deleted_style="";
            
            $deleted_str='<a href="'.route('ies.edit', ['id' => $data->id]).'" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Editar institución educativa con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-ui-edit"></span></a>';
            
            /*if($data->deleted==0){
                $deleted_str='<a href="'.route('ies.edit', ['id' => $data->id]).'" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Editar institución educativa con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-ui-edit"></span></a>
                <button type="button" class="btn btn-danger"  style="margin: 0px;" id="eliminar" name="eliminar" data-toggle="tooltip"  data-placement="bottom" onclick="archiveFunctionCapturista(\''.route('ies.destroy', ['id' => $data->id]).'\')" title="Eliminar capturista con el id '.$data->id.'"><span class="icofont icofont-ui-delete"></span></button>';
            }else{
              $deleted_str ="";
              $deleted_style=' style="color:red"';
            }*/
          
            array_push($ies,
            [
                '<div'.$deleted_style.'>'.$data->id.'</div>',
                '<div'.$deleted_style.'>'.$data->name.'</div>',
                '<center>
                  <a href="'.route('ies.show', ['id' => $data->id]).'" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ver detalles de institución educativa con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-eye-alt"></span></a>
                  
                  '.$deleted_str.'
                </center>'
            ]);
        }
      
        return view("ies.list")->with("ies",$ies);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("ies.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'ies' => 'required|max:255',
            'email' => 'required',
          ],[
            'ies.required' => ' * Este campo es obligatorio.',
            'ies.max' => ' * Este campo debe contener sólo 255 caracteres.',
            'email.required' => ' * Este campo es obligatorio.',
          ]);
        
        $ies = new Institucion_educativa;
        $ies->name = filter_var(Input::get('ies'), FILTER_SANITIZE_STRING);
        $ies->email = filter_var(Input::get('email'), FILTER_SANITIZE_STRING);

        //Se carga la imagen subida
        $file = Input::file('file');
        //Si se ingreso una imagen a guardar, entonces la guarda en storage en la carpeta de instituciones_educativas
        if ($file!=null) {
            //Almacenando la imagen del alumno
            $path=$request->file('file')->store('/public/instituciones_educativas');
            $ies->logo_img = 'storage/instituciones_educativas/'.$request->file('file')->hashName();
        }

        $ies->save();
        Alert::success('Exitosamente','Institución Educativa Registrada')->autoclose(4000);
  
        LogHelper::instance()->insertToLog(Auth::user()->id, 'added', $ies->id, "institución educativa");
        //insertToLog(Auth::user()->id, 'added', $subcomponent->id, "institución educativa");

        return redirect()->route('ies.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ies = DB::table("institution")->where("id","=",$id)->first();
        return view("ies.show",compact("ies"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Institucion_educativa $ies)
    {
        return view("ies.edit",compact("ies"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Institucion_educativa $ies)
    {
        $data = request()->validate([
            'ies' => 'required|max:255',
            'email' => 'required',
          ],[
            'ies.required' => ' * Este campo es obligatorio.',
            'ies.max' => ' * Este campo debe contener sólo 255 caracteres.',
            'email.required' => ' * Este campo es obligatorio.',
          ]);
        
        $ies->name = filter_var(Input::get('ies'), FILTER_SANITIZE_STRING);
        $ies->email = filter_var(Input::get('email'), FILTER_SANITIZE_STRING);

        //Imagen nueva(mandada atraves del file input)
          $image = Input::file('image');
          //Imagen actual(registrada en la base de datos)
          $image2 = $ies->logo_img;

          $ies->update();
          if ($image!=null) {
            if($image2 != null){
		
              		unlink(public_path()."/".$image2);
            }
              //Se realiza el almacenado de la nueva imagen(cargada en el file input)
              $path=Input::file('image')->store('/public/instituciones_educativas');
              //Se obtiene el nombre de la imagen
              $image_url = 'storage/instituciones_educativas/'.Input::file('image')->hashName();
              //Se realiza la nueva actualizacion al registro del usuario actual con el
              //nombre de la nueva imagen
            
              DB::update('UPDATE institution SET logo_img = ? WHERE id = ?', [$image_url, $ies->id]);
          }
          Alert::success('Exitosamente','Institución Educativa Modificada')->autoclose(4000);

          LogHelper::instance()->insertToLog(Auth::user()->id, 'updated', $ies->id, "institución educativa");

          return redirect()->route('ies.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
