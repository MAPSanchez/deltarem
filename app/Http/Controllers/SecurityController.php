<?php

namespace App\Http\Controllers;

use Alert;
use Illuminate\Http\Request;
use App\Helpers\DeleteHelper;
use App\Helpers\SpanishDateHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Libro;
use App\Helpers\LogHelper;

class SecurityController extends Controller
{
    public function index()
    {
        $data_libros = DB::table("libros")
          ->join("users","libros.id_user","=","users.id")
          ->select("libros.*","users.first_name","users.last_name","users.second_last_name")
          ->where('libros.estado','=',1)->get();

        $libros = array();

        foreach($data_libros as $data){
            $book_entries=DB::table("registros")
                ->select(
                    'id',
                    'libro',
                    'foja',
                    'registro',
                    'folio',
                    'nombre_egresado',
                    'fecha_examen',
                    'id_profesiones',
                    'fecha_registro',
                    'id_estatus'
                )
                ->where('registros.libro','=',$data->id)
            ->get();

            $found_error=false;
            for($i=0;$i<sizeof($book_entries);$i++){
                if($this->valid_hash($book_entries[$i]->id)==false){
                    $found_error=true;
                    break;
                }
            }

            if($found_error){
                $registros = DB::table("registros")->where("libro","=",$data->id)->count();
                $deleted_style="";
                $porcentaje = $registros * 100 / 1000;

                if($data->estado==2 ){
                    $deleted_str='
                    <button type="button" class="btn btn-inverse"  style="margin: 0px;" id="cambiar_estado" name="cambiar_estado" data-toggle="tooltip"  data-placement="bottom" onclick="revisar_libro_function('.$data->id.')" title=" Responder revisión de libro con el id '.$data->id.'"><span class="fas fa-edit"></span></button>';
                }else{
                  $deleted_str ="";
                }

                if($data->estado==0 ){
                  $estado = "Activo";
                  $deleted_style='';
                }else if($data->estado==1 ){
                  $estado = "Cerrado";
                  $deleted_style=' style="color:red"';
                }else{
                  $estado = "Revisión";
                  $deleted_style=' style="color:orange"';
                }

                array_push($libros,
                [
                    '<div'.$deleted_style.'>'.$data->id.'</div>',
                    '<div'.$deleted_style.'>'.$estado.'</div>',
                    '<div'.$deleted_style.'>'.$porcentaje.' %</div>',
                    '<div'.$deleted_style.'>'.$data->first_name." ".$data->last_name." ".$data->second_last_name.'</div>',
                    '<div'.$deleted_style.'>Registro Corrupto</div>',
                    '<center>
                      <a href="'.route('security.get_corrupt_registros', ['id' => $data->id]).'" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ver registros de libro con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-eye-alt"></span></a>
                      '.$deleted_str.'
                    </center>'
                ]);

            }

        }

        return view("security.list")->with("libros",$libros);
    }

    public function update_hashes_from_book($book_id){
        $registros=DB::table("registros")
            ->select(
                'id',
                'libro',
                'foja',
                'registro',
                'folio',
                'nombre_egresado',
                'fecha_examen',
                'id_profesiones',
                'fecha_registro',
                'id_estatus'
            )
            ->where('libro','=',$book_id)
            ->where('hash','=',null)
        ->get();

        for($i=0;$i<sizeof($registros);$i++){
            $actual_entry_str=$registros[$i]->id.
                $registros[$i]->libro.
                $registros[$i]->foja.
                $registros[$i]->registro.
                $registros[$i]->folio.
                $registros[$i]->nombre_egresado.
                $registros[$i]->fecha_examen.
                $registros[$i]->id_profesiones.
                //$registros[$i]->fecha_registro.
                $registros[$i]->id_estatus;

            DB::table('registros')
            ->where('id', $registros[$i]->id)
            ->update(['hash' => hash('sha256', $actual_entry_str)]);
        }
    }

    public function valid_hash($id){
        $entry=DB::table("registros")
            ->select(
                'id',
                'libro',
                'foja',
                'registro',
                'folio',
                'nombre_egresado',
                'fecha_examen',
                'id_profesiones',
                'fecha_registro',
                'id_estatus',
                'hash'
            )
            ->where('id','=',$id)
        ->first();

        $actual_entry_str=$entry->id.
            $entry->libro.
            $entry->foja.
            $entry->registro.
            $entry->folio.
            $entry->nombre_egresado.
            $entry->fecha_examen.
            $entry->id_profesiones.
            //$entry->fecha_registro.
            $entry->id_estatus;

        //dd($actual_entry_str);

        if(hash('sha256', $actual_entry_str)==$entry->hash)
            return true;
        else
            return false;
    }

    public function getCorruptRegistros($id){
        $libro = Libro::find($id);
        $this->update_hashes_from_book($id);
        $num_registros = DB::table("registros")->where("libro","=",$id)->count();
        $porcentaje = $num_registros * 100 / 1000;

        $prev_registros = DB::table("registros")
          ->join("profesiones","registros.id_profesiones","=","profesiones.id_profesion")
          ->join("instituciones_educativas","profesiones.id_ies","=","instituciones_educativas.id_ies")
          ->select("registros.id","foja","folio","nombre_egresado","fecha_examen","instituciones_educativas.nombre_ies","profesiones.profesion")->where("libro","=",$id)->get();

        $data_registros = array();
        for($i=0;$i<sizeof($prev_registros);$i++){
          if($this->valid_hash($prev_registros[$i]->id)==false){
              array_push($data_registros, $prev_registros[$i]);
          }
        }

        $registros = array();

        foreach($data_registros as $data){
            $fecha=SpanishDateHelper::instance()->getSpanishDate($data->fecha_examen);
            array_push($registros,
            [
                '<div style="color:red">'.$data->foja.'</div>',
                '<div style="color:red">'.$data->folio.'</div>',
                '<div style="color:red">'.$data->nombre_egresado.'</div>',
                '<div style="color:red">'.$fecha.'</div>',
                '<div style="color:red">'.$data->nombre_ies.' </div>',
                '<div style="color:red">'.$data->profesion.' </div>'
            ]);
        }

        return view("security.corrupt_registros_list")->with("registros",$registros)->with("id",$id)->with("porcentaje",$porcentaje)->with("libro",$libro);
    }

}
