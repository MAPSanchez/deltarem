<?php

namespace App\Http\Controllers;

use Alert;
use App\User;
use App\Libro;
use Illuminate\Http\Request;
use App\Helpers\DeleteHelper;
use App\Helpers\SpanishDateHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Helpers\LogHelper;

class ProfileController extends Controller
{
    public function showOwnProfile(){
        //Se obtiene el id del usuario con la sesion activa
        $user = DB::table('users')->where('id', '=', Auth::user()->id)->first();

        return view('profile.show', compact('user'));
    }
    
    public function editOwnProfile() {
        $user = User::find(Auth::user()->id);

        return view('profile.edit', compact('user'));
    }

    /**
     * Permite realizar la actualizacion del usuario en sesion
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updateOwnProfile(User $user){
        //Se obtiene el id del usuario en sesion
        $user_id = Auth::user()->id;
        $user = User::find(Auth::user()->id);

        //Validaciones a los datos de entrada
        $data = request()->validate([
            'email' => 'required|max:70',
            'username' => 'required|max:128',
        ], [
            'email.required' => ' * Este campo es obligatorio.',
            'email.max' => ' * Este campo debe contener sólo 70 caracteres.',
            'username.required' => ' * Este campo es obligatorio.',
            'username.max' => ' * Este campo debe contener sólo 128 caracteres.',
        ]);
        $user->username = filter_var(Input::get('username'), FILTER_SANITIZE_STRING);
        $user->email = filter_var(Input::get('email'), FILTER_SANITIZE_STRING);
    


        //Se actualiza el usuario
        $user->update();

        //Si el usuario cambia la contraseña, se actualiza, es caso contrario se queda como estaba guardada
        if (Input::get('password') != null) {
            $password = bcrypt(filter_var(Input::get('password'), FILTER_SANITIZE_STRING));
            DB::update('UPDATE users SET password = ? WHERE id = ?', [$password, $user_id]);
        }

        Alert::success('Exitosamente', 'Contraseña modificada');
        return redirect()->route('profile.show_own_profile');
    }

  
    
}
