<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Helpers\DeleteHelper;
use App\Helpers\SpanishDateHelper;
use Illuminate\Support\Facades\Input;
use App\Examenes;
use App\Competencias;
use App\Administradores_institucionales;
use App\User;
use App\Estudiante;
use App\Helpers\LogHelper;

use Alert;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_institucion = DB::table("institution_admin")->where("id_user","=",Auth::user()->id)->first();
        $data_students = DB::table("student")
            ->join("users","student.id_users","=","users.id")
            ->select("student.*","users.email","users.username")
            ->where("id_institution","=",$id_institucion->id_institution)->get();

        $students = array();
        foreach($data_students as $data){
            $deleted_style="";

            array_push($students,
            [
                '<div'.$deleted_style.'>'.$data->CURP.'</div>',
                '<div'.$deleted_style.'>'.$data->full_name.'</div>',
                '<div'.$deleted_style.'>'.$data->username.'</div>',
                '<div'.$deleted_style.'>'.$data->email.'</div>',
                '<center>
                    <a href="'.route('students.edit', ['id' => $data->id]).'" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Editar estudiante con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-ui-edit"></span></a>
                    <a href="'.route('students.destroy', ['id' => $data->id]).'"><button type="button" class="btn btn-danger"  style="margin: 0px;" id="eliminar" name="eliminar" data-toggle="tooltip"  data-placement="bottom" title="Eliminar el estudiante con el id '.$data->id.'"><span class="icofont icofont-ui-delete"></span></button></a>
                </center>'
            ]);
        }

        return view("students.list")->with("students",$students);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("students.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'email' => 'required|unique:users,email',
            'username' => 'required|unique:users,username',
            'nombre' => 'required',
            'curp' => 'required|unique:student,CURP',
            'password' => 'required',
          ],[
            'email.required' => ' * Este campo es obligatorio.',
            'email.unique' => ' * Este email ya existe.',
            'username.required' => ' * Este campo es obligatorio.',
            'username.unique' => ' * Este username ya existe.',
            'nombre.required' => ' * Este campo es obligatorio.',
            'curp.required' => ' * Este campo es obligatorio.',
            'curp.unique' => ' * Este username ya existe.',
            'password.required' => ' * Este campo es obligatorio.',
          ]);

        $student = new User;
        $student->username = filter_var(Input::get('username'), FILTER_SANITIZE_STRING);
        $student->password =  bcrypt(Input::get('password'));
        $student->email = filter_var(Input::get('email'), FILTER_SANITIZE_STRING);
        $student->type = 3;
        $student->save();

        $id_institucion = DB::table("institution_admin")->where("id_user","=",Auth::user()->id)->first();
        $student_user = new Estudiante;
        $student_user->full_name = filter_var(Input::get('nombre'), FILTER_SANITIZE_STRING);
        $student_user->CURP =  filter_var(Input::get('curp'), FILTER_SANITIZE_STRING);
        $student_user->id_users = $student->id;
        $student_user->id_institution = $id_institucion->id_institution;
        $student_user->save();

        Alert::success('Exitosamente', 'Estudiante Registrado');
        LogHelper::instance()->insertToLog(Auth::user()->id, 'added', $student_user->id, "estudiante");
        return redirect()->route('students.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Estudiante $student)
    {
        $data_student = DB::table("student")
            ->join("users","student.id_users","=","users.id")
            ->select("student.*","users.email","users.username")
            ->where("student.id","=",$student->id)->first();

        return view("students.edit",compact("data_student"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estudiante $student)
    {
        $user = User::find($student->id_users);

        $data = request()->validate([
            'email' => 'required|unique:users,email,'.$user->id,
            'username' => 'required|unique:users,username,'.$user->id,
            'nombre' => 'required',
            'curp' => 'required',
          ],[
            'email.required' => ' * Este campo es obligatorio.',
            'email.unique' => ' * Este email ya existe.',
            'username.required' => ' * Este campo es obligatorio.',
            'username.unique' => ' * Este username ya existe.',
            'nombre.required' => ' * Este campo es obligatorio.',
            'curp.required' => ' * Este campo es obligatorio.',
        ]);

        if(Input::get('password')!= null){
            $user->password =  bcrypt(Input::get('password'));
        }

        $user->username = filter_var(Input::get('username'), FILTER_SANITIZE_STRING);
        $user->email = filter_var(Input::get('email'), FILTER_SANITIZE_STRING);
        $user->update();

        $student->full_name = filter_var(Input::get('nombre'), FILTER_SANITIZE_STRING);
        $student->CURP =  filter_var(Input::get('curp'), FILTER_SANITIZE_STRING);
        $student->update();

        Alert::success('Exitosamente', 'Estudiante Modificado');
        LogHelper::instance()->insertToLog(Auth::user()->id, 'updated', $student->id, "estudiante");
        return redirect()->route('students.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function real_destroy($student_id)
    {
        $data_student = DB::table("student")
            ->join("users","student.id_users","=","users.id")
                ->select("student.*","users.id")
            ->where("student.id","=",$student_id)->first();

        $test_applications = DB::table("test_application")
                ->select("test_application.id")
            ->where("test_application.id_student","=",$student_id)
            ->where("test_application.state","!=", NULL)->get();

        if(sizeof($test_applications)==0){
            //can delete
        }
        dd($data_student, $test_applications);
        Alert::success('Exitosamente', 'Estudiante Eliminado');
        LogHelper::instance()->insertToLog(Auth::user()->id, 'removed', $student_id, "estudiante");
    }
}
