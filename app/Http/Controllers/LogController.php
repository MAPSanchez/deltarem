<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Alert;
use App\Helpers\DeleteHelper;
use App\User;

class LogController extends Controller{
    public function indexMovements()
    {
        $data_movimientos = DB::table('log')
            ->get();

        $movimientos = array();

        foreach($data_movimientos as $data){
            $deleted_style="";

            array_push($movimientos,
            [
                '<div'.$deleted_style.'>'.$data->id.'</div>',
                '<div'.$deleted_style.'>'.$data->message.'</div>',
                '<div'.$deleted_style.'>'.$data->date.'</div>',
                '<center>
                  <a href="'.route('movements.show', ['id' => $data->id]).'" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Ver detalles de movimiento con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-eye-alt"></span></a>
                </center>'
            ]);
    }

    return view('log.movementslist')
        ->with('movimientos',$movimientos)
        ->with('title', 'Historial de Movimientos');
    }

    public function showMovement($id)
    {
        $log = DB::select("SELECT * FROM log WHERE id = $id")[0];
        $user = User::find($log->user_id);
        return view('log.showMovement', compact('user','log'));
    }

    public function get_movements(Request $request){
        if($request->start_date==null && $request->end_date==null){
            $data_movimientos = DB::table('log')
                ->get();
        }else{
            $data_movimientos = DB::table('log')
                ->whereRaw("date BETWEEN CAST('".$request->start_date."' AS DATE) AND CAST('".$request->end_date."' AS DATE)")
                ->get();

        }

        $movimientos = array();

        foreach($data_movimientos as $data){
            $deleted_style="";

            array_push($movimientos,
            [
                '<div'.$deleted_style.'>'.$data->id.'</div>',
                '<div'.$deleted_style.'>'.$data->message.'</div>',
                '<div'.$deleted_style.'>'.$data->date.'</div>',
                '<center>
                  <a href="'.route('movements.show', ['id' => $data->id]).'" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Ver detalles de movimiento con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-eye-alt"></span></a>
                </center>'
            ]);
        }
        return response()->json(['response'=>$movimientos]);
    }
}
