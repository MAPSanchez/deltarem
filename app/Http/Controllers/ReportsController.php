<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Helpers\DeleteHelper;
use App\Helpers\SpanishDateHelper;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Libro;
use Alert;
use PDF;

class ReportsController extends Controller{

  public function results_student($id){
    
    $data = DB::table("test_application")
      ->join("test","test_application.id_test","=","test.id")
      ->join("institution","test.id_institution","=","institution.id")
      ->join("student","test_application.id_student","=","student.id")
      ->join("users","student.id_users","=","users.id")
      ->select("institution.logo_img as logo",
              "institution.name as escuela",
              "test.description as examen",
              "student.full_name",
              "student.CURP as matricula",
              "users.email",
              "test_application.score",
              "test_application.start_date_time")
      ->where("test_application.id","=",$id)
      ->first();

    $calificaciones = DB::table("area_application")
      ->join("area","area_application.id_area","=","area.id")
      ->select("area.description",
              "area_application.score")
              ->where("area_application.id_application","=",$id)
              ->get();

    $pdf = PDF::loadView("pdf.results_student",compact("data","calificaciones"))->setPaper('letter', 'portrait');

    setCookie("downloadStarted", 1, time() + 20, '/', "", false, false);
    return $pdf->download("pdf_resultados_".$data->full_name."_".$data->examen."_".date("d-m-Y-His").".pdf");
  }

  public function results_test($id){
    
    $data = DB::table("test_application")
      ->join("student","test_application.id_student","=","student.id")
      ->select("student.full_name",
              "test_application.score")
              ->where("test_application.state","=",2)
              ->where("test_application.id_test","=",$id)
              ->orderBy("test_application.score","desc")
              ->get();

    $examen = DB::table("test")
      ->join("institution","test.id_institution","=","institution.id")
      ->select("test.description","institution.name")
      ->where("test.id","=",$id)->first();

    $pdf = PDF::loadView("pdf.results_test",compact("data","examen"))->setPaper('letter', 'portrait');

    setCookie("downloadStarted", 1, time() + 20, '/', "", false, false);
    return $pdf->download("pdf_resultados_".$examen->description."_".date("d-m-Y-His").".pdf");
  }


  public function ies(){
    
    $ies = DB::table("instituciones_educativas")->get();
    
    $data_ies = array();
    
    foreach($ies as $ie){
      $registros = DB::table("libros")
        ->join("registros","libros.id","=","registros.libro")
        ->join("profesiones","registros.id_profesiones","=","profesiones.id_profesion")
        ->join("instituciones_educativas","profesiones.id_ies","=","instituciones_educativas.id_ies")
        ->where("libros.estado","=",1)
        ->where("instituciones_educativas.id_ies","=",$ie->id_ies)
        ->count();
      
      $nombre_corto="[ Vacio ]";
      if($ie->nombre_corto!=""){
        $nombre_corto=$data->nombre_corto;
      }
      $ie->numero_registros = $registros;
      
      array_push($data_ies,
          [
              '<div>'.$ie->id_ies.'</div>',
              '<div>'.$ie->nombre_ies.'</div>',
              '<div>'.$registros.'</div>'
          ]);
    }
    
    return view("reports.ies")->with("data_ies",$data_ies);
  }
}
