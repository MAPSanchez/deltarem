<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Helpers\DeleteHelper;
use App\Helpers\SpanishDateHelper;
use Illuminate\Support\Facades\Input;
use App\Examenes;
use App\Competencias;
use App\Administradores_institucionales;
use App\User;
use App\Helpers\LogHelper;

use Alert;


class AdministratorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_admins = DB::table("institution_admin")
            ->join("institution","institution.id","=","institution_admin.id_institution")
            ->join("users","users.id","=","institution_admin.id_user")
            ->select("users.username","users.email","institution.name as institucion","institution_admin.deleted","institution_admin.id")
            ->get();
        
        $admins = array();
        foreach($data_admins as $data){
            $deleted_style="";
            if($data->deleted==0){
                $deleted_str='<a href="'.route('administrators.edit', ['id' => $data->id]).'" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Editar administrador institucional con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-ui-edit"></span></a>';
            }else{
              $deleted_str ="";
              $deleted_style=' style="color:red"';
            }
          
            array_push($admins,
            [
                '<div'.$deleted_style.'>'.$data->id.'</div>',
                '<div'.$deleted_style.'>'.$data->username.'</div>',
                '<div'.$deleted_style.'>'.$data->email.'</div>',
                '<div'.$deleted_style.'>'.$data->institucion.'</div>',
                '<center>
                  <a href="'.route('administrators.show', ['id' => $data->id]).'" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Ver detalles de administrador institucional con el id '.$data->id.'" style="margin: 0px;"><span class="icofont icofont-eye-alt"></span></a>
                  
                  '.$deleted_str.'
                </center>'
            ]);
        }
      
        return view("admins.list")->with("admins",$admins);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $instituciones = DB::table("institution")->get();
        return view("admins.create",compact("instituciones"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'email' => 'required|unique:users,email',
            'username' => 'required|unique:users,username',
            'password' => 'required',
          ],[
            'email.required' => ' * Este campo es obligatorio.',
            'email.unique' => ' * Este email ya existe.',
            'username.required' => ' * Este campo es obligatorio.',
            'username.unique' => ' * Este username ya existe.',
            'password.required' => ' * Este campo es obligatorio.',
          ]);
        
          $admin = new User;
          $admin->username = filter_var(Input::get('username'), FILTER_SANITIZE_STRING);
          $admin->password =  bcrypt(Input::get('password'));
          $admin->email = filter_var(Input::get('email'), FILTER_SANITIZE_STRING);
          $admin->type = 2;
          $admin->save();

          $admin_institution = new Administradores_institucionales;
          $admin_institution->id_user = $admin->id;
          $admin_institution->id_institution = Input::get('ies');
          $admin_institution->save();

          Alert::success('Exitosamente', 'Administrador de Exámenes Registrado');
          LogHelper::instance()->insertToLog(Auth::user()->id, 'added', $admin_institution->id, "administrador de exámenes");
          return redirect()->route('administrators.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
