<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UtilController extends Controller
{
    public function getCurrentTime(){
        return response()->json(['response'=>date('Y-m-d H:i:s')]);
    }
}
