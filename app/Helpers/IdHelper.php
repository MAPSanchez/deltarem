<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class IdHelper
{
    public function getIdFrom($table_name, $column_name, $ab)
    {
        $results = DB::select( DB::raw("SELECT * FROM $table_name WHERE $column_name REGEXP '^".$ab."[0-9]' ORDER BY 'id'"));

        $values_and_positions=[];
        for($i=0;$i<sizeof($results);$i++){
            $current_code=preg_replace("/[^0-9]/", "", $results[$i]->code);

            $values_and_positions[$i][0]=intval($current_code);
            $values_and_positions[$i][1]=$results[$i]->id;
            $values_and_positions[$i][2]=$results[$i];
        }

        for($i=0;$i<sizeof($values_and_positions);$i++){
            for($j=0;$j<sizeof($values_and_positions);$j++){
                if($values_and_positions[$i][0]>$values_and_positions[$j][0]){
                    $aux=$values_and_positions[$i];
                    $values_and_positions[$i]=$values_and_positions[$j];
                    $values_and_positions[$j]=$aux;
                }
            }
        }

        if(sizeof($results)==0){
            $id = 1;
        }else{
            $id = $values_and_positions[0][0]+1;
        }

        return $ab.$id;
    }

    //Inicializacion de la clase
    public static function instance()
    {
        return new IdHelper();
    }
}
