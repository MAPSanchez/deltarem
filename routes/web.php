<?php

Route::get('/', 'Auth\LoginController@index')->name('main');
Auth::routes();

/*******************************************
* Validacion de conexion                   *
*******************************************/
Route::post('/auth/check', 'VerificationsController@loggedIn')->name('validation');
/**************  Termina validacion de conexion  ***************/

Route::group(['middleware'=>['auth']], function () {
    /*******************************************
    * Rutas para para las operaciones AJAX    *
    *******************************************/
    Route::post('/operations/ajax/obtener_datos_para_asignar_examen_estudiante', 'TestController@obtener_datos_para_asignar_examen_estudiante')->name('test.obtener_datos_para_asignar_examen_estudiante');

    Route::post('/operations/ajax/column_exists', 'VerificationsController@column_exists')->name('verify.column_exists');

    Route::post('/operations/ajax/home/buscar', 'HomeController@buscar')->name('home.buscar');
    Route::post('/operaciones/ajax/util/get_current_time', 'UtilController@getCurrentTime')->name('util.get_current_time');

    Route::post('/operaciones/ajax/tests/get_question_info', 'StudentTestController@getQuestionInfo')->name('student_test.get_question_info');
    Route::post('/operaciones/ajax/tests/get_questions_list', 'StudentTestController@getQuestionsList')->name('student_test.get_questions_list');
    Route::post('/operaciones/ajax/tests/save_answer', 'StudentTestController@saveAnswer')->name('student_test.save_answer');
    Route::post('/operaciones/ajax/tests/end_area', 'StudentTestController@endArea')->name('student_test.end_area');



    /**************  Termina rutas AJAX  ***************/


    /*******************************************
     * Rutas base del sistema                  *
     *******************************************/
    Route::get('/principal', 'HomeController@index')->name('dashboard');
    /**************  Termina rutas base  ***************/


    //Mostrar Perfil del Usuario
    Route::get('/perfil', 'ProfileController@showOwnProfile')->where('id', '[0-9]+')->name('profile.show_own_profile');

    //Editar Perfil del Usuario
    Route::get('/perfil/editar', 'ProfileController@editOwnProfile')->where('id', '[0-9]+')->name('profile.edit');

    //Actualizar Perfil de Usuario
    Route::put('/perfil', 'ProfileController@updateOwnProfile')->name('profile.update_own_profile');


    Route::get('/sesiones', 'LogController@indexSessions')->name('log.sessionlist');
    Route::get('/sesiones/{id}', 'LogController@showSession')->where('id', '[0-9]+')->name('sessions.show');
    Route::get('/movimientos', 'LogController@indexMovements')->name('log.movementslist');
    Route::get('/movimientos/{id}', 'LogController@showMovement')->where('id', '[0-9]+')->name('movements.show');


    Route::group(['middleware'=>['access:1']], function(){
        Route::get('/administradores_institucionales', 'AdministratorsController@index')->name('administrators.list');
        Route::get('/administradores_institucionales/{id}', 'AdministratorsController@show')->where('id', '[0-9]+')->name('administrators.show');
        Route::get('/administradores_institucionales/nuevo', 'AdministratorsController@create')->name('administrators.create');
        Route::get('/administradores_institucionales/{administrador}/editar', 'AdministratorsController@edit')->where('id', '[0-9]+')->name('administrators.edit');
        Route::post('/administradores_institucionales', 'AdministratorsController@store')->name('administrators.store');

        Route::get('/instituciones_educativas', 'IesController@index')->name('ies.list');
        Route::get('/instituciones_educativas/{id}', 'IesController@show')->where('id', '[0-9]+')->name('ies.show');
        Route::get('/instituciones_educativas/nuevo', 'IesController@create')->name('ies.create');
        Route::get('/instituciones_educativas/{ies}/editar', 'IesController@edit')->where('id', '[0-9]+')->name('ies.edit');
        Route::post('/instituciones_educativas', 'IesController@store')->name('ies.store');
        Route::put('/instituciones_educativas/{ies}', 'IesController@update')->name('ies.update');

    });

    Route::group(['middleware'=>['access:2']], function(){

        Route::get('/import', 'ImportController@index')->name('imports.list');
        Route::post('/import', 'ImportController@store')->name('imports.store');

        Route::get('/asignaciones', 'TestController@list_asignaciones')->name('assign.list');
        Route::post('/asignaciones/examen_estudiante', 'TestController@store_asignacion')->name('assign.store');
        Route::get('/asignaciones/asignar_examen_por_csv', 'TestController@csv')->name('assign.csv');
        Route::delete('/asignaciones/{test_application}', 'TestController@destroy_assign')->name('assign.destroy');

        Route::get('/estudiantes', 'StudentsController@index')->name('students.list');
        Route::get('/estudiantes/{id}', 'StudentsController@show')->where('id', '[0-9]+')->name('students.show');
        Route::get('/estudiantes/nuevo', 'StudentsController@create')->name('students.create');
        Route::get('/estudiantes/{student}/editar', 'StudentsController@edit')->where('id', '[0-9]+')->name('students.edit');
        Route::post('/estudiantes', 'StudentsController@store')->name('students.store');
        Route::put('/estudiantes/{student}', 'StudentsController@update')->name('students.update');

        Route::get('/listado_examenes', 'TestController@index')->name('test.list');
        Route::get('/listado_examenes/{examen}/agregar_competencias', 'TestController@add_competencies')->where('id', '[0-9]+')->name('test.add_competencies');
        Route::get('/listado_examenes/{id}', 'TestController@show')->where('id', '[0-9]+')->name('test.show');
        Route::get('/listado_examenes/nuevo', 'TestController@create')->name('test.create');
        Route::get('/listado_examenes/{examen}/editar', 'TestController@edit')->where('id', '[0-9]+')->name('test.edit');
        Route::post('/listado_examenes', 'TestController@store')->name('test.store');
        Route::post('/listado_examenes/agregar_competencia', 'TestController@store_competencia')->name('test.store_competencia');

        Route::get('/listado_examenes/{competencia}/editar_competencia', 'TestController@edit_area')->where('id', '[0-9]+')->name('test.edit_area');
        Route::put('/listado_examenes/competencias/actualizar/{competencia}', 'TestController@update_area')->name('test.update_area');
        Route::delete('/listado_examenes/competencias/eliminar/{competencia}', 'TestController@destroy_area')->name('test.destroy_area');

        Route::get('/listado_examenes/competencias/{id}/preguntas', 'TestController@show_questions')->where('id', '[0-9]+')->name('test.show_questions');
        Route::get('/listado_examenes/competencias/{competencia}/agregar_pregunta', 'TestController@add_question')->where('id', '[0-9]+')->name('test.add_question');
        Route::post('/listado_examenes/competencias/agregar_pregunta', 'TestController@store_question')->name('test.store_question');

        Route::delete('/listado_examenes/preguntas/eliminar/{pregunta}', 'TestController@destroy_question')->name('test.destroy_question');
        Route::get('/listado_examenes/preguntas/editar/{pregunta}', 'TestController@edit_question')->where('id', '[0-9]+')->name('test.edit_question');
        Route::put('/listado_examenes/preguntas/actualizar/{pregunta}', 'TestController@update_question')->name('test.update_question');
        //Route::get('/reportes/instituciones_educativas', 'ReportsController@ies')->name('reports.ies');
        //Route::get('/reportes/profesiones_instituciones_educativas', 'ReportsController@professions')->name('reports.professions');

        //Route::get('/registros/{registro}', 'RegistrosController@show')->where('id', '[0-9]+')->name('registros.show');

        Route::get('/resultados/por_estudiante', 'ResultsController@results_student')->name('results.student');
        Route::get('/resultados/por_examen', 'ResultsController@results_test')->name('results.test');

        Route::get("/reporte/resultados_por_estudiante/{id}","ReportsController@results_student")->where('id', '[0-9]+')->name("reports.results_student");
        Route::get("/reporte/resultados_por_examen/{id}","ReportsController@results_test")->where('id', '[0-9]+')->name("reports.results_test");

    });

    Route::group(['middleware'=>['access:3']], function(){
        /*******************************************
         * Rutas libros (capturista)               *
         *******************************************/

        Route::get('/examenes', 'StudentTestController@index')->name('student_test.list');
        Route::get('/examenes/{id}', 'StudentTestController@takeTest')->where('id', '[0-9]+')->name('student_test.take_test');
        Route::get('/examenes/{id}/competencias', 'StudentTestController@listTestAreas')->where('id', '[0-9]+')->name('student_test.areas.list');
        Route::get('/examenes/{id}/{id_area}', 'StudentTestController@answerTestArea')->where('id', '[0-9]+')->name('student_test.areas.answer');
        Route::get('/examenes/finish/{id}/{id_area}', 'StudentTestController@finishTime')->where('id', '[0-9]+')->name('student_test.finish_time');

        /**************  Termina libros (capturista)  ***************/
    });

});

Route::get('type/{type}', 'SweetAlertController@notification');
