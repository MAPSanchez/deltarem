@extends('layouts.app')

@section('title',"Reporte de títulos emitidos por IES- SRT")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-university" style="min-width:50px; background-color:#43B4E4"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Reporte de títulos emitidos por IES</h4>
						<span style="text-transform: none;">Reporte de número de títulos emitidos por institución educativa superior.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Reportes
						</li>
            <li class="breadcrumb-item">IES
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Zero config.table start -->
				<div class="card">
					<div class="card-block">

						<div class="dt-responsive table-responsive">
							@if($data_ies!=NULL)
								<table style="width:100%" id="ies" class="table table-striped table-bordered">
									<thead id="table_header">
										<tr>
											<th class="all" scope="col" style="width:10%;">Id</th>
											<th scope="col" style="width:70%;">Institución Educativa Superior</th>
											<th scope="col">Número de títulos registrados</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							@else
								<center>
									<div class="alert alert-warning icons-alert" id="alert_div">
										<strong>Información</strong>
										<p>Actualmente no hay ningúna institucion educativa registrada en el sistema.</p>
									</div>
								</center>
							@endif
						</div>
					</div>
				</div>
			</div>
      
		</div>
   
	</div>
</div>
@endsection

@section('javascriptcode')
<script>
	var data = @php echo(json_encode($data_ies)); @endphp;
	$("#ies").DataTable({
		responsive: true,
		dom : 'frtip',
		data: data,
		aaSorting: [ [2,'desc'] ],
		language: {
			search: "Buscar:",
			searchPlaceholder: "Buscar en Instituciones Educativas..."
		},
		initComplete: function() {
			$('#ies_filter').find(">:first-child").css('float','left');
			$('#ies_filter').find(">:first-child").css('width','93%');
		}
	});

</script>
@endsection
