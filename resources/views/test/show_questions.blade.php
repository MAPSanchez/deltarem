@extends('layouts.app')

@section('title',"Preguntas - SAE")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="icofont icofont-eye-alt" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Preguntas de "{{$competencia->description}}"</h4>
						<span style="text-transform: none;">Mostrando preguntas de competencia "{{$competencia->description}}" del examen "{{$examen->description}}".</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Exámenes
						</li>
						<li class="breadcrumb-item">Competencias
						</li>
                        <li class="breadcrumb-item">Preguntas
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Basic Form Inputs card start -->
				<div class="card">
					<div class="card-block">

						<div class="page-body">
							<div class="row">
								<div class="col-md-12 col-xl-12 ">

                                @if($preguntas->isNotEmpty())
                                    @php 
                                        $num = 1;
                                    @endphp
                                    @foreach($preguntas as $pregunta)
                                    <div class="card-block">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <h5> <strong>Pregunta {{$num}}</strong> </h5>
                                            </div>
                                            @if($permitir_cambios == "si")
                                            <div class="col-lg-2">
                                                <a href="{{route('test.edit_question', ['id' => $pregunta->id])}}" class="btn btn-inverse col-lg-12" data-toggle="tooltip" data-placement="bottom" title="Editar pregunta {{$num}}"><span class="fa fa-edit"></span>&nbsp Editar</a>
                                            </div>
                                            <div class="col-lg-2">
                                            <a href="#" onclick="delete_question('{{ route('test.destroy_question', ['id' => $pregunta->id]) }}')" class="btn btn-inverse col-lg-12" data-toggle="tooltip" data-placement="bottom" title="Eliminar pregunta {{$num}}" style="margin: 0px;"><span class="fas fa-trash-alt"></span>&nbsp Eliminar</a>
                                            </div>
                                            @else
                                            <div class="col-lg-2">
                                            <button class="btn btn-disabled col-lg-12" data-toggle="tooltip" data-placement="bottom" title="No se puede editar la pregunta porque este examen ya ha sido asignado"><span class="fa fa-trash-alt"></span>&nbsp Editar</a>
                                            </div>
                                            <div class="col-lg-2">
                                                <button class="btn btn-disabled col-lg-12" data-toggle="tooltip" data-placement="bottom" title="No se puede eliminar la pregunta porque este examen ya ha sido asignado"><span class="fa fa-trash-alt"></span>&nbsp Eliminar</a>
                                            </div>
                                            @endif
                                        </div><br>
										<div>
                                            @php 
                                                echo($pregunta->question_txt);
                                            @endphp
                                            &nbsp
                                        </div>
                                        
                                        <p style="color:red">Respuesta correcta: 
                                            
                                            @if($pregunta->correct_option == 0)
                                                                        A)
                                                                    @elseif($pregunta->correct_option == 1)
                                                                    B)
                                                                    @elseif($pregunta->correct_option == 2)
                                                                    C)
                                                                    @else
                                                                    D)
                                                                    @endif</p>										
										<br>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <table class="table table-de">
                                                    @foreach($pregunta->respuestas as $respuesta)
                                                    <tr>    
                                                        <td style="text-align: left; width:5%;" >@if($respuesta->num_answer == 0)
                                                                A) &nbsp
                                                            @elseif($respuesta->num_answer == 1)
                                                            B) &nbsp
                                                            @elseif($respuesta->num_answer == 2)
                                                            C) &nbsp
                                                            @else
                                                            D) &nbsp
                                                            @endif                    
                                                        </td>
                                                        <td style="text-align: left; width:95%;" >
                                                            @php echo($respuesta->text); @endphp
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                            <div class="col-lg-6">
                                                <div style="overflow-y:auto; overflow-x:auto; max-height:17vw">
                                                    @if($pregunta->img_src != NULL)
                                                        <img title="Haga clic en la imagen para verla con mayor detalle" style="cursor:pointer; float:right; object-fit: cover; overflow: hidden;" src='{{ asset($pregunta->img_src) }}' />
                                                    @endif                                                
                                                    <div id="modal_show_img" class="modal">
                                                        <span class="close">&times;</span>
                                                        <img class="modal-content" style=";display: block;
                                                            max-width:100%;
                                                            max-height:100%;
                                                            width: auto;
                                                            height: 60vh;" id="img_content">
                                                        <div id="caption"></div>
                                                    </div>
                                                </div>
                                            </div>
										</div>
                                        <br>
                                        <hr>
									</div>
                                    @php 
                                        $num++;
                                    @endphp
                                    @endforeach
                                @else
                                    
                                    <div class="alert alert-primary icons-alert">
                                        <p><strong>Sin preguntas</strong></p>
                                        <p> Esta competencia no posee preguntas agregadas.</p>
                                    </div>
                                @endif

								</div>
							</div>
                            <br><br>
                                <center>
                                <a style="color:white" onclick="returnURL('{{ url()->previous() }}')"  class="btn btn-primary"><i class="icofont icofont-arrow-left"></i>Regresar</a>
                                </center>
						</div>
					</div>
				</div>






			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>
	function delete_question(url){

        swal({
            title: "Atención",
            text: "¿Está seguro de eliminar la pregunta?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            buttons: ["Cancelar", "Eliminar"],
            closeModal: false
        }).then(function(result){
            if (result) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}',
                    }
                });
                $.ajax({
                    url: url,
                    type: 'POST',
                    data:{
                            _method:"DELETE"
                        },
                    success: function(result) {
                        swal({
                            icon: 'success',
                            title: 'Éxito',
                            text: 'La pregunta fue eliminada correctamente.',
                            buttons: 'Aceptar',
                        });
                        setTimeout(function(){
                            location.reload();
                        },900);
                    }
                });
            }
        });
            

    }
</script>
@endsection
