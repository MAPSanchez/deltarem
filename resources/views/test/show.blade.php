@extends('layouts.app')

@section('title',"Competencias de Examen - SAE")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="icofont icofont-eye-alt" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Competencias de Examen</h4>
						<span style="text-transform: none;">Mostrando información y competencias del examen "{{$examen->description}}".</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Exámenes
						</li>
						<li class="breadcrumb-item">Competencias del Examen
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Basic Form Inputs card start -->
				<div class="card">
					<div class="card-block">

						<div class="page-body">
							<div class="row">
								<div class="col-md-12 col-xl-12 ">
									<div class="card-block user-detail-card">
										<h4 class="sub-title">Información del Examen</h4>
										<div class="row">
										
											<div class="col-sm-12 user-detail">
												<div class="row">
													<div class="col-sm-4">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-cube"></i>Nombre:</h6>
													</div>
													<div class="col-sm-8">
														<h6 class="m-b-30">{{ $examen->description }}</h6>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-4">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-cube"></i>Tiempo límite:</h6>
													</div>
													<div class="col-sm-8">
														<h6 class="m-b-30">{{ $tiempo_limite}} minutos</h6>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-4">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-cube"></i>Cantidad de Preguntas:</h6>
													</div>
													<div class="col-sm-8">
														<h6 class="m-b-30">{{ $cantidad_preguntas}} preguntas</h6>
													</div>
												</div>
											</div>
										</div>
									</div>
                                    <div class="card-block user-detail-card">
										<h4 class="sub-title">Competencias que evalúan el examen</h4>
										<div class="row">
										

                                        <div class="dt-responsive table-responsive">
                                            <table id="compact" class="table compact table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Competencia</th>
                                                        <th>Tiempo Limite</th>
                                                        <th>Cantidad de Preguntas</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                        @foreach($competencias as $c)
                                                        <tr>
                                                            <td>{{$c->description}}</td>
                                                            <td>{{$c->time}} minutos</td>
                                                            <td>{{$c->num_preguntas}}</td>
                                                            <td>
                                                            <a href="{{route('test.show_questions', ['id' => $c->id])}}" class="btn btn-warning" data-toggle="tooltip" data-placement="bottom" title="Ver preguntas de {{$c->description}}" style="margin: 0px;"><span class="icofont icofont-eye-alt"></span></a>

															@if($permitir_cambios == "si")
                                                            <a href="{{route('test.add_question', ['id' => $c->id])}}" class="btn btn-inverse" data-toggle="tooltip" data-placement="bottom" title="Agregar pregunta a {{$c->description}}" style="margin: 0px;"><span class="fas fa-plus"></span></a>
															<a href="{{route('test.edit_area', ['id' => $c->id])}}" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="Editar competencia {{$c->description}}" style="margin: 0px;"><span class="fas fa-edit"></span></a>
															<a href="#" onclick="delete_area('{{ route('test.destroy_area', ['id' => $c->id]) }}')" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Eliminar competencia {{$c->description}}" style="margin: 0px;"><span class="fas fa-trash-alt"></span></a>
															@endif
                                                            </td>
                                                            </tr>
                                                        @endforeach
                                                   
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                         <th>Competencia</th>
                                                        <th>Tiempo Limite</th>
                                                        <th>Cantidad de Preguntas</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>
	function delete_area(url){

		swal({
			title: "Atención",
			text: "¿Está seguro de eliminar la competencia?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Eliminar"],
			closeModal: false
		}).then(function(result){
			if (result) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': '{{ csrf_token() }}',
					}
				});
				$.ajax({
					url: url,
					type: 'POST',
					data:{
                            _method:"DELETE"
                        },
					success: function(result) {
						swal({
							icon: 'success',
							title: 'Éxito',
							text: 'La competencia fue eliminada correctamente.',
							buttons: 'Aceptar',
						});
						setTimeout(function(){
							location.reload();
						},900);
					}
				});
			}
		});
			

	}
</script>
@endsection
