@extends('layouts.app')

@section('title',"Editar Competencia de Examen- SAE")

@section('body')


<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-list-alt" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Editar Competencia de Examen</h4>
						<span style="text-transform: none;">Editar competencia de examen con el id {{$examen->id}}.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Exámenes
						</li>
						<li class="breadcrumb-item">Editar Competencia
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Basic Form Inputs card start -->
				<div class="card">
					<div class="card-block">

						<div class="page-body">
							<div class="row">
								<div class="col-md-6 col-xl-6 ">
									<div class="card-block user-detail-card">
										
											<div class="alert alert-warning icons-alert">
												<p><strong>Editando Competencia</strong></p>
												<p> Usted esta editando una competencia del examen "{{$examen->description}}".</p>
											</div>
                                            <form id="form_libros" method="POST" action="{{ route('test.update_area', ['competencia'=>$competencia->id]) }}" enctype="multipart/form-data">
                                            {{ method_field('PUT') }}
                                                {!! csrf_field() !!}

                                                <input type="hidden" id="id_examen" name="id_examen" value="{{$examen->id}}">

                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">Nombre de la Competencia:</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="description" name="description" value="{{ old('description',$competencia->description) }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">Tiempo límite (exprese cantidad en minutos):</label>
                                                    <div class="col-sm-8">
                                                        <input type="number" class="form-control" id="time" name="time" value="{{ old('time',$competencia->time) }}" required>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div style="margin:0px;" class="col-sm-8">

                                                    </div>
                                                    <div class="col-sm-4">
                                                        <button class="btn btn-success btn-adjust-mobile" type="submit" style="float:right;width:100%; min-width:150px"><i class="fa fa-edit"></i>Editar</button>

                                                    </div>
                                                </div>
                                            </form>
									</div>
								</div>
                                <div class="col-md-6 col-xl-6 ">
									<div class="card-block user-detail-card">
										
                                    <h4 class="sub-title">otras Competencias asignadas al examen</h4>
										<div class="row">
										
                                        @if($competencias->isNotEmpty())
                                        <div class="dt-responsive table-responsive">
                                            <table  class="table compact table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Competencia</th>
                                                        <th>Tiempo Limite</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                        @foreach($competencias as $c)
                                                        <tr>
                                                            <td>{{$c->description}}</td>
                                                            <td>{{$c->time}} minutos</td>
														</tr>
                                                        @endforeach
                                                   
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                         <th>Competencia</th>
                                                        <th>Tiempo Limite</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        @else
                                            
											<div class="alert alert-primary icons-alert">
												<p><strong>Sin competencias</strong></p>
												<p> Este examen no posee otras competencias agregadas.</p>
											</div>
                                        @endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('javascriptcode')
<script>



</script>
@endsection
