@extends('layouts.app')

@section('title',"Listado de Exámenes - SAE")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-list-alt" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Listado de Exámenes</h4>
						<span style="text-transform: none;">Lista de todos los exámenes creados en el sistema.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Exámenes
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Zero config.table start -->
				<div class="card">
					<div class="card-block">
						<div class="row">
							<div class="col-sm-7"></div>
							<div class="col-sm-5">
								<button id="crear_examen" class="btn btn-success col-sm-12"><i class="fa fa-plus"></i>Crear Examen</button>
							</div>
						</div><br>
						<div class="dt-responsive table-responsive">
							@if($examenes!=NULL)
								<table style="width:100%" id="examenes" class="table table-striped table-bordered">
									<thead id="table_header">
										<tr>
											<th class="all" scope="col" style="width:10%;">ID</th>
											<th scope="col" style="width:70%;">Nombre</th>
											<th class="all" scope="col" style="width:20%;">Acciones</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							@else
								<center>
									<div class="alert alert-warning icons-alert" id="alert_div">
										<strong>Información</strong>
										<p>Actualmente no hay ningún examen registrado en el sistema.</p>
									</div>

								</center>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>

  	var data = @php echo(json_encode($examenes)); @endphp;
  	$("#examenes").DataTable({
		responsive: true,
		dom : 'frtip',
    data: data,
		aaSorting: [ [2,'asc'] ],
		language: {
			search: "Buscar:",
			searchPlaceholder: "Buscar en exámenes..."
		},
		initComplete: function() {
			$('#examenes_filter').find(">:first-child").css('float','left');
			$('#examenes_filter').find(">:first-child").css('width','93%');
		}
	});

	$("#crear_examen").click(function(){
		$('#crear_examen_modal').modal('show');
	});



</script>
@endsection
