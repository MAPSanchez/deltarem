@extends('layouts.app')

@section('title',"Editar Pregunta- SAE")

@section('body')


<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-question-circle" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Editar Pregunta</h4>
						<span style="text-transform: none;">Llene los campos solicitados en la parte inferior para editar una pregunta.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Exámenes
						</li>
						<li class="breadcrumb-item">Competencias
						</li>
                        <li class="breadcrumb-item">Preguntas
						</li>
                        <li class="breadcrumb-item">Editar Pregunta
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
    
		<div class="row">
			<div class="col-sm-12">
				<!-- Basic Form Inputs card start -->
				<div class="card">
					<div class="card-block">

						<div class="page-body">
							<div class="row">
								<div class="col-md-12 col-xl-12">
									<div class="card-block user-detail-card">
										
											<div class="alert alert-warning icons-alert">
												<p><strong>Editando Pregunta</strong></p>
												<p> Usted se encuentra editando una pregunta de la competencia "{{$competencia->description}}".</p>
											</div>
                                            <form method="POST" action="{{ route('test.update_question', ['pregunta'=>$pregunta->id]) }}" enctype="multipart/form-data">
                                            {{ method_field('PUT') }}
                                                {!! csrf_field() !!}

                                                
												<div class="form-group row">
													<label class="col-sm-2 col-form-label" for="pregunta">Pregunta:</label>
													<div class="col-sm-10">
														<textarea name="pregunta" class="form-control" required id="pregunta" cols="30" rows="2">{{ old('pregunta',$pregunta->question_txt) }}</textarea>
														@if ($errors->has('pregunta'))
															<div class="col-form-label" style="color:red;">{{$errors->first('pregunta')}}</div>
														@endif
													</div>
												</div>
												<h4 class="sub-title">Posibles respuestas</h4>

												<div class="form-group row">
													<label class="col-sm-2 col-form-label" for="a">Respuesta A:</label>
													<div class="col-sm-4">
														<textarea name="a" class="form-control" required id="a" cols="30" rows="2">{{ old('a',$respuestas[0]->text) }}</textarea>
														@if ($errors->has('a'))
															<div class="col-form-label" style="color:red;">{{$errors->first('a')}}</div>
														@endif
													</div>
													<label class="col-sm-2 col-form-label" for="b">Respuesta B:</label>
													<div class="col-sm-4">
														<textarea name="b" class="form-control" required id="b" cols="30" rows="2">{{ old('b',$respuestas[1]->text) }}</textarea>
														@if ($errors->has('b'))
															<div class="col-form-label" style="color:red;">{{$errors->first('b')}}</div>
														@endif
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2 col-form-label" for="c">Respuesta C:</label>
													<div class="col-sm-4">
														<textarea name="c" class="form-control" required id="c" cols="30" rows="2">{{ old('c',$respuestas[2]->text) }}</textarea>
														@if ($errors->has('c'))
															<div class="col-form-label" style="color:red;">{{$errors->first('c')}}</div>
														@endif
													</div>
													<label class="col-sm-2 col-form-label" for="d">Respuesta D:</label>
													<div class="col-sm-4">
														<textarea name="d" class="form-control" required id="d" cols="30" rows="2">{{ old('d',$respuestas[3]->text) }}</textarea>
														@if ($errors->has('d'))
															<div class="col-form-label" style="color:red;">{{$errors->first('d')}}</div>
														@endif
													</div>
												</div>
                                               <hr>
											   <div class="form-group row">
													<label class="col-sm-2 col-form-label" for="respuesta">Respuesta correcta:</label>
													<div class="form-radio col-sm-10">
														<div class="radio radiofill radio-inverse radio-inline">
															<label>
                                                                @if($pregunta->correct_option == 0)
                                                                <input type="radio" name="respuesta" required value="0" checked="checked">
                                                                <i class="helper"></i>A)
                                                                @else
																<input type="radio" name="respuesta" required value="0">
																<i class="helper"></i>A)
                                                                @endif
															</label>
														</div>
														<div class="radio radiofill radio-inverse radio-inline">
															<label>
                                                                @if($pregunta->correct_option == 1) 
																<input type="radio" name="respuesta" value="1" checked="checked">
																<i class="helper"></i>B)
                                                                @else
                                                                <input type="radio" name="respuesta" value="1">
																<i class="helper"></i>B)
                                                                @endif
															</label>
														</div>
														<div class="radio radiofill radio-inverse radio-inline">
															<label>
                                                                @if($pregunta->correct_option == 2) 
																<input type="radio" name="respuesta" value="2" checked="checked">
																<i class="helper"></i>C)
                                                                @else
                                                                <input type="radio" name="respuesta" value="2">
																<i class="helper"></i>C)
                                                                @endif
															</label>
														</div>
														
														<div class="radio radiofill radio-inverse radio-inline">
															<label>
                                                                @if($pregunta->correct_option == 3)
																<input type="radio" name="respuesta" value="3" checked="checked">
																<i class="helper"></i>D)
                                                                @else
                                                                <input type="radio" name="respuesta" value="3">
																<i class="helper"></i>D)
                                                                @endif
															</label>
														</div>
														
														



													</div>
												</div>
												
                                                
                                                <table style="width:100%">
												<tr>
													<th style="border-top: 1px solid #e9ecef; padding-top: 20px;  padding-bottom: 20px;" scope="row">Recurso: </th>
												</tr>
											</table>
											<table style="width:100%">
												<tr>
													@if($pregunta->img_src!="")
														<th scope="row"></th>
														<td>
															<img id="modal_img" style="border-radius: 15px; max-width:300px" src='{{ asset("/".$pregunta->img_src)}}' class="img-fluid p-b-10">
															<input type="text" name="image_2" class="form-control" hidden value="{{ $pregunta->img_src }}">
															<div id="modal_show_img" class="modal">
																<span class="close">&times;</span>
																<img class="modal-content" id="img_content">
																<div id="caption"></div>
															</div>
															<div class="col-form-label" style="align:justify;"> * Vista del recurso actual de la pregunta.</div>
														</td>
													@endif
													<td>
														<div class="file-upload">
															<div class="image-upload-wrap">
																<input id="image_input" class="file-upload-input" type='file' name="image" onchange="readURL(this);" accept="image/*" />
																<div style="padding-top:40px" onclick="$('.file-upload-input').trigger('click' )">
																	<center>
																		<i style="font-size: 60px;" class="fas fa-cloud-upload-alt drag-icon"></i>
																	</center>
																</div>
																<div class="drag-text">
																	<span>Arrastre y suelte el archivo <span style="font-weight: bold; font-size:16px;"> aquí</span> o haga clic <span style="font-weight: bold; font-size:16px;"> aquí</span> para buscarlo en su equipo.</span>
																</div>
															</div>
															<div class="file-upload-content">
																<img class="file-upload-image" src="#" alt="your image" />
																<div class="image-title-wrap">
																	<button type="button" onclick="removeUpload()" class="remove-image">Remover Imagen</button>
																</div>
															</div>
														</div>

													</td>
												</tr>
											</table>



                                                <br>
                                                <div class="row">
                                                    <div style="margin:0px;" class="col-sm-5">

                                                    </div>
                                                    <div class="col-sm-3">
                                                    <a style="color:white; width:100%;" onclick="returnURL('{{ url()->previous() }}')"  class="btn btn-primary"><i class="icofont icofont-arrow-left"></i>Regresar</a>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <button class="btn btn-success btn-adjust-mobile" type="submit" style="float:right;width:100%; min-width:150px"><i class="fa fa-edit"></i>Editar Pregunta</button>

                                                    </div>
                                                </div>
                                            </form>
									</div>
								</div>
                               
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('javascriptcode')
<script src="{{ asset('assets/pages/wysiwyg-editor/wysiwyg-editor.js') }}"></script>
<script src="{{ asset('assets/pages/wysiwyg-editor/js/tinymce.min.js') }}"></script>

<script>

function readURLForComponents(input) {
    
    var split = input.files[0]['name'].split(".");
    var extension = split[split.length - 1];
    var accepted_extentions = ['pdf', 'PDF', 'jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG', 'bmp', 'BMP'];
    var accepted_file = false;
    if (accepted_extentions.includes(extension)) {
        accepted_file = true;
    }
    if (accepted_file) {
        if (input.files && input.files[0]) {
            if(input.files[0]['size']<5242880){
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('.image-upload-wrap').hide();
                    if(extension=="pdf" || extension=="PDF"){
                        $('.file-upload-image').height(100);
                        $('.file-upload-image').width(75);
                        $('.file-upload-image').attr('src', window.location.href.split('/').slice(0, 3).join('/')+"/img/pdf_icon.png");
                    }else{
                        $('.file-upload-image').css('height', 'auto');
                        $('.file-upload-image').css('width', 'auto');
                        $('.file-upload-image').attr('src', e.target.result);
                    }
                    $('.file-upload-content').show();
                    $('.image-title').html(input.files[0].name);
                };
                reader.readAsDataURL(input.files[0]);
                console.log(reader);
            }else{
                $("#image_input").val("");
                removeUpload();
                swal({
                    icon: 'error',
                    title: 'Archivo demasiado grande',
                    text: 'El archivo supera los 5 MB de tamaño, por favor seleccione un archivo que no sobrepase los 5 MB de tamaño.',
                    buttons: 'Aceptar',
                });
            }
        } else {
            removeUpload();
        }
    }else{
        $("#image_input").val("");
        swal({
            icon: 'error',
            title: 'Formato de archivo no valido',
            text: 'Por favor solo agregue archivos con los siguientes formatos validos: pdf, jpg, png, jpeg, bmp.',
            buttons: 'Aceptar',
        })
    }
     
}
    
function removeUpload() {
    $('.file-upload-input').replaceWith($('.file-upload-input').clone());
    $("#image_input").val("");
    $('.file-upload-content').hide();
    $('.image-upload-wrap').show();
    $('.image-title').html('Ningún archivo cargado');
}
$('.image-upload-wrap').bind('dragover', function() {
    $('.image-upload-wrap').addClass('image-dropping');
});
$('.image-upload-wrap').bind('dragleave', function() {
    $('.image-upload-wrap').removeClass('image-dropping');
});


</script>
@endsection
