@extends('layouts.app')

@section('title',"Resultados por Estudiante - SAE")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-file-pdf" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Resultados por Estudiante</h4>
						<span style="text-transform: none;">A continuación se observa la lista de estudiantes y los exámenes que ha presentado el estudiante.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Resultados por estudiante
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Zero config.table start -->
				<div class="card">
					<div class="card-block">
						<div class="dt-responsive table-responsive">
                        @if($students!=NULL)
                    <table style="width:100%" id="custom_datatable" class="table table-striped table-bordered">
                        <thead id="table_header">
                            <tr>
                                <th class="all" scope="col">Matrícula</th>
                                <th scope="col">Estudiante</th>
                                <th scope="col">Examen Presentado</th>
                                <th class="all" scope="col" style="width:23%;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                       
                    </table>
                @else
                    <center>
                        <div class="alert alert-warning icons-alert" id="alert_div">
                            <strong>Información</strong>
                            <p>Actualmente no hay ningún estudiante que haya presentado un examen en el sistema.</p>
                        </div>
                    </center>
                @endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>
  var data = @php echo(json_encode($students)); @endphp;
	var button='';
  applyStyleToDatatable(button, 'Buscar en resultados por estudiante...',1,'asc',data);

</script>
@endsection
