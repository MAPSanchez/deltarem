@extends('layouts.app')

@section('title',"Editar Perfil - SAE")

@section('body')

<!-- Main-body start -->
<div class="main-body">
	<!-- Page-body start -->
	<div class="page-body">
		<!--profile cover start-->
		<div class="row">
			<div class="col-lg-12">
				<div class="cover-profile">
					<div class="profile-bg-img">
						<img class="profile-bg-img img-fluid" src="{{asset('assets/images/user-profile/blue2.jpg')}}" style="width: 100%; height: 170px;" alt="bg-img">
						<div class="card-block user-info">
							<div class="col-md-12">
								<div class="media-left">
									<a class="profile-image">
										<img id="modal_img" src='{{ asset('assets/images/user_img.png') }}' alt="{{ $user->username }}" class="img-fluid rounded" style="width:142px; height: 132px;">
										<div id="modal_show_img" class="modal">
											<span class="close">&times;</span>
											<img class="modal-content" style="top:-60%;transform: scale(0.5) " id="img_content">
											<div id="caption"></div>
										</div>
									</a>
								</div>
								<div class="media-body row">
									<div class="col-lg-12">
										<div class="user-title">
											<h2>{{ $user->username }}</h2>
											@switch($user->type)
												@case(1)
												<span class="text-white">Administrador de Sistema</span>
												@break
												@case(2)
												<span class="text-white">Administrador de Institución</span>
												@break
												@case(3)
												<span class="text-white">Estudiante</span>
												@break

											@endswitch
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--profile cover end-->
		<div class="row">
			<div class="col-lg-12">
				<!-- tab header end -->
				<!-- tab content start -->
				<div class="tab-content">
					<!-- tab panel personal start -->
					<div class="tab-pane active" id="personal" role="tabpanel">
						<!-- personal card start -->
						<div class="card">

							<div class="card-block">
								<h4 class="sub-title"><strong>Actualizar contraseña</strong></h4>
								<form id="form" method="POST" action="{{ route('profile.update_own_profile') }}" files="true" enctype="multipart/form-data">
									{{ method_field('PUT') }}
									{!! csrf_field() !!}

									<div class="view-info">
										<div class="row">
											

										<div class="table-responsive">
														<table class="table">
															<tbody>
																<tr>
																	<th scope="row">Email: </th>
																	<td>
																		<input type="email" class="form-control" id="email" name="email" value="{{ old('email', $user->email) }}" readonly>
																		@if ($errors->has('email'))
																		<div class="col-form-label" style="color:red;">{{$errors->first('email')}}</div>
																		@endif
																		<div id="error_email" class="col-form-label" style="color:red; display:none;"></div>
																	</td>
																</tr>
																<tr>
																	<th scope="row">Username: </th>
																	<td>
																		<input type="text" class="form-control" id="username" name="username" value="{{ old('username', $user->username) }}" readonly>
																		@if ($errors->has('username'))
																		<div class="col-form-label" style="color:red;">{{$errors->first('username')}}</div>
																		@endif
																		<div id="error_username" class="col-form-label" style="color:red; display:none;"></div>
																	</td>
																</tr>
																<tr>
																	<th scope="row">Contraseña: </th>
																	<td>
																		<input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}">
																		@if ($errors->has('password'))
																		<div class="col-form-label" style="color:red;">{{$errors->first('password')}}</div>
																		@endif
																		<div id="message_password_div" class="col-form-label">* Si deja la contraseña en blanco, no se cambiará</div>
																	</td>
																</tr>
																<tr id="confirm_password_div">
																	<th scope="row">Confirmar Contraseña: </th>
																	<td>
																		<input type="password" class="form-control" id="confirm_password" value="">
																		<div id="error_password_div" class="col-form-label">* Las contraseñas no coinciden</div>

																	</td>
																</tr>
																<tr>
																	<th scope="row">Tipo de Usuario: </th>
																	@switch($user->type)
																	@case(1)
																			<td>Administrador de Sistema</td>
																			@break
																			@case(2)
																			<td>Administrador de Institución</td>
																			@break
																			@case(3)
																			<td>Estudiante</td>
																			@break
	
																	@endswitch
																</tr>
															</tbody>
														</table>

													</div>


										</div>

										<!-- end of row -->
									</div>
									<!-- end of general info -->
									<center>
										<a style="color:white" onclick="confirmationOnReturn('{{ url()->previous() }}')" class="btn btn-primary btn-adjust-mobile"><i class="icofont icofont-arrow-left"></i>Regresar</a>
										<button type="button" id="update_btn" class="btn btn-success btn-adjust-mobile"><i class="icofont icofont-refresh"></i>Actualizar Contraseña</button>
									</center>
								</form>
							</div>
							<!-- end of col-lg-12 -->
						</div>
						<!-- end of row -->
					</div>
					<!-- end of view-info -->
				</div>
				<!-- end of card-block -->
			</div>
		</div>
		<!-- tab pane personal end -->
	</div>
	<!-- Page-body end -->
</div>

@endsection

@section('javascriptcode')
<script>
	$(document).ready(function() {
		var valid_password=true;
		$("#confirm_password_div").hide();
		$("#error_password_div").hide();

		$("#password").keyup(function(){
			if($("#password").val()!=""){
				$("#confirm_password_div").show(200);
				$("#message_password_div").hide();
			}else{
				$("#confirm_password_div").hide(200);
				$("#message_password_div").show();
			}
		});
		$("#update_btn").click(function(){
			if(valid_password){
				$('form#form').submit();
			}else{
				swal({
					icon: 'error',
					title: 'Las contraseñas que ingreso no coinciden',
					text: 'Ingrese de nuevo su contraseña en el campo de confirmación poder continuar.',
					buttons: 'Aceptar',
				});
			}
		});
		$("#confirm_password").keyup(function(){
			if($("#confirm_password").val()!=""){
				if($("#confirm_password").val()!=$("#password").val()){
					$("#confirm_password").css("border-color",'red');
					$("#password").css("border-color",'red');
					$("#error_password_div").show();
					valid_password=false;
				}else{
					$("#message_password_div").hide();
					$("#confirm_password").css("border-color",'#bab8b8');
					$("#password").css("border-color",'#bab8b8');
					$("#error_password_div").hide();
					valid_password=true;
				}
			}else{
				$("#message_password_div").hide();
				$("#confirm_password").css("border-color",'#bab8b8');
				$("#password").css("border-color",'#bab8b8');
				$("#error_password_div").hide();
				valid_password=true;
			}
		});
		elements_id = [
			$('#email'),
			$('#username'),
		];
		var original_values = [
			['email', $('#email').val()],
			['username', $('#username').val()],
		];
		unique_elements = [
			[$('#email'), 'email', 'users', original_values, $('#error_email'),
				'* El correo electrónico que esta intentando ingresar no está disponible.'],
			[$('#username'), 'username', 'users', original_values, $('#error_username'),
				'* El username que esta intentando ingresar no está disponible.'],
		];
		error_divs = [
			$('#error_email'),
			$('#error_username'),
		]
		checkIfChangesHaveBeenMadeIn(elements_id, unique_elements);
	});
</script>
@endsection
