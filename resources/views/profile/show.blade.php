@extends('layouts.app')

@section('title',"Perfil - SAE")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-body start -->
	<div class="page-body">
		<!--profile cover start-->
		<div class="row">
			<div class="col-lg-12">
				<div class="cover-profile">
					<div class="profile-bg-img">
						<img class="profile-bg-img img-fluid" src="{{asset('assets/images/user-profile/blue2.jpg')}}" style="width: 100%; height: 170px;" alt="bg-img">
						<div class="card-block user-info">
							<div class="col-md-12">
								<div class="media-left">
									<a class="profile-image">
										<img id="modal_img" src='{{ asset('assets/images/user_img.png') }}' alt="{{ $user->username }} " class="img-fluid rounded" style="width:142px; height: 132px;">
										<div id="modal_show_img"  class="modal">
											<span class="close">&times;</span>
											<img class="modal-content" style="top:-60%;transform: scale(0.5) " id="img_content">
											<div id="caption"></div>
										</div>
									</a>
								</div>
								<div class="media-body row">
									<div class="col-lg-12">
										<div class="user-title">
											<h2>{{ $user->username }}</h2>
											@switch($user->type)
												@case(1)
												<span class="text-white">Administrador de Sistema</span>
												@break
												@case(2)
												<span class="text-white">Administrador de Institución</span>
												@break
												@case(3)
												<span class="text-white">Estudiante</span>
												@break
											@endswitch
										</div>
									</div>
									<div>
										<div class="pull-right cover-btn">
											<a href="{{ route('profile.edit') }}" title="Editar Perfil"><button type="button" class="btn btn-inverse m-r-10 m-b-5"><i class="icofont icofont-ui-edit"></i>Editar Perfil</button></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--profile cover end-->
		<div class="row">
			<div class="col-lg-12">
				<!-- tab header end -->
				<!-- tab content start -->
				<div class="tab-content">
					<!-- tab panel personal start -->
					<div class="tab-pane active" id="personal" role="tabpanel">
						<!-- personal card start -->
						<div class="card">
							<div class="card-header">
								<h4 class="sub-title"><strong>Información General</strong></h4>
							</div>
							<div class="card-block">
								<div class="view-info">
									<div class="row">
										<div class="col-lg-12">
											<div class="general-info">
												<div class="row">
													<div class="col-lg-12 col-xl-12">
														<div class="table-responsive">
															<table class="table">
																<tbody>
																	<tr>
																		<th scope="row">Correo Electrónico: </th>
																		<td>{{ $user->email }}</td>
																	</tr>
																	<tr>
																		<th scope="row">Username: </th>
																		<td>{{ $user->username }}</td>
																	</tr>
																	<tr>
																		<th scope="row">Tipo de Usuario: </th>
																		@switch($user->type)
																			@case(1)
																			<td>Administrador de Sistema</td>
																			@break
																			@case(2)
																			<td>Administrador de Institución</td>
																			@break
																			@case(3)
																			<td>Estudiante</td>
																			@break
	
																		@endswitch
																	</tr>
																</tbody>
															</table>
														</div>
													</div>
													<!-- end of table col-lg-6 -->
												</div>
												<!-- end of row -->
											</div>

											<br />

										</div>
										<!-- end of col-lg-12 -->
									</div>
									<!-- end of row -->
								</div>
								<center>
									<a style="color:white" onclick="returnURL('{{ url()->previous() }}')" class="btn btn-primary btn-adjust-mobile"><i class="icofont icofont-arrow-left"></i>Regresar</a>

								</center>
								<!-- end of view-info -->
							</div>
							<!-- end of card-block -->
						</div>
					</div>
					<!-- tab pane personal end -->
				</div>
				<!-- tab content end -->
			</div>
		</div>
	</div>
	<!-- Page-body end -->

	@endsection


	@section('javascriptcode')
	<script>
		var state=0;
		$(document).ready(function() {
			$("#permissions").hide();
			$("#see_permissions").click(function(){
				if(state==0){
					$("#permissions").show(400);
					state=1;
				}else{
					$("#permissions").hide(200);
					state=0;
				}
			});
		});
	</script>
	@endsection
