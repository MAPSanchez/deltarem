@extends('layouts.app')

@section('title',"Importar - SAE")

@section('style')

<style>
.nav{padding-left:0;margin-bottom:0;list-style:none}.nav>li{position:relative;display:block}.nav>li>a{position:relative;display:block;padding:10px 15px}.nav>li>a:hover,.nav>li>a:focus{text-decoration:none;background-color:#eee}.nav>li.disabled>a{color:#777}.nav>li.disabled>a:hover,.nav>li.disabled>a:focus{color:#777;text-decoration:none;cursor:not-allowed;background-color:transparent}
</style>

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/steps.css') }}" />

@endsection

@section('body')



<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card" style="margin-top: 0px;">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fas fa-file-import" style="background-color: #7491F0;"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Importar CSV</h4>
						<span style="text-transform: none;">Importe los registros de las opciones inferior mediante un archivo .csv</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Importar</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Design Wizard card start -->
				<div class="card">
					<div class="card-block">
						<section>
							<div class="wizard" style="background-color:transparent; margin-top: -40px;">
								<div class="wizard-inner">
									<div class="connecting-line"></div>
									<ul class="nav nav-tabs" role="tablist">

										<li id="tab-1" role="presentation" class="active" aria-expanded="false">
											<a id="a-1" href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Paso 1" class="active" aria-expanded="true">
												<span class="round-tab">
													<i class="fas fa-hand-pointer"></i>
												</span>
											</a>
										</li>

										<li id="tab-2" role="presentation" class="" aria-expanded="false">
											<a id="a-2" href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Paso 2" class="active" aria-expanded="true">
												<span class="round-tab">
													<i class="fas fa-pencil-alt"></i>
												</span>
											</a>
										</li>
										<li id="tab-3" role="presentation" class="">
											<a id="a-3" href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Paso 3" class="" aria-expanded="false">
												<span class="round-tab">
													<i class="fas fa-file"></i>
												</span>
											</a>
										</li>

										<li id="tab-4" role="presentation" class="">
											<a id="a-4" href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Finalizar" class="active" aria-expanded="true">
												<span class="round-tab">
													<i class="far fa-check-circle"></i>
												</span>
											</a>
										</li>
									</ul>
								</div>

								<form id="form" method="POST" action="{{ route('imports.store') }}" enctype="multipart/form-data">
									{{ csrf_field() }}
									<input type="text" name="actual_importation" id="actual_importation" hidden />
									<input type="text" name="importation_type" id="importation_type" hidden />
									<input type="text" name="table" id="table" hidden />
									<input type="text" name="second_table" id="second_table" hidden />
									<input type="text" name="ignore_password_value" id="ignore_password_value" hidden />
									<div id="content" style="margin:25px" class="tab-content">

										<div class="clearfix"></div>
									</div>
								</form>
							</div>
						</section>

					</div>
				</div>
				<!-- Design Wizard card end -->
			</div>
		</div>
	</div>
	<div hidden>
		<div class="tab-pane active" role="tabpanel" id="step1">
			<div class="form-group row" style="margin-top: -20px; margin-left:20px; margin-right:20px;">
				<h4><strong>Seleccione el tipo de importación que desea realizar:</strong></h4>
				<br />
			</div>
			<div class="page-body">
				<div class="row">
					<!-- card1 start -->
					<!-- user card start -->
					<div class="col-sm-6">
						<div class="card bg-c-yellow text-white widget-visitor-card" style="cursor:pointer">
							<div class="card-block-small text-center" id="import_students_btn">
								<h2 id="import_students_text" class="noselect">Estudiantes</h2>
								<br />
								<h6></h6>
								<i id="import_students_icon" class="fa fa-users"></i>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="card text-white widget-visitor-card" style="background-color:#FF5733" >
							<div class="card-block-small text-center" id="import_assignation_btn" style="cursor:pointer">
								<h2 id="import_assignation_text" class="noselect">Asignación</h2>
								<br />
								<h6></h6>
								<i id="import_assignation_icon" class="fa fa-link"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" role="tabpanel" id="step2">
			<div class="form-group row" style="margin-top: -20px; margin-left:20px; margin-right:20px;">
				<strong><h4>Instrucciones para importar <span style="font-weight: bold; font-size: 20px" id="instructions_title"></span></h4></strong>
			</div>
			<div class="form-group row" style="margin-left:20px; margin-right:20px;">
				<h6 id="instructions_text">instrucciones</h6>
			</div>
			<div style=padding-top:30px>
				<button id="tab_2_continue" style="float:right" type="button" class="btn btn-primary"><i id="tab_2_icon_continue" class="far fa-arrow-alt-circle-right"></i>Continuar</button>
				<button id="tab_2_return" style="float:left" type="button" class="btn btn-primary"><i id="tab_2_icon_return" class="far fa-arrow-alt-circle-left"></i>Regresar</button>
			</div>
		</div>
		<div class="tab-pane" role="tabpanel" id="step3">
				<div class="form-group row" style="margin-top: -20px; margin-left:20px; margin-right:20px;">
					<strong><h4>Adjunte el archivo CSV de los registros de <span style="font-weight: bold; font-size: 20px" id="tab_3_title"></span></h4></strong>
					<br />
				</div>
				<div id="ignore_passwords" style="margin-left:20px; ">
					<strong>*Nota </strong>
					<br />
					<span>Al momento de realizar una importación de este tipo de usuarios <strong>(estudiantes, profesores y usuarios en general)</strong> si en el CSV agregado se encuentra un usuario que ya está registrado en el sistema, <strong>todos</strong> sus datos van a ser actualizados <strong>incluyendo su contraseña,</strong> por lo que esta se puede perder en la importación, en caso de que solo desee agregar registros sin modificar la contraseña actual de los usuarios, active la siguiente opción.</span>
					<br /><br />
					<input id="ignore_password_checkbox" name="ignore_password" value="false" type="checkbox" />
					<label for="ignore_password">
						Ignorar las contraseñas de los usuarios de este CSV
					</label>
					<br /><br />
				</div>
				<div class="form-group row">
					<div class="col-sm-12">
						<div class="file-upload">
							<div class="image-upload-wrap">
								<input id="csv_input" class="file-upload-input" type='file' name="csv_input" onchange="readURLForImportation(this);" accept=".csv" />
								<div style="padding-top:40px" onclick="$('.file-upload-input').trigger('click' )">
									<center>
										<i style="font-size: 60px;" class="fas fa-cloud-upload-alt drag-icon"></i>
									</center>
								</div>
								<div class="drag-text">
									<span>Arrastre y suelte el archivo CSV <span style="font-weight: bold; font-size:16px;"> aquí</span> o haga clic <span style="font-weight: bold; font-size:16px;"> aquí</span> para buscarlo en su equipo.</span>
								</div>
							</div>
							<div class="file-upload-content">
								<img class="file-upload-image" src="#" alt="your image" />
								<div class="image-title-wrap">
									<button type="button" onclick="removeUpload()" class="remove-image">Remover CSV</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style=padding-top:30px>
					<button id="upload_file" style="float:right" type="submit" class="btn btn-success"><i id="upload_file_icon" class="icofont icofont-check-circled"></i>Mandar Archivo y Continuar</button>
					<button id="tab_3_return" style="float:left" type="button" class="btn btn-primary"><i id="tab_3_icon_return" class="far fa-arrow-alt-circle-left"></i>Regresar</button>
				</div>

		</div>
		<div class="tab-pane" role="tabpanel" id="step4">
			<div class="form-group row" style="margin-top: -20px; margin-left:20px; margin-right:20px;">
				<h4><strong>Resultados: </strong></h4>
			</div>
			<div class="form-group row" style="margin-left:20px; margin-right:20px;">
				<div class="dt-responsive table-responsive">
					<table style="width:100%" id="datatable_results" class="table table-striped table-bordered">
						<thead id="table_header">
							<tr>
								<th class="all" scope="col" style="width: 15%">Tipo</th>
								<th class="all" scope="col" style="width: 90%">Mensaje</th>
								<th class="none">Valores del Registro</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
				            <tr>
				                <th>Tipo</th>
				                <th>Mensaje</th>
				                <th>Valores del Registro</th>
				            </tr>
				        </tfoot>
					</table>
				</div>
			</div>
			<div style=padding-top:0px>
				<button id="tab_4_return" style="float:left" type="button" class="btn btn-primary"><i id="tab_4_icon_return" class="far fa-arrow-alt-circle-left"></i>Regresar al Inicio</button>
			</div>

		</div>
	</div>

	<!-- Modal -->
	<div id="loading_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div style="padding-top:9%" class="loader-block">
			 <svg id="loader2" viewBox="0 0 100 100">
			 <circle id="circle-loader2" cx="50" cy="50" r="45"></circle>
			 </svg>
	 	</div>
		<center>
			<h4 id="myModalLabel" style="color:white;">Cargando archivo .csv</h4>
			<h5 style="color:white;">Por favor espere...</h5>

		</center>
	</div>

	<!-- Page body end -->
	@endsection

	@section('javascriptcode')
	<script>
		var dt_reports;
		var wrong_csv=false;
		var actual_importation="";
		var importation_from_get="";
		@if(Request::get('type')!=null)
			importation_from_get="@php echo(Request::get('type')) @endphp";
		@endif
		@if($is_uploaded=="true")
			actual_importation="SI";
			var row_result = @php echo(json_encode($row_result)) @endphp;
			var results=true;
		@elseif($is_uploaded=="false")
			var results=false;
		@elseif($is_uploaded=="wrong")
			wrong_csv=true;
			actual_importation="@php echo($actual_importation) @endphp";
			var results=false;
		@endif
		if(importation_from_get!=""){
			switch(importation_from_get){
				case 'students':
					actual_importation="Estudiantes";
					break;
				case 'asignation':
					actual_importation="Asignacion";
					break;
			}
		}
		var instructions_for=[];
		instructions_for['Estudiantes']='<div class="row"><div class="col-sm-12 col-xl-12"><img src="{{asset('import_help/images/students.png')}}" class="img-fluid p-b-10 rounded"></div></div> <br><div class="row"> <div class="col-sm-6"> <p>Se necesita un archivo CSV, que contenga las siguientes columnas:</p> <ul> <li style="margin-left: 20px;"><strong> - </strong>Nombre_completo</li> <li style="margin-left: 20px;"><strong> - </strong>Matrícula</li> <li style="margin-left: 20px;"><strong> - </strong>Email</li> <li style="margin-left: 20px;"><strong> - </strong>Username</li> <li style="margin-left: 20px;"><strong> - </strong>Password</li> </ul> <br /> </div> <div class="col-sm-6"> <p><strong>Nombre completo:</strong> es el nombre completo del estudiante.</p> <p><strong>Matrícula:</strong> es la matrícula del estudiante.</p> <p><strong>Email:</strong> corresponde al correo electrónico del estudiante.</p> <p><strong>Username:</strong> es el nombre de usuario con el cual podrá ingresar el estudiante al sistema.</p> <p><strong>Password:</strong> es la contraseña que usará el estudiante para ingresar al sistema.</p> </div></div> <div class="row" style="margin-left:20px"> <br /><h6 class="m-b-30"><a target="_blank" href="{{asset('import_help/csv/students.csv') }}" download="sae_estudiantes.csv" ><i style="color:#3366BB" style="font-size:1px" class="fas fa-external-link-alt"></i>Descargar archivo csv de ejemplo</a></h6> </div> ';

        instructions_for['Asignacion']='<div class="row"> <div class="col-sm-12 col-xl-7"> <p><strong>NOTA: Esta importación requiere que los estudiantes y exámenes se encuentren dados de alta en el sistema.</strong></p> <div class="form-group"> <p>Se necesita un archivo CSV, que contenga las siguientes columnas:</p> <ul> <li style="margin-left: 20px;"><strong> - </strong>Id_examen</li> <li style="margin-left: 20px;"><strong> - </strong>Matrícula</li> </ul> <br /> <p><strong>Id_examen:</strong> es el identificador del examen registrado en el sistema.</p><p><strong>Matrícula:</strong> es la matrícula del estudiante que presentará el examen.</p> <br /><h6 class="m-b-30"><a target="_blank" href="{{asset('import_help/csv/asignacion.csv') }}" download="sae_asignacion.csv"><i style="color:#3366BB" style="font-size:1px" class="fas fa-external-link-alt"></i>Descargar archivo csv de ejemplo</a></h6> </div> </div> <div class="col-sm-12 col-xl-5"> <img src="{{asset('import_help/images/asignacion.png')}}" alt="CSV de Asignacion" class="img-fluid p-b-10 rounded" > </div> </div>';
		
		function resetClass() {
			$("#tab-1").removeClass('active');
			$("#tab-2").removeClass('active');
			$("#tab-3").removeClass('active');
			$("#tab-4").removeClass('active');
		}
		function first_tab(){
			actual_importation="";
			$("#actual_importation").val("");
			results=false;
			resetClass();
			$("#content").html('<div class="tab-pane active" role="tabpanel"' + $("#step1").html() + "</div>");
			$("#tab-1").addClass('active');
		}
		function second_tab(){
			if(actual_importation!=""){
				resetClass();
				$("#content").html('<div class="tab-pane active" role="tabpanel"' + $("#step2").html() + "</div>");
				$("#tab-2").addClass('active');
				if(actual_importation!=""){
                    if(actual_importation == "Asignacion"){
                        $("#instructions_title").text("Asignación");
                    }else{
                        $("#instructions_title").text(actual_importation);
                    }
					$("#instructions_text").html(instructions_for[actual_importation]);
				}
			}else{
				$("#content").html('<div class="tab-pane active" role="tabpanel"' + $("#step1").html() + "</div>");
				$("#tab-1").addClass('active');
			}
		}
		function third_tab(){
			if(actual_importation!=""){
				resetClass();
				$("#content").html('<div class="tab-pane active" role="tabpanel"' + $("#step3").html() + "</div>");
                $("#tab-3").addClass('active');
                
                if(actual_importation=="Asignacion"){
                    $("#tab_3_title").text("Asignación");
                }else{
                    $("#tab_3_title").text(actual_importation);
                }
				switch(actual_importation){
					case "Alumnos":
					case "Tutores":
					case "Usuarios":
						$("#ignore_passwords").show();
						$('#ignore_password_checkbox').click(function() {
					        if(this.checked) {
								$("#ignore_password_value").val("true");
					        }else{
								$("#ignore_password_value").val("false");
							}
					    });
				}
			}else{
				$("#content").html('<div class="tab-pane active" role="tabpanel"' + $("#step1").html() + "</div>");
				$("#tab-1").addClass('active');
			}
		}
		function fourth_tab(){
			if(actual_importation!="" && results==true){
				resetClass();
				$("#content").html('<div class="tab-pane active" role="tabpanel"' + $("#step4").html() + "</div>");
				$("#tab-4").addClass('active');
				dt_reports = $('#datatable_results').DataTable({
					columns: [
						{title: "Tipo"},
						{title: "Mensaje"},
						{title: "Valores del Registro"},
					],
					responsive: true,
					dom: 'frtip',
				});
				dt_reports.clear().draw();
				for (var i = 0; i < row_result.length; i++) {
					var siz=Object.keys(row_result[i][2])[0];
					var details_table_html='<table style="width:100%"><thead><tr>';
					for(var j=0;j<row_result[i][2][siz].length;j++){
						details_table_html=details_table_html+"<td>"+row_result[i][2][siz][String(j)]["1"]+"</td>";
					}
					details_table_html=details_table_html+'</tr></thead><tbody><tr>';
					for(var j=0;j<row_result[i][2][siz].length;j++){
						details_table_html=details_table_html+"<td>"+row_result[i][2][siz][String(j)]["0"]+"</td>";
					}
					details_table_html=details_table_html+'</tr></tbody></table>';
					type_html="";
					switch(row_result[i][0]){
						case 'Error':
							type_html='<i style="color:red" class="fas fa-times-circle"></i> Error';
							break;
						case 'Stays':
							type_html='<i style="color:blue" class="fas fa-info-circle"></i> Información';
							break;
						case 'Update':
							type_html='<i style="color:#e29528" class="fas fa-chevron-circle-up"></i> Actualización';
							break;
						case 'Correct':
							type_html='<i style="color:green" class="fas fa-check-circle"></i> Registrado';
							break;
                    }
                    
					dt_reports.row.add([
						type_html,
						row_result[i][1],
						details_table_html
					]).draw();
				}
			}else{
				$("#content").html('<div class="tab-pane active" role="tabpanel"' + $("#step1").html() + "</div>");
				$("#tab-1").addClass('active');
			}
		}
		function is_selected_importation_type(){
			if(actual_importation==""){
				swal({
		            icon: 'error',
		            title: 'No puede ingresar a la sección',
		            text: 'Para ingresar primero seleccione el tipo de importacion que desee realizar',
		            buttons: 'Aceptar',
		        })
			}
		}
		function is_updated_csv(){
			if(results==false){
				swal({
		            icon: 'error',
		            title: 'No puede ingresar a la sección',
		            text: 'Para ingresar primero carge y envie el archivo .csv al sistema',
		            buttons: 'Aceptar',
		        })
				third_tab();
				return false;
			}
			return true;
		}
		function after_uploading(){
			if(results==true){
				swal({
		            icon: 'error',
		            title: 'Sección no disponible',
		            text: 'Esta sección no esta disponible actualmente, termine o comience una nueva importación para acceder aqui',
		            buttons: 'Aceptar',
		        })
				fourth_tab();
			}
		}
		$(document).ready(function() {
			$("#ignore_passwords").hide();
			$("#ignore_password_value").val("false");
			if(!wrong_csv){
				if(results){
					fourth_tab();
				}else{
					if(importation_from_get!=""){
						second_tab();
					}else{
						first_tab();
					}
				}
			}else{
				swal({
		            icon: 'error',
		            title: 'Csv con formato incorrecto',
		            text: 'El archivo .csv que subió, no tiene el formato correcto. Intente subir el archivo nuevamente con el formato requerido para la importación de '+actual_importation.toLowerCase(),
		            buttons: 'Aceptar',
		        })
				third_tab();
			}
			$("#content").click(function(e){
				console.log(e.target.id);
				switch(e.target.id){
					case 'import_students_btn':
					case 'import_students_text':
					case 'import_students_icon':
                        actual_importation="Estudiantes";
                        $("#importation_type").val('Estudiantes');
						$("#actual_importation").val("Estudiantes");
						second_tab();
						break;
					case 'import_assignation_btn':
					case 'import_assignation_text':
					case 'import_assignation_icon':
                        actual_importation="Asignacion";
                        $("#importation_type").val('Asignacion');
						$("#actual_importation").val("Asignacion");
						second_tab();
						break;
					case 'tab_3_return':
					case 'tab_3_icon_return':
						second_tab();
						break;
					case 'upload_file':
					case 'upload_file_icon':
						//send_file();
						//fourth_tab();
						//results=true;
						break;
					case 'tab_2_return':
					case 'tab_2_icon_return':
						first_tab();
						actual_importation=""
						$("#actual_importation").val("");
						break;
					case 'tab_2_continue':
					case 'tab_2_icon_continue':
						third_tab();
						break;
					case 'tab_4_return':
					case 'tab_4_icon_return':
						first_tab();
						actual_importation=""
						$("#actual_importation").val("");
						results=false;
						break;
				}
			});
		});
		$("#form").submit(function(e){
			if(!$("#csv_input").val()){
				swal({
		            icon: 'error',
		            title: 'Archivo .csv requerido',
		            text: 'Por favor, cargue el archivo .csv para continuar',
		            buttons: 'Aceptar',
		        })
				e.preventDefault();
			}else{
				if($("#importation_type").val()==null){
					e.preventDefault();
				}else{
					$('#loading_modal').modal('show');
					switch(actual_importation){
						case 'Estudiantes':
							$("#importation_type").val('students');
							$("#table").val('users');
							$("#second_table").val('students');
							$("#form").submit();
							break;
						case 'Asignacion':
							$("#importation_type").val('assignation');
							$("#table").val('students');
							$("#second_table").val('');
							$("#form").submit();
							break;
					}
				}
			}
		});
		$("#tab-1").click(function() {
			first_tab();
		});
		$("#tab-2").click(function() {
			second_tab();
			is_selected_importation_type();
			after_uploading();
		});
		$("#tab-3").click(function() {
			third_tab();
			is_selected_importation_type();
			after_uploading();
		});
		$("#tab-4").click(function() {
			fourth_tab();
			is_selected_importation_type();
			is_updated_csv();
		});
		$("#a-1").click(function() {
			first_tab();
		});
		$("#a-2").click(function() {
			second_tab();
		});
		$("#a-3").click(function() {
			third_tab();
		});
		$("#a-4").click(function() {
			fourth_tab();
		});
	</script>
	@endsection