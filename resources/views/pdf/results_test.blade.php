<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
    @page { margin: 130px 25px; }
    header { position: fixed; top: -110px; left: 10px; right: 0px; height: 50px; }
    p { page-break-after: always; }
    p:last-child { page-break-after: never; }
    th{
      border-collapse: collapse;
      border:1px solid black;
      font-size:10px;
    }
    td{
      border-collapse: collapse;
      border:1px solid black;
      font-size:10px;
    }
  </style>
</head>
<body>
    <script type="text/php">
        if (isset($pdf)) {
            $text = "{PAGE_NUM} de {PAGE_COUNT}";
            $size = 10;
            $font = $fontMetrics->getFont("helvetica", "bold");
            $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
            $x = (($pdf->get_width() - $width) / 2)+270;
            $y = $pdf->get_height() - 35;
            $pdf->page_text($x, $y, $text, $font, $size);
        }
    </script>
  <main>
  <h6 style="font-size:16px; position: fixed; top:-80px; left:246px;"><strong>Sistema de Aplicación de Exámenes</strong></h6>
    <h6 style="font-size:14px; position: fixed; top:-55px; left:300px;"><strong>Resultados de Examen</strong></h6>
<br>
    <h6 style="font-size:12px; margin-top:-10px; margin-left:570px;"><strong>Fecha: </strong>@php
        $fecha_actual=date("d-m-Y");
        $date_split = explode("-", $fecha_actual);
        switch($date_split[1]){
          case '01':
              $date_split[1] = "Enero";
              break;
          case '02':
              $date_split[1] = "Febrero";
              break;
          case '03':
              $date_split[1] = "Marzo";
              break;
          case '04':
              $date_split[1] = "Abril";
              break;
          case '05':
              $date_split[1] = "Mayo";
              break;
          case '06':
              $date_split[1] = "Junio";
              break;
          case '07':
              $date_split[1] = "Julio";
              break;
          case '08':
              $date_split[1] = "Agosto";
              break;
          case '09':
              $date_split[1] = "Septiembre";
              break;
          case '10':
              $date_split[1] = "Octubre";
              break;
          case '11':
              $date_split[1] = "Noviembre";
              break;
          case '12':
              $date_split[1] = "Diciembre";
              break;
        }
        echo($date_split[0].' de '.$date_split[1].' del '.$date_split[2]); @endphp</h6>
     <br>
  
  <h6><small><strong>Información General</strong> </small></h6>
  <table class="table table-sm" style="width: 100%; border:0px solid black; border-collapse: collapse;">
    <tbody>
      <tr>
        <th style="width:20%"><small><strong>Institución Educativa:</strong></small></th>
        <td style="width:80%"><small> <center> {{$examen->name}}</center></small></td>
      </tr>
      <tr>
        <th><small> <strong>Exámen:</strong></small></th>
        <td><small> <center>{{$examen->description}}</center></small></td>
      </tr>
    </tbody>
  </table>
  <hr>

 
  <h6><small> <strong>Calificaciones de estudiantes que presentaron el examen</strong> </small></h6>
  <table class="table table-sm">
    <thead>
        <tr >
            <th style="width:65%"><small><center><strong>Estudiante</strong></center></small></th>
            <th style="width:35%"><small><center><strong>Promedio Obtenido</strong></center></small></th>
        </tr>
        </thead>
    <tbody>
    @foreach ($data as $c)
        <tr>
            <td><small><center>{{$c->full_name}}</center></small></td>
            <td> <small><center>@php echo number_format((float)$c->score,2,'.',',') @endphp</center></small></td>
        </tr>
    @endforeach
    </tbody>
  </table><hr>
        <br>

  </main>
</body>
</html>