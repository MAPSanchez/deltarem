<!DOCTYPE html>

<html lang="en-us" class="no-js">

<head>
	<meta charset="utf-8">
	<title>Página no encontrada</title>
	<meta name="description" content="No se encontró la página ingresada">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="SRT">

	<!-- Favicon -->
	<link rel="icon" href="{{ asset('assets/images/favicon.ico') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" type="image/x-icon">

	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

</head>

<body>
	<!-- Logo de la Universidad -->
	<a href="{{route('dashboard')}}" class="logo-link" title="back home">
        <img src="{{ asset('assets/images/auth/tamaulipas-2.png') }}" class="logo" style="width:150px;" alt="CG" />
    </a>

	<div class="content">
		<div class="content-box">
			<div class="big-content">

				<!-- Lineas del fondo -->
				<div class="list-line">
					<span class="line"></span>
					<span class="line"></span>
					<span class="line"></span>
					<span class="line"></span>
					<span class="line"></span>
					<span class="line"></span>
				</div>

				<!-- La lupa animada -->
				<i class="fas fa-search" aria-hidden="true"></i>

				<!-- div clearing the float -->
				<div class="clear"></div>

			</div>
			@yield('content')

		</div>

	</div>

	<footer class="blue">
		<ul>
			@yield('back')
		</ul>
	</footer>
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<!-- Mozaic plugin -->
	<script src="{{ asset('js/mozaic.js') }}"></script>
</body>
</html>
