@extends('errors.expired_session')

@section('title',"Sesión Expirada")

@section('content')
    <h1>Su sesión ha expirado<h1>

    <h2> por favor recargue la página e intente nuevamente.</h2>
    <br />
    <center>
        <a href="{{ URL::previous() }}" style="height:60px;width:200px;font-size:3rem;" class="btn btn-warning btn-lg">
            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
            Regresar
        </a>
    </center>
@endsection

@section('back')

@endsection
