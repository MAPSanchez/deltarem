@extends('errors.notfound')

@section('title',"Error")

@section('content')
    <h1>Ocurrió un error y la página no pudo ser mostrada</h1>
    <h3>Por favor, notifique al administrador del sistema sobre este problema.</h3>
    <br />
    <center>
        <a href="{{ URL::previous() }}" style="height:60px;width:200px;font-size:3rem;" class="btn btn-warning btn-lg">
            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
            Regresar
        </a>
    </center>
@endsection

@section('back')

@endsection
