@extends('layouts.app')

@section('title',"Listado de Asignaciones - SAE")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-link" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Listado de asignaciones</h4>
						<span style="text-transform: none;">Lista de todas las asignaciones de examenes realizadas en el sistema.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Listado de Asignaciones
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Zero config.table start -->
				<div class="card">
					<div class="card-block">
						<br>
						<div class="dt-responsive table-responsive">
							@if($asignaciones!=NULL)
								<table style="width:100%" id="asignaciones" class="table table-striped table-bordered">
									<thead id="table_header">
										<tr>
                                            <th class="all" scope="col" style="width:20%;">Matrícula</th>
											<th class="all" scope="col" style="width:30%;">Estudiante</th>
											<th scope="col" style="width:35%;">Examen</th>
											<th class="all" scope="col" style="width:15%;">Acciones</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							@else
								<center>
									<div class="alert alert-warning icons-alert" id="alert_div">
										<strong>Información</strong>
										<p>Actualmente no hay ninguna asignación registrada en el sistema.</p>
									</div>

								</center>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>

  	var data = @php echo(json_encode($asignaciones)); @endphp;
  	$("#asignaciones").DataTable({
		responsive: true,
		dom : 'frtip',
    data: data,
		language: {
			search: "Buscar:",
			searchPlaceholder: "Buscar en asignaciones..."
		},
		initComplete: function() {
			$('#asignaciones_filter').find(">:first-child").css('float','left');
			$('#asignaciones_filter').find(">:first-child").css('width','93%');
		}
	});


	function assign_destroy(url){

		swal({
			title: "Atención",
			text: "¿Está seguro de eliminar la asignación del examen?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
			buttons: ["Cancelar", "Eliminar"],
			closeModal: false
		}).then(function(result){
			if (result) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': '{{ csrf_token() }}',
					}
				});
				$.ajax({
					url: url,
					type: 'POST',
					data:{
							_method:"DELETE"
						},
					success: function(result) {
						swal({
							icon: 'success',
							title: 'Éxito',
							text: 'La asignación del examen fue eliminada correctamente.',
							buttons: 'Aceptar',
						});
						setTimeout(function(){
							location.reload();
						},900);
					}
				});
			}
		});
	}
</script>
@endsection
