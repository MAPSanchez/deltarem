@extends('layouts.app')

@section('title',"Listado de Exámenes - SAE")

@section('body')

<script>
	function errorImg(image){
		$(image).attr('src','{{ asset('assets/images/exams/exam.svg') }}')
	}
</script>
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-list-alt" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Listado de Exámenes Disponibles</h4>
						<span style="text-transform: none;">En esta lista se muestran todos los examenes que tiene disponibles para realizar.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Exámenes
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Zero config.table start -->

				@if(sizeof($available_tests)>0)
					@foreach($available_tests as $test)
						<div class="card list-view-media">
							<div class="card-block">
								<div style="margin-right:20px" class="media">
									<div class="media-left my-auto" style="margin-right:20px; margin-left:30px ">

										<img class="float-md-center mx-auto d-block user-img img-radius" style="margin:10px; margin-right: 40px; margin-left: 40px; max-height: 10vw" src="{{ asset($test->institution_logo_img) }}" onerror="errorImg(this);"/>
									</div>
									<div class="media-body my-auto">
										<div>
											<h5 class="d-inline-block">
												<b>{{$test->test_id}} - {{$test->description}}</b></h5>&nbsp&nbsp&nbsp
											<label style="font-size: 11px; margin-bottom:4px; text-transform: none;" class="label  label-primary">Duración: {{$test->total_time}}</label>
										</div>

										<hr />
										<div id="media_main_div" class="media">
											<div style="width:100%" class="media-left my-auto">

												<table style="width:100%;">
													<colgroup>
														<col span="1" style="width: 20%;">
														<col span="1" style="width: 80%;">
												   	</colgroup>
													<tr>
														<th style="text-align: left; color: #525252">Institución:</th>
														<th style="text-align: left"><span style="font-weight: bold; font-size: 17px;">{{$test->institution_name}}</span></th>
													</tr>
													<tr>
														<th style="text-align: left; color: #525252">Cantidad de Preguntas:</th>
														<th style="text-align: left"> <span style="font-weight: bold; font-size: 17px;">{{$test->number_of_questions}}</span></th>
													</tr>
													<tr>
														<th style="text-align: left; color:#525252">Competencias:</th>
														<th style="text-align: left"> <span style="font-weight: bold; font-size: 17px;">{{$test->areas}}</span></th>
													</tr>
												</table>
												<div id="hr_div">
													<br />
												</div>
											</div>
											<div id="media_main_right_button" class="media-right my-auto">

												@if($test->state==NULL)
													<a href="{{ route('student_test.take_test', ['id' => $test->test_application_id]) }}"><button style="font-size: 20px; font-size: 1.2vw; width:100%; font-family: helvetica" type="button" class="btn btn-success btn-adjust-mobile float-md-right exam-main-btn"><i class="icofont icofont-check"></i>Comenzar Examen</button></a>
												@else
													<a href="{{ route('student_test.take_test', ['id' => $test->test_application_id]) }}"><button style="font-size: 20px; font-size: 1.2vw; width:100%; font-family: helvetica" type="button" class="btn btn-warning btn-adjust-mobile float-md-right exam-main-btn"><i class="icofont icofont-check"></i>Continuar Examen</button></a>
												@endif
											</div>
										</div>
										<hr />

									</div>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<div class="card list-view-media">
						<div class="card-block">
							<center>
								<div class="alert alert-warning icons-alert" id="alert_div">
									<strong>Información</strong>
									<p>Actualmente no hay ningún examen disponible.</p>
								</div>
								<a style="color:white" onclick="returnURL('{{ url()->previous() }}')" class="btn btn-primary btn-adjust-mobile"><i class="icofont icofont-arrow-left"></i>Regresar</a>

							</center>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>

    if ($(window).width() < 992) {

        $("#media_main_div").addClass("row");
        $("#media_main_right_button").addClass("mx-auto");
		$("#media_main_right_button").css("width", "100%");
		$(".exam-main-btn").css("font-size","15px");
        $("#hr_div").show();
    }

</script>
@endsection
