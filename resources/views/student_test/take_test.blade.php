@extends('layouts.app')

@section('title',"Exámen:  - SAE")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-list-alt" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Antes de comenzar el examen</h4>
						<span style="text-transform: none;">Lee con cuidado las siguientes indicaciones </span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Preparativo examen
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>


	<!-- Page body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Basic Form Inputs card start -->
				<div class="card">
					<div class="card-block">

						<div id="starting_exam_div" class="page-body">
							<div class="row">
								<div class="col-md-12 col-xl-12 ">

									<div class="card-block user-detail-card">
										<h4 class="sub-title">Información del examen </h4>
										<div class="row">

											<div class="col-sm-12 user-detail">
												<div class="row">
													<div class="col-sm-2">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-cube"></i>Id de Examen:</h6>
													</div>
													<div class="col-sm-2">
														<h6 class="m-b-30"><strong>{{$test->test_id}}</strong></h6>
													</div>
													<div class="col-sm-2">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-clipboard-list"></i>Nombre:</h6>
													</div>
													<div class="col-sm-6">
														<h6 class="m-b-30"><strong>{{$test->description}}</strong></h6>
													</div>
												</div>


											</div>
											<div class="col-sm-12 user-detail">
												<div class="row">
													<div class="col-sm-2">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-cube"></i>Competencias:</h6>
													</div>
													<div class="col-sm-10">
														<h6 class="m-b-30"><strong>{{$test->areas}}</strong></h6>
													</div>
												</div>
											</div>
										</div>
										<hr />
										Lee todas las preguntas detenidamente y selecciona una de las respuestas que considere correcta, posteriormente haz clic en el botón siguiente. El tiempo total de su examen es <b>{{$test->total_time}}</b> para contestar un total de <b>{{$test->number_of_questions}}</b> preguntas, los cuales se dividen entre las siguientes secciones:
										<br /><br />
										<div  class="row">
											<div class="col-sm-3">

											</div>
											<div class="col-sm-6">
												<table style="width:100%;" id="simple_datatable" class="requisition-table table table-striped table-bordered">
													<thead id="table_header">
														<tr>
															<th class="all" scope="col" style="width:80%;"><center>Nombre</center></th>
															<th scope="col" style="width:20%;">Tiempo</th>
														</tr>
													</thead>
													<tbody>
														@foreach($areas as $area)
															<tr>
																<td><center>{{$area->description}}</center></td>
																<td><center>{{$area->time}} minutos</center></td>
															</tr>
														@endforeach
													</tbody>
												</table>
											</div>
											<div class="col-sm-3">

											</div>
										</div>


										<table>
											<tr>
												<th>

												</th>
												<th>

												</th>
											</tr>
										</table>
										<br />
										<br />
										<ul style="font-weight:bold">
											<li>1. Asegúrate que tus datos son los correctos.</li>
											<li>2. Las preguntas tienen cuatro opciones de respuesta, indicadas con las letras A, B, C y D; sólo una es la respuesta correcta.</li>
											<li>3. Lee cuidadosamente cada pregunta y elige la respuesta que consideres correcta.</li>
											<li>4. El uso de la calculadora, material físico o electrónico está prohibido.</li>
											<li>5. Es importante contar con una buena conexión a Internet y un explorador actualizado: Chrome, Mozilla Firefox o Safari.</li>
											<li>6. Su tiempo por área comenzara a correr una vez inicie alguna de ellas.</li>
										</ul>
										<br />
									</div>

									<center>
										<a style="color:white" onclick="returnURL('{{ url()->previous() }}')" class="btn btn-primary btn-adjust-mobile"><i class="icofont icofont-arrow-left"></i>Regresar</a>
										<a href="{{ route('student_test.areas.list', ['id' => $test->test_application_id]) }}"><button type="button" class="btn btn-success btn-adjust-mobile"><i class="icofont icofont-check"></i>Iniciar Examen</button></a>
									</center>
								</div>
							</div>
						</div>

						<div id="taking_exam_div" class="page-body">
							<div class="row">
								<div class="col-md-12 col-xl-12 ">
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>
	//$("#starting_exam_div").hide();
</script>
@endsection
