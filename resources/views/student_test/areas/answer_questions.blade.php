@extends('layouts.app')

@section('title',"{$test->description}  - {$area->description} - SAE")

@section('body')
<script>
    function errorImg(image){
        $(image).attr('src','{{ asset('assets/images/white.png') }}')
    }

    function zoomImage(){
        $("#modal_img").click();
    }
</script>

<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div style="padding:5px" class="page-header card">

		<div class="card-block">
			<div id="media_div_top" class="media">
				<div style="width:95%" class="media-left my-auto">
					<div id="align_items_end_div" class="row align-items-end">
						<div style="width:100%" class="page-header-title">
                            <div>
                                <img style="margin-left:25px; margin-right:25px; width:7vh" src="{{url($institution->logo_img)}}"  />
                            </div>

							<div class="d-inline" >
								<h4 style="text-transform: none;">{{$test->description}}  - {{$area->description}}</h4>
								<span style="text-transform: none; color:#4d4d4d">Institucion:<b>  {{$institution->name}} </b> - Estudiante: <b>{{$student->full_name}}</b> - CURP: <b>{{$student->CURP}}</b></span>
							</div>
						</div>

					</div>
				</div>

				<div id="media_right_div_time_top" style="width: 10%; text-align: center" class="media-right my-auto">
                    <div id="hr_div">
                        <hr />
                    </div>
					<span id="remaining_time_span_title" style="font-size:0.68vw">Tiempo restante: <span><br />
					<span id="remaining_time_span" style="text-align: center; font-size:1.7vw">...</span></h3>
				</div>
			</div>
		</div>
	</div>


	<!-- Page body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Basic Form Inputs card start -->
				<div class="card">
                    <div class="row">
                        <div class="col-sm-3" >
                            <div class="card-block"  style="margin-top: 11px; margin-right: 0px; min-width: 150px ">
                                <center>
                                    <h4 class="sub-title">Navegación</h4>
                                    <div id="nav_items_container">
                                    </div>
                                </center>
                            </div>
                        </div>
                        <div class="col-sm-9" >
                            <div id="main_div_question" style=" margin-right:1%; margin-top:0.8%;  width:98%; height:23vh; max-height:25vh; overflow:auto;" class="card-block">
        						<h4 class="sub-title">Pregunta <span id="actual_question">0</span> / <span id="total_questions">0</span></h4>
        						<div class="row">
        							<div style="font-size:15.5px" class="col-sm-12">
                                        <span id="question_txt_span">Cargando...</span>
        							</div>
        						</div>
        						<br /><br />
                            </div>
                            <div style="margin-bottom: 25px">
        						<div id="image_secondary_container" style="margin-left:1%;  width: 98; min-height: 30vh; max-height: 50vh; height:33vh; overflow-y:auto" class="row">
        							<div id="answers_div" class="col-sm-5">
        								<br />
        								<div style="margin-left:2%" class="form-radio">
        									<ul>
        										<li>
        											<div class="radio radio-inline">
        												<label>
        													<input type="radio" id="rbtn_0" name="radio" class="ans_rb" value="0">
        													<i class="helper"></i><span id="rbtn_txt_0"></span>
        												</label>
        											</div>
        										</li>
        										<li>
        											<div class="radio radio-inline">
        												<label>
        													<input type="radio" id="rbtn_1" name="radio" class="ans_rb" value="1">
        													<i class="helper"></i><span id="rbtn_txt_1"></span>
        												</label>
        											</div>
        										</li>
        										<li>
        											<div class="radio radio-inline">
        												<label>
        													<input type="radio" id="rbtn_2" name="radio" class="ans_rb" value="2">
        													<i class="helper"></i><span id="rbtn_txt_2"></span>
        												</label>
        											</div>
        										</li>
        										<li>
        											<div class="radio radio-inline">
        												<label>
        													<input type="radio" id="rbtn_3" name="radio" class="ans_rb" value="3">
        													<i class="helper"></i><span id="rbtn_txt_3"></span>
        												</label>
        											</div>
        										</li>
        									</ul>
        								</div>
                                        <br />
                                        <br />


        							</div>
        							<div id="image_div" style="padding-right:4%" class="col-sm-5">
        								<div id="image_div_container" style="overflow-y:auto; overflow-x:auto; max-height:17vw">
                                            <div style="overflow:hidden; width:100%" class="container" >
        									    <img id="modal_img" title="Haga clic en la imagen para verla con mayor detalle" style="cursor:zoom-in; float:right; width:100%; height:100%; object-fit: cover; overflow: hidden;" src="{{ asset('assets/images/white.png') }}" onerror="errorImg(this);" />
                                                <a style="display: inline-block; text-align:  center;  margin-left: 41%; margin-right: 41%; margin-top:10px; font-size:10px; " href="#" onclick="zoomImage()"><i class="fas fa-search-plus"></i>&nbspAmpliar&nbspImagen</i></a>
                                            </div>
                                            <div id="modal_show_img" class="modal" style="overflow:auto">
        										<span class="close">&times;</span>
        										<img class="modal-content" style="margin-left:20%; margin-right:20%; margin-top:7%;display: block;
                                                      max-width:100%;
                                                      width: 60%;" id="img_content">
        										<div id="caption"></div>
        									</div>
        								</div>

        							</div>
        						</div>


        					</div>
                        </div>

                    </div>

                    <div style="margin-bottom: 25px; z-index:9999">
                        <center>
                            <button id="back_btn" type="button" class="btn btn-softblue btn-adjust-mobile"><i class="icofont icofont-arrow-left"></i>Anterior</button>
                            <button id="next_btn" type="button" style="margin-bottom:0.5px" class="btn btn-success btn-adjust-mobile">&nbsp Siguiente<i class="icofont icofont-arrow-right"></i></button>
                            <button id="end_btn" type="button" style="margin-bottom:0.5px" class="btn btn-olive btn-adjust-mobile"><i class="fas fa-check"></i>&nbsp Finalizar Etapa</button>

                        </center>

                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('style')
    <style>
        @media only screen and (max-width: 992px) {
            #back_btn{

            }
            #media_right_div_time_top {
                width: 100% !important;
            }
            #remaining_time_span_title {
                font-size: 10px !important;
            }
            #remaining_time_span{
                font-size: 25px !important;
            }
        }
    </style>

@endsection

@section('javascriptcode')
<script>

var actual_question_position=0;
var questions = [];
var actual_answer;
var saved_count=0;
var total_answered=0;

var vh = $(window).height()/100;
var original_answers_height = $("#image_secondary_container").height();

get_questions_list();

function fill_nav_questions(){
    var final_str="";
    for(var i=0;i<questions.length;i++){
        final_str=final_str+'<div id="nav_'+i+'" class="nav_btn" style="min-width: 30px; margin:1px;  text-align: center; display: inline-block; width:2vw; border: 1px solid #303548; cursor: pointer; ">&nbsp'+(i+1)+'&nbsp</div>';
    }

    final_str=final_str+'<div id="nav_end" class="nav_btn_end" style="min-width: 85px; margin:1px;  text-align: center; display: inline-block; width:2vw; border: 1px solid #303548; cursor: pointer; "><i class="fas fa-check"></i>&nbspFinalizar&nbsp</div>';

    $("#nav_items_container").html(final_str);

    $("#nav_end").hide()
    $(".nav_btn").click(function(){
        $("#end_btn").hide();
        $("#next_btn").prop('disabled',false);
        $("#back_btn").prop('disabled',false);
        $("#back_btn").prop('disabled',false);

        var new_question_number=$(this).attr('id').split("_")[1];

        if(new_question_number!="end"){
            save_answer(actual_question_position);

            actual_question_position=parseInt(new_question_number);

            if(actual_question_position==0)
                $("#back_btn").prop('disabled',true);
            else if(actual_question_position==questions.length-1){
                $("#next_btn").prop('disabled',true);
            }

            load_question(actual_question_position);
            $("#actual_question").text(actual_question_position+1);

            update_nav_questions();
        }
    });

    $("#nav_end").click(function(){
        end_area();
    });
}

function update_nav_questions(){
    for(var i=0;i<questions.length;i++){
        if(questions[i]['actual_student_answer']!=null)
            $("#nav_"+i).addClass("nav_btn_answered");
    }

    if(total_answered==questions.length){

        $("#nav_end").show();
    }

    $(".nav_btn").removeClass("nav_btn_active");
    $("#nav_"+actual_question_position).addClass("nav_btn_active");
    $("#nav_"+actual_question_position).removeClass("nav_btn_answered");
}


$(document).ready(function() {
    $("#hr_div").hide();

    if ($(window).width() >= 992) {
        $("#mobile-collapse").click();

    }else{
        $("#image_secondary_container").css("height","40vh");
        $("#image_div_container").css("height","10vh");
        $("#image_secondary_container").css("max-height","40vh");
        $("#image_div_container").css("max-height","10vh");

        $("#media_div_top").addClass("row");
        $("#align_items_end_div").removeClass("align-items-end");

        $("#hr_div").show();

        $(".modal-content").css("margin-right","5%");
        $(".modal-content").css("margin-left","5%");
        $(".modal-content").css("width","90%");
    }

    $("#back_btn").prop("disabled",true);
    $("#end_btn").hide();


    for(var i=0;i<4;i++){
        $("#rbtn_txt_"+i).click(function(){
            radiobtn();
        });

        $("#rbtn_"+i).click(function(){
            radiobtn();
        });
    }

    function radiobtn(){
        if(total_answered==(questions.length-1)){
            save_answer(actual_question_position);
            //total_answered+=1;
            get_questions_list();
        }else{
            $("#end_btn").hide();
        }
    }
    $("#back_btn").click(function(){
        $("#end_btn").hide();
        $("#next_btn").prop('disabled',false);
        $("#back_btn").prop('disabled',false);


        save_answer(actual_question_position);

        if(actual_question_position-1>=0)
            actual_question_position-=1;
        else
            actual_question_position=0;

        if(actual_question_position==0)
            $("#back_btn").prop('disabled',true);

        load_question(actual_question_position);
        $("#actual_question").text(actual_question_position+1);

        update_nav_questions();
    });

    $("#next_btn").click(function() {
        $("#back_btn").prop('disabled',false);

        save_answer(actual_question_position);

        if(actual_question_position+1<questions.length)
            actual_question_position+=1;
        else
            actual_question_position=questions.length-1;

        if(actual_question_position==questions.length-1){
            $("#next_btn").prop('disabled',true);
        }

        load_question(actual_question_position);
        $("#actual_question").text(actual_question_position+1);

        update_nav_questions();
    });

    $("#end_btn").click(function(){
        end_area();
    });
});

function end_area(){
    save_answer(actual_question_position);
    swal({
        title: "¿Está seguro de finalizar esta etapa de su examen? ",
        text: "Al confirmar esta acción ya no podrá realizar ningún cambio a sus respuestas. ",
        icon: "warning",
        buttons: true,
        dangerMode: false,
        buttons: ["Cancelar","Aceptar"],
        closeModal: false
    }).then((result) => {
        if (result) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });

            $.ajax({
                url: '{{ route('student_test.end_area') }}',
                method: 'post',
                data: {
                    id_area: {{$id_area}},
                    test_application_id: {{$test_application_id}}
                },
                success: function(result) {
                    location.reload();
                }
            });
        }
    });
}

function save_answer(question_number){
    actual_answer=$('.ans_rb:checked').val();


    if(actual_answer!=""){
        if(questions[question_number]['actual_student_answer']==actual_answer){
            return false;
        }else{
            questions[question_number]['actual_student_answer']=actual_answer;
        }

        $.ajaxSetup({
    		headers: {
    			'X-CSRF-TOKEN': '{{ csrf_token() }}'
    		}
    	});
    	$.ajax({
    		url: '{{ route('student_test.save_answer') }}',
    		method: 'post',
    		data: {
                id_area: {{$id_area}},
                actual_answer: actual_answer,
                id_question: questions[question_number],
                test_application_id: {{$test_application_id}}
    		},
    		success: function(result) {
                response = result['response'];

                if(response=="finished_time"){
                    window.location.replace("{{route('student_test.finish_time', ['id'=>$test_application_id, 'id_area'=>$id_area])}}");
                }
                saved_count+=1;
                total_answered+=1;
                if(saved_count>=3){
                    get_questions_list(false);
                    saved_count=0;


                }

                if(total_answered==questions.length){
                    $("#nav_end").show();
                }
                if(response=="error"){
                    console.log("Some error happened");
                }
    		}
    	});
    }

    return true;
}

function get_questions_list(update_question=true){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': '{{ csrf_token() }}'
		}
	});
	$.ajax({
		url: '{{ route('student_test.get_questions_list') }}',
		method: 'post',
		data: {
			id_area: {{$id_area}},
            test_application_id: {{$test_application_id}}
		},
		success: function(result) {
            questions = result['response'];
            fill_nav_questions();

            update_nav_questions();

            if(update_question){
                load_question(actual_question_position);

                $("#actual_question").text(actual_question_position+1);
                $("#total_questions").text(questions.length);
            }

            total_answered=0;
            for(var i=0;i<questions.length;i++){
                if(questions[i]['actual_student_answer']!=null){
                    total_answered+=1;
                }
            }

            if(total_answered==questions.length){
                $("#end_btn").show();
                $("#nav_end").show();
            }

		}
	});
}
var new_height=0;
function load_question(question_number){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': '{{ csrf_token() }}'
		}
	});
	$.ajax({
		url: '{{ route('student_test.get_question_info') }}',
		method: 'post',
        data: {
            id_question: questions[question_number]['id'],
            id_area: {{$id_area}},
            test_application_id: {{$test_application_id}}
        },
		success: function(result) {
            var question = result['response'];

            
            $("#question_txt_span").html(question['question_txt']);

            for(var i=0;i<4;i++){
                $("#rbtn_txt_"+i).html(question['answers'][i]['text']);
            }

            if(question['actual_student_answer']!=null){
                $("#rbtn_"+Number(question['actual_student_answer'])).prop("checked", true);
            }else{
                for(var i=0;i<4;i++){
                    $("#rbtn_"+i).prop("checked", false);
                }
            }

            if(question['img_src']==null){
                $("#answers_div").removeClass("col-sm-7");
                $("#image_div").removeClass('col-sm-5');
                $("#answers_div").addClass("col-sm-11");
                $("#image_div").addClass("col-sm-1");

                $("#image_div").hide();

                $("#modal_img").attr('src', "");
            }else{
                $("#answers_div").removeClass("col-sm-11");
                $("#image_div").removeClass("col-sm-1");
                $("#answers_div").addClass("col-sm-7");
                $("#image_div").addClass('col-sm-5');

                $("#image_div").show();

                $("#modal_img").attr('src', base_img_url+question['img_src']);

                modal_img_function();
            }

            $("#image_secondary_container").css('height', original_answers_height);
            if($("#answers_div").height()>33*vh){
                $("#image_secondary_container").css('height', $("#answers_div").height());
            }else{
                console.log("aqui");
                $("#image_secondary_container").css('height', original_answers_height);
            }
            /*

            if($("#answers_div").height()>33*vh){
                //changed_height=true;
                $("#image_secondary_container").css('height', $("#answers_div").height());
                new_height=$("#answers_div").height();
            }else{

                $("#image_secondary_container").css('height', original_answers_height);


                //console.log("aqui");
                //changed_height=false;
            }
            //answers_div
            //image_secondary_container*/
            console.log("-------");
            console.log($("#answers_div").height());
            console.log($("#image_secondary_container").height());

            console.log(original_answers_height);
                    console.log(33*vh);

		}
	});
}

function timer_interval(){

    if(remaining_time<=0){
        clearInterval(timer);
        window.location.replace("{{route('student_test.finish_time', ['id'=>$test_application_id, 'id_area'=>$id_area])}}");
    }else{
        remaining_time=remaining_time-1000;
        elapsed_seconds+=1;

        $("#remaining_time_span").text(milliseconds_to_datetime_str(remaining_time));
    }
}

var current_datetime;       //get from server
var timer;
exam_started_datetime = new Date('{{$started_datetime}}');      //minutes
var remaining_minutes, remaining_time;
$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': '{{ csrf_token() }}'
	}
});

var elapsed_seconds=0;
$.ajax({
	url: '{{ route('util.get_current_time') }}',
	method: 'post',
	success: function(result) {

		current_datetime = new Date(result['response'])
		limit_datetime = new Date(result['response']);

        remaining_minutes= {{$area->time}}-(current_datetime-exam_started_datetime)/1000/60;

		limit_datetime.setTime(current_datetime.getTime() + remaining_minutes*60000);

		remaining_time = limit_datetime-current_datetime;


        timer = setInterval(timer_interval , 1000);

	}
});


function milliseconds_to_datetime_str(duration) {
	var milliseconds = parseInt((duration % 1000) / 100),
		seconds = Math.floor((duration / 1000) % 60),
		minutes = Math.floor((duration / (1000 * 60)) % 60),
		hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

	hours = (hours < 10) ? "0" + hours : hours;
	minutes = (minutes < 10) ? "0" + minutes : minutes;
	seconds = (seconds < 10) ? "0" + seconds : seconds;

	return hours + ":" + minutes + ":" + seconds;
}

</script>
@endsection
