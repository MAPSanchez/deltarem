@extends('layouts.app')

@section('title',"Listado de Competencias - SAE")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-list-alt" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Examen - Competencias</h4>
						<span style="text-transform: none;">Lista de todas las competencias que debe realizar en este examen.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Exámenes
						</li>
						<li class="breadcrumb-item">Competencias
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Zero config.table start -->
				<div class="card">
					<div class="card-block">
						<div class="dt-responsive table-responsive">
							@if($areas_data!=NULL)
								<table style="width:100%" id="areas" class="table table-striped table-bordered">
									<thead id="table_header">
										<tr>
											<th class="all" scope="col" style="width:5%;">Numero</th>
											<th scope="col" style="width:15%;">Nombre</th>
											<th scope="col" style="width:7%;">Preguntas</th>
											<th scope="col" style="width:7%;">Tiempo Total</th>
											<th scope="col" style="width:7%;">Estado</th>
											<th class="all" scope="col" style="width:3%;">Acciones</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							@else
								<center>
									<div class="alert alert-warning icons-alert" id="alert_div">
										<strong>Atención</strong>
										<p>Ha ocurrido un error, por favor notifique al administrador del sistema.</p>
									</div>

								</center>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>

  	var data = @php echo(json_encode($areas_data)); @endphp;
  	$("#areas").DataTable({
		responsive: true,
		dom : 'rtip',
    data: data,
		aaSorting: [ [2,'asc'] ],
		language: {
			search: "Buscar:",
			searchPlaceholder: "Buscar en exámenes..."
		},
		initComplete: function() {
			$('#examenes_filter').find(">:first-child").css('float','left');
			$('#examenes_filter').find(">:first-child").css('width','93%');
		}
	});



</script>
@endsection
