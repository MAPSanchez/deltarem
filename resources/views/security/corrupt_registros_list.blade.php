@extends('layouts.app')

@section('title',"Registros corruptos del Libro: {$id} - SRT")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-book" style="min-width:50px; background-color:#43B4E4"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Registros corruptos del Libro {{$id}}</h4>
						<span style="text-transform: none;">Listado de todos los registros corruptos del libro seleccionado.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">
							<a href="{{ route('security.list')}}">
								Integridad de Libros
							</a>

						</li>
            <li class="breadcrumb-item">Registros corruptos de Libro
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Zero config.table start -->
				<div class="card">
					<div class="card-block">
            <div class="row">

            </div>
          @if($libro->estado == 0 && $porcentaje <100)
               <br>
                @endif
          <div class="row">
              <div class="col-sm-12">

                 <center>
                    <div class="alert alert-primary icons-alert" id="alert_div">
                        <strong>Información</strong>
                        <p>Los registros que aparecen en la parte inferior han sido modificados de alguna manera, por lo que no son confiables.</p>
                    </div>
                </center>
              </div>
            </div>
						<div class="dt-responsive table-responsive">
								@if($registros!=NULL)
                    <table style="width:100%" id="registros" class="table table-striped table-bordered">
                        <thead id="table_header">
                            <tr>
                                <th class="all" scope="col">Foja</th>
                                <th scope="col">Folio</th>
                                <th scope="col">Nombre Egresado</th>
                                <th scope="col">Fecha de Examen</th>
                                <th scope="col">IES</th>
                                <th scope="col">Profesión</th>
                            </tr>
                        </thead>
                        <tbody></tbody>

                    </table>
                @else
              <br>
                    <center>
                        <div class="alert alert-warning icons-alert" id="alert_div">
                            <strong>Información</strong>
                            <p>Actualmente no hay ningún registro del libro seleccionado.</p>
                        </div>
                    </center>
                @endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>

  var data = @php echo(json_encode($registros)); @endphp;


  $("#registros").DataTable({
		responsive: true,
		dom : 'frtip',
    data: data,
		aaSorting: [ [0,'asc'] ],
		language: {
			search: "Buscar:",
			searchPlaceholder: "Buscar en registros del libro..."
		},
		initComplete: function() {
			$('#registros_filter').find(">:first-child").css('float','left');
			$('#registros_filter').find(">:first-child").css('width','93%');
		}
	});


</script>
@endsection
