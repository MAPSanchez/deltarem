@extends('layouts.app')

@section('title',"Integridad de Libros - SRT")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-book" style="min-width:50px; background-color:#43B4E4"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Integridad de Libros</h4>
						<span style="text-transform: none;">Lista de los libros que tienen registros corruptos en el sistema.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Integridad de Libros
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Zero config.table start -->
				<div class="card">
					<div class="card-block">
						<div class="dt-responsive table-responsive">
							@if($libros!=NULL)
								<table style="width:100%" id="libros" class="table table-striped table-bordered">
									<thead id="table_header">
										<tr>
											<th class="all" scope="col" style="width:10%;">Libro</th>
											<th scope="col">Estado</th>
											<th scope="col">Porcentaje de Uso</th>
											<th scope="col">Capturista Responsable</th>
											<th scope="col">Problema Encontrado</th>
											<th class="all" scope="col" style="width:18%;">Acciones</th>
										</tr>
									</thead>
									<tbody></tbody>
								</table>
							@else
								<center>
									<div class="alert alert-warning icons-alert" id="alert_div">
										<strong>Información</strong>
										<p>Actualmente no hay ningún libro en el sistema que tenga algun problema con sus registros.</p>
									</div>

								</center>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>

  	var data = @php echo(json_encode($libros)); @endphp;
  	$("#libros").DataTable({
		responsive: true,
		dom : 'frtip',
    data: data,
		aaSorting: [ [2,'asc'] ],
		language: {
			search: "Buscar:",
			searchPlaceholder: "Buscar en Libros..."
		},
		initComplete: function() {
			$('#libros_filter').find(">:first-child").css('float','left');
			$('#libros_filter').find(">:first-child").css('width','93%');
		}
	});
</script>
@endsection
