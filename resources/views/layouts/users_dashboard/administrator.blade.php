
<ul class="pcoded-item pcoded-left-item">

   <li id="dashboard_li" class="">
        <a href="{{ route('dashboard') }}">
            <span style="background-color: #7491F0" class="pcoded-micon"><i style="padding-top: 13%" class="fas fa-home"></i><b>D</b></span>
            <span class="pcoded-mtext" data-i18n="nav.chat.main">Principal</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
    
    <li id="dashboard_li" class="">
        <a href="{{ route('ies.list') }}">
            <span style="background-color: #7491F0" class="pcoded-micon"><i style="padding-top: 13%" class="fas fa-university"></i><b>D</b></span>
            <span class="pcoded-mtext" data-i18n="nav.chat.main">Instituciones Educativas</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>

    <li id="dashboard_li" class="">
        <a href="{{ route('administrators.list') }}">
            <span style="background-color: #7491F0" class="pcoded-micon"><i style="padding-top: 13%" class="fas fa-users-cog"></i><b>D</b></span>
            <span class="pcoded-mtext" data-i18n="nav.chat.main">Administradores de Exámenes</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>
   


    <br /><br /><br /><br /><br />


</ul>
</div>
</nav>
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                @yield('body')
            </div>
        </div>
    </div>
</div>
