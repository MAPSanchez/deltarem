
<ul class="pcoded-item pcoded-left-item">

   <li id="dashboard_li" class=" {{ explode('.', $view_name)[0]=='home' ? 'active pcoded-trigger' : '' }}">
        <a href="{{ route('dashboard') }}">
            <span style="background-color: #7491F0" class="pcoded-micon"><i style="padding-top: 13%" class="fas fa-home"></i><b>D</b></span>
            <span class="pcoded-mtext" data-i18n="nav.chat.main">Principal</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>

    <li id="tests_li" class=" {{ explode('.', $view_name)[0]=='student_test.list' ? 'active pcoded-trigger' : '' }}">
        <a href="{{ route('student_test.list') }}">
            <span style="background-color: #7491F0" class="pcoded-micon"><i style="padding-top: 13%" class="fas fa-pencil-alt"></i><b>D</b></span>
            <span class="pcoded-mtext" data-i18n="nav.chat.main">Exámenes</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>

    <li id="tests_li" class=" {{ explode('.', $view_name)[0]=='student_test.list' ? 'active pcoded-trigger' : '' }}">
        <a href="{{ route('student_test.list') }}">
            <span style="background-color: #7491F0" class="pcoded-micon"><i style="padding-top: 13%" class="fas fa-list-alt"></i><b>D</b></span>
            <span class="pcoded-mtext" data-i18n="nav.chat.main">Historial de Examenes</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>

    <br /><br /><br /><br /><br />

</ul>
</div>
</nav>
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                @yield('body')
            </div>
        </div>
    </div>
</div>
