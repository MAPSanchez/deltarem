
<ul class="pcoded-item pcoded-left-item">

<li id="dashboard_li" class="">
     <a href="{{ route('dashboard') }}">
         <span style="background-color: #7491F0" class="pcoded-micon"><i style="padding-top: 13%" class="fas fa-home"></i><b>D</b></span>
         <span class="pcoded-mtext" data-i18n="nav.chat.main">Principal</span>
         <span class="pcoded-mcaret"></span>
     </a>
 </li>
 
 <li id="dashboard_li" class="">
     <a href="{{ route('test.list') }}">
         <span style="background-color: #7491F0" class="pcoded-micon"><i style="padding-top: 13%" class="fas fa-list-alt"></i><b>D</b></span>
         <span class="pcoded-mtext" data-i18n="nav.chat.main">Exámenes</span>
         <span class="pcoded-mcaret"></span>
     </a>
 </li>

 <li id="dashboard_li" class="">
     <a href="{{ route('students.list') }}">
         <span style="background-color: #7491F0" class="pcoded-micon"><i style="padding-top: 13%" class="fas fa-users"></i><b>D</b></span>
         <span class="pcoded-mtext" data-i18n="nav.chat.main">Estudiantes</span>
         <span class="pcoded-mcaret"></span>
     </a>
 </li>

 <li id="users_li" class="pcoded-hasmenu {{ explode('.', $view_name)[0]=='assign' ? 'active pcoded-trigger' : '' }}">
        <a href="javascript:void(0)">
            <span style="background-color: #7491F0"  class="pcoded-micon"><i style="padding-top: 13%" class="fa fa-link"></i></span>
            <span class="pcoded-mtext" data-i18n="nav.social.main">Asignación</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ Route::currentRouteNamed('assign.list') ? 'active' : '' }}">
                <a href="{{ route('assign.list') }}">
                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                    <span class="pcoded-mtext" data-i18n="nav.social.fb-wall">Listado de Asignaciones Realizadas</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="">
                <a id="asignar_test_to_student_btn" href="#">
                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                    <span class="pcoded-mtext" data-i18n="nav.social.fb-wall">Asignar Examen a Estudiante</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li id="users_li" class="pcoded-hasmenu {{ explode('.', $view_name)[0]=='results' ? 'active pcoded-trigger' : '' }}">
        <a href="javascript:void(0)">
            <span style="background-color: #7491F0"  class="pcoded-micon"><i style="padding-top: 13%" class="fa fa-registered"></i></span>
            <span class="pcoded-mtext" data-i18n="nav.social.main">Resultados</span>
            <span class="pcoded-mcaret"></span>
        </a>
        <ul class="pcoded-submenu">
            <li class="{{ Route::currentRouteNamed('results.test') ? 'active' : '' }}">
                <a href="{{ route('results.test') }}">
                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                    <span class="pcoded-mtext" data-i18n="nav.social.fb-wall">Por Examen</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
            <li class="{{ Route::currentRouteNamed('results.student') ? 'active' : '' }}">
                <a href="{{ route('results.student') }}">
                    <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                    <span class="pcoded-mtext" data-i18n="nav.social.fb-wall">Por Estudiante</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </li>
    <li class="{{ explode('.', $view_name)[0]=='imports.list' ? 'active pcoded-trigger' : '' }}">
        <a href="{{ route('imports.list') }}">
            <span class="pcoded-micon" style="background-color:#7491F0;"><i class="fas fa-file-import"></i><b>T</b></span>
            <span class="pcoded-mtext" data-i18n="nav.task.main">Importar Datos</span>
            <span class="pcoded-mcaret"></span>
        </a>
    </li>

 <br /><br /><br /><br /><br />


</ul>
</div>
</nav>
<div class="pcoded-content">
 <div class="pcoded-inner-content">
     <div class="main-body">
         <div class="page-wrapper">
             @yield('body')
         </div>
     </div>
 </div>
</div>
