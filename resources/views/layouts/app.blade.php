
@php

if(empty(Auth::user())){
echo ("<script>window.location.href='http://".$_SERVER['HTTP_HOST']."/';</script>");
exit;
}
@endphp

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<input id="backbuttonstate" type="text" value="0" style="display:none;" />
    <script type="text/javascript" src="{{ asset('bower_components/jquery/js/jquery.min.js') }}"></script>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
    }
});

$.ajax({
    url: '{{ route('validation') }}',
    method: 'post',
    success: function(result) {},
    error: function(){
      window.location.href=window.location.origin;;
    }
});
</script>

<script type="text/javascript" src="{{ asset('bower_components/jquery/js/jquery.min.js') }}"></script>
<head>
    <script>
    	if(performance.navigation.type == 2){
    	   location.reload(true);
    	}
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Favicon icon -->
    <link rel="icon" href="{{ asset('assets/images/2.png') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('assets/images/2.png') }}" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap/css/bootstrap.min.css') }}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/icon/themify-icons/themify-icons.css') }}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/icon/icofont/css/icofont.css') }}">
    <!-- Menu-Search css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/menu-search/css/component.css') }}">
    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{asset('bower_components/select2/css/select2.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/j-pro/css/j-forms.css')}}">
    <!--forms-wizard css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/jquery.steps/css/jquery.steps.css')}}">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- notifications -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/animate.css/css/animate.css')}}">
    <!-- Chartlist chart css -->
    <link rel="stylesheet" href="{{ asset('bower_components/chartist/css/chartist.css') }}" type="text/css" media="all">
    <!--Sweetalert css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/sweetalert.min.css') }}" />
    <!-- C3 chart -->
    <link rel="stylesheet" href="{{ asset('bower_components/c3/css/c3.css') }}" type="text/css" media="all">
    <!--file_upload_input css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/file_upload_input.css') }}" />
    <!-- Date-time picker css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/advance-elements/css/bootstrap-datetimepicker.css') }}">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-daterangepicker/css/daterangepicker.css') }}" />
    <!-- Date-Dropper css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datedropper/css/datedropper.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('bower_components/chartist/css/chartist.css')}}" type="text/css" media="all">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-47593322-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-47593322-4');
    </script>

    @yield('style')

    </head>
<body>

    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            @yield('content')
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a style="padding:7px; padding-left: 10px; padding-right: 10px;" class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="ti-menu" style="padding-left:10px; color:#000000"></i>
                        </a>

                        <center>
                        <a href="{{route('dashboard')}}">
                            <i style="color:#303548"> <font FACE="verdana" size="26"><strong>SAE</strong></font></i>
                            <!--<img id="top_logo" class="img-fluid" src="{{ asset('assets/images/2.png') }}" style="width:33%;" alt="Tam_logo" />-->
                        </a></center>
                        <a class="mobile-options">
                            <i class="ti-more" style="color:black"></i>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                            </li>
                             @if(Auth::user()->type == 1)
                             <!--<li>
                                <a class="main-search morphsearch-search" href="#">

                                    <i class="ti-search"></i>
                                </a>
                            </li>-->
                             @endif
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen();">
                                    <i class="ti-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">

                            <li class="user-profile header-notification">
                                <a href="#!">
                                    <img src="{{ asset('assets/images/user_img.png') }}" class="img-radius" alt="Imagen-Usuario">
                                    <span>
                                        @if(Auth::user()->type == 3)
                                            @php
                                                $full_name = DB::table('student')->where('id_users', '=', Auth::user()->id)->first()->full_name;
                                                if($full_name=="")
                                                    $full_name = Auth::user()->username;

                                                echo($full_name);

                                            @endphp
                                        @else
                                            {{ Auth::user()->username }}
                                        @endif
                                    </span>

                                    <i class="ti-angle-down"></i>
                                </a>
                                <ul class="show-notification profile-notification">
                                    <li>
                                        <li>
                                            <a href="{{ route('profile.show_own_profile') }}">
                                                <i class="ti-user"></i> Perfil de Usuario
                                            </a>
                                        </li>
                                        <li>
                                            <a class="" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                <i class="ti-layout-sidebar-left"></i>{{ __('Cerrar Sesión') }}
                                            </a>
                                        </li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {!! csrf_field() !!}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>



                       <div id="morphsearch" class="morphsearch">
                            <form class="morphsearch-form" >
                                <input style="font-size:25px;" class="morphsearch-input" type="search" oninput="realizarBusqueda(this.value)" placeholder="Buscar" />
                                <button class="morphsearch-submit" type="submit">Buscar</button>
                            </form>
                            <div class="morphsearch-content" id="busqueda_contenido" style="width:95%;
                                                                                           height:80%;
                                                                                           overflow:auto;">

                            </div>
                            <!-- /morphsearch-content -->
                            <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
                        </div>



                    </div>
                </div>
            </nav>
            {{-- Preparacion de la pagina y menu lateral de la aplicacion --}}
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <br><br>
                            {{-- Permite el mostrado del dashboard de un usuario
                            usuario autenticado, revisando tu atributo type y porsteiormente
                            llamando el codigo de la vista correspondiente--}}

                            @switch(Auth::user()->type)
                                @case(1)
                                 @include('layouts.users_dashboard.administrator')
                                @break
                                @case(2)
                                 @include('layouts.users_dashboard.administrator_institucion')
                                @break
                                @case(3)
                                @include('layouts.users_dashboard.estudiante')
                                @break
                                @default
                                <span></span>
                            @endswitch
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

    @include('sweet::alert')
    <?php Session::forget('sweet_alert'); ?>

    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('bower_components/jquery-ui/js/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/popper.js/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="{{ asset('bower_components/jquery-slimscroll/js/jquery.slimscroll.js') }}"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="{{ asset('bower_components/modernizr/js/modernizr.js') }}"></script>
    <!-- am chart -->
    <script type="text/javascript" src="{{ asset('assets/pages/widget/amchart/amcharts.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/widget/amchart/serial.min.js') }}"></script>
    <!-- Chart js -->
    {{--<script type="text/javascript" src="{{ asset('bower_components/chart.js/js/Chart.js') }}"></script>--}}

    <!-- i18next.min.js -->
    <script type="text/javascript" src="{{ asset('bower_components/i18next/js/i18next.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/jquery-i18next/js/jquery-i18next.min.js') }}"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/custom-picker.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/pcoded.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/script.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/demo-upv-1.js') }}"></script>
    <script src="{{ asset('assets/pages/forms-wizard-validation/form-wizard.js') }}"></script>

    <!--Forms - Wizard js-->
    <script src="{{ asset('bower_components/jquery.cookie/js/jquery.cookie.js') }}"></script>
    <script src="{{ asset('bower_components/jquery.steps/js/jquery.steps.js') }}"></script>
    <script src="{{ asset('bower_components/jquery-validation/js/jquery.validate.js') }}"></script>

    <!-- Datatable -->
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/data-table/js/data-table-custom.js') }}"></script>

    <!-- Select 2 js -->
    <script type="text/javascript" src="{{asset('bower_components/select2/js/select2.full.min.js')}}"></script>

    <!-- file_upload_input js -->
    <script type="text/javascript" src="{{ asset('assets/js/file_upload_input.js') }} "></script>

    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/moment-with-locales.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-daterangepicker/js/daterangepicker.js') }}"></script>
    <!-- Date-dropper js -->
    <script type="text/javascript" src="{{ asset('bower_components/datedropper/js/datedropper.min.js') }}"></script>
    <!-- Color picker js -->
    <script type="text/javascript" src="{{ asset('bower_components/spectrum/js/spectrum.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/jscolor/js/jscolor.js') }}"></script>

    <!-- notification js -->
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap-growl.min.js') }}"></script>

    <!-- Modal -->
	<div id="loading_modal_pdf" class="modal-loading-pdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div style="padding-top:9%" class="loader-block">
			 <svg id="loader2" viewBox="0 0 100 100">
			 <circle id="circle-loader2" cx="50" cy="50" r="45"></circle>
			 </svg>
	 	</div>
		<center>
			<h4 id="myModalLabel_pdf" style="color:white;">Generando el archivo .pdf </h4>
			<h5 style="color:white;">Por favor espere...</h5>
		</center>
	</div>

    <div id="crear_examen_modal" class="modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content crearlibros-modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title">Crear Examen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form_libros" method="POST" action="{{ route('test.store') }}" enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Nombre:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre') }}" require>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Mostrar Puntuación:</label>
                            <div class="form-radio col-sm-8">
                                <div class="radio radiofill radio-inverse radio-inline">
                                    <label>
                                        <input type="radio" name="mostrar_puntuacion" required value="1">
                                        <i class="helper"></i>Si
                                    </label>
                                </div>
                                <div class="radio radiofill radio-inverse radio-inline">
                                    <label>
                                        <input type="radio" name="mostrar_puntuacion" value="0">
                                        <i class="helper"></i>No
                                    </label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div style="margin:0px;" class="col-sm-4">
                                <button type="button" class="btn btn-secondary btn-adjust-mobile" style="float:right;width:100%; margin-left:20px;" data-dismiss="modal"><i class="fa fa-times"></i>Cerrar</button>
                            </div>
                            <div class="col-sm-8">
                                <button class="btn btn-success btn-adjust-mobile" type="submit" style="float:right;width:100%; min-width:150px"><i class="fa fa-plus"></i>Crear Examen</button>

                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>




    <div id="asignar_examen_a_examen_modal" class="modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content crearlibros-modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title">Asignar Examen a Estudiante</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form_asignacion" method="POST" action="{{ route('assign.store') }}" enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label"><strong>Seleccionar Examen</strong></label>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Exámenes:</label>
                            <div class="col-sm-10">
                                <select name="select_examen" id="select_examen" class="form-control">
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-12 col-form-label"><strong>Seleccionar Estudiante</strong></label>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Estudiantes:</label>
                            <div class="col-sm-10">
                                <select name="select_estudiante" id="select_estudiante" class="form-control">
                                </select>
                            </div>
                        </div>

                        <br>

                        <div class="row">
                            <div style="margin:0px;" class="col-sm-4">
                                <button type="button" class="btn btn-secondary btn-adjust-mobile" style="float:right;width:100%; margin-left:20px;" data-dismiss="modal"><i class="fa fa-times"></i>Cerrar</button>


                            </div>
                            <div class="col-sm-8">
                                <button class="btn btn-success btn-adjust-mobile" id="confirmar_asignacion_btn" type="button" style="float:right;width:100%; min-width:150px"><i class="fa fa-plus"></i>Asignar Examen</button>

                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


<script>
    function open_asignar_test_to_student_modal(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}',
            }
        });
        $.ajax({
            url: '{{ route('test.obtener_datos_para_asignar_examen_estudiante') }}',
            type: 'POST',

            success: function(result) {
                students = result['students'];
                tests = result['tests'];

                var o = new Option("Seleccionar examen", 0);
                  $("#select_examen").append(o);
                  tests.forEach(function(test){
                  var o = new Option(test.description, test.id);
                  $("#select_examen").append(o);
                });


                var u = new Option("Seleccionar estudiante", 0);
                  $("#select_estudiante").append(u);
                  students.forEach(function(student){
                  var u = new Option(student.full_name+" - "+student.CURP, student.id);
                  $("#select_estudiante").append(u);
                });

                $('#asignar_examen_a_examen_modal').modal('show');
            }
        });

    }

    $("#asignar_test_to_student_btn").click(function(){
        $("#select_estudiante").html("");
        $("#select_examen").html("");
        open_asignar_test_to_student_modal();
    });


    $("#select_estudiante").select2();
    $("#select_examen").select2();

    $("#confirmar_asignacion_btn").click(function(){


        if( $("#select_examen").val() == 0 || $("#select_estudiante").val() == 0){
            swal({
                icon: 'error',
                title: 'Información Faltante',
                text: 'Favor de seleccionar el estudiante y el examen a asignar a dicho estudiante.',
                buttons: 'Aceptar',
            })
        }else{
            swal({
    			title: "Atención",
    			text: "Esta a punto de asignar un examen a un estudiante, ¿Desea continuar?.",
    			icon: "warning",
    			buttons: true,
    			dangerMode: false,
    			buttons: ["Cancelar","Continuar"],
    			closeModal: false
    		}).then((result) => {
    			if (result) {
    				$("#form_asignacion").submit();
    			}
    		});
        }

    });




      function realizarBusqueda(palabra){

        if(palabra != ""){
            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              }
            });
            $.ajax({
              url: '{{ route('home.buscar') }}',
              method: 'post',
              data: {
                word: palabra,
              },
              success: function(result) {
                buscar_registros = result['registros'];

               if(Object.keys(buscar_registros).length != 0){
                      var contenido = document.getElementById("busqueda_contenido");
                      while (contenido.hasChildNodes()){
                          contenido.removeChild(contenido.firstChild);
                      }


                      buscar_registros.forEach(function (r){
                        var label = document.createElement("h6");
                        var h5 = document.createElement("a");
                        //var strong = document.createElement("strong");
                        //strong.append("Programa sin reglas de operación: ");
                        //h5.appendChild(strong);
                        h5.href = "/srt/registros/"+r.id;
                        h5.append(r.nombre_completo);
                        h5.style ="color:black;";
                        label.appendChild(h5);
                        contenido.appendChild(label);
                      });

               }else{
                    var contenido = document.getElementById("busqueda_contenido");
                    while (contenido.hasChildNodes()){
                        contenido.removeChild(contenido.firstChild);
                    }
                    var label = document.createElement("h2");
                    label.append("Sin resultados");
                    contenido.appendChild(label);

               }

              }
            });
        }else{
            var contenido = document.getElementById("busqueda_contenido");
            while (contenido.hasChildNodes()){
                contenido.removeChild(contenido.firstChild);
            }

            var label = document.createElement("h2");
            label.append("Sin resultados");
            contenido.appendChild(label);
        }
      }




    var capturistas_table;
    var id_capturista;
    var flag = 0;

    var capturistas=[];
    $(document).ready(function(){

    });
        var base_img_url = '{{ asset('') }}';
        var can_continue = true;
        var modifications_done = false;
        $('#downloadLink').click(function() {
          $('#fader').css('display', 'block');
        });
        var setCookie = function(name, value, expiracy) {
          var exdate = new Date();
          exdate.setTime(exdate.getTime() + expiracy * 1000);
          var c_value = escape(value) + ((expiracy == null) ? "" : "; expires=" + exdate.toUTCString());
          document.cookie = name + "=" + c_value + '; path=/';
        };
        var getCookie = function(name) {
          var i, x, y, ARRcookies = document.cookie.split(";");
          for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
            x = x.replace(/^\s+|\s+$/g, "");
            if (x == name) {
              return y ? decodeURI(unescape(y.replace(/\+/g, ' '))) : y; //;//unescape(decodeURI(y));
            }
          }
        };

        $(".select2_basic").select2();
        $(".select2_datatable").select2();
        $('#table_footer').appendTo('#table_header');
        if($('#form')!=null){
            $("#form").submit(function(e) {
                can_continue=true;
                for(i=0;i<error_divs.length;i++){
                    if(error_divs[i].css('display')=='block'){
                        can_continue=false;
                        break;
                    }
                }
                if(!can_continue){
                    e.preventDefault();
                    swal({
                        icon: 'error',
                        title: 'No se pudo guardar el registro',
                        text: 'Por favor corrija los errores antes de continuar.',
                        buttons: 'Aceptar',
                    })
                }
            });
        }

        if ( $('[type="date"]').prop('type') != 'date' ) {
            $('[type="date"]').datepicker();
        }
        if ($(window).width() < 440) {
            $("#top_logo").css("width","130px");
            $(".t-footer").css('padding-right','15%');
        }else if ($(window).width() < 600) {
            $("#top_logo").css("width","130px");
            $(".t-footer").css('padding-right','10%');
        }else if ($(window).width() < 750) {
            $("#top_logo").css("width","130px");
            $(".t-footer").css('padding-right','7%');
        }else if ($(window).width() < 880) {
            $("#top_logo").css("width","130px");
            $(".t-footer").css('padding-right','6%');
        }else if ($(window).width() < 1060) {
            $(".t-footer").css('padding-right','5%');
        }

		$('#downloadLink').click(function() {
		  $('#fader').css('display', 'block');
		  setCookie('downloadStarted', 0, 100); //Expiration could be anything... As long as we reset the value
		  setTimeout(checkDownloadCookie, 1000); //Initiate the loop to check the cookie.
		});
		var downloadTimeout;
		var checkDownloadCookie = function() {
		  if (getCookie("downloadStarted") == 1) {
		    setCookie("downloadStarted", "false", 100); //Expiration could be anything... As long as we reset the value
		    $('#fader').css('display', 'none');
		  } else {
		    downloadTimeout = setTimeout(checkDownloadCookie, 1000); //Re-run this function in 1 second.
		  }
		};
    	function get_s(string){
    		return string.replace(/</g, "&lt;").replace(/>/g, "&gt;");
    	}
    	function get_as(array){
    		for(var i=0;i<array.length;i++){
    			var temp_string=array[i];
    			if (typeof temp_string === 'string' || temp_string instanceof String){
    				array[i]=temp_string.replace(/</g, "&lt;").replace(/>/g, "&gt;");
    			}
    		}
    		return array;
    	}
        function notify(message, type){
          $.growl({
              message: message
          },{
              type: type,
              allow_dismiss: false,
              label: 'Cancel',
              className: 'btn-xs btn-success',
              placement: {
                  from: 'bottom',
                  align: 'right'
              },
              delay: 2500,
              animate: {
                      enter: 'animated fadeInRight',
                      exit: 'animated fadeOutRight'
              },
              offset: {
                  x: 30,
                  y: 30
              }
          });
        };

        function modal_img_function(){

            var modal = document.getElementById('modal_show_img');
            var is_open = false;
            var img = document.getElementById('modal_img');
            var modalImg = document.getElementById("img_content");
            var captionText = document.getElementById("caption");

            if (img!=null) {
                img.onclick = function() {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                    is_open = true;
                }
                modalImg.onclick = function() {
                    modal.style.display = "none";
                    is_open = false;
                }
                modal.onclick = function() {
                    modal.style.display = "none";
                    is_open = false;
                }
                $(document).keyup(function(e) {
                    if (e.key === "Escape") {
                        if (is_open) {
                            modal.style.display = "none";
                            is_open = false;
                        }
                    }
                });
                var span = document.getElementsByClassName("close")[0];
                span.onclick = function() {
                    modal.style.display = "none";
                }
            }
        }

        modal_img_function();

        function verify_column(elem_ref, col_name, tab_name, original = null, error_div = null, error_message = null) {
            $(error_div).text(error_message);
            $(error_div).css('display', 'none');
            if (original != null) {
                for (x = 0; x < original.length; x++) {
                    if (original[x][0] == col_name && original[x][1] == $(elem_ref).val()) {
                        $(elem_ref).css('border-color', '#bab8b8');
                        $(error_div).css('display', 'none');
                        return;
                    }
                }
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $.ajax({
                url: '{{ route('verify.column_exists') }}',
                method: 'post',
                data: {
                    column_value: $(elem_ref).val(),
                    column_name: col_name,
                    table_name: tab_name
                },
                success: function(result) {
                    if (result['response'] == true) {
                        $(elem_ref).css('border-color', '#ff0300');
                        $(error_div).css('display', 'block');
                    } else {
                        if($(elem_ref).css('border-color')=='#f4ad42' || $(elem_ref).css('border-color')=='rgb(244, 173, 66)'){
                            can_continue=true;
                        }
                        if($(elem_ref).css('border-color')=='#ff0300' || $(elem_ref).css('border-color')=='rgb(255, 3, 0)'){
                            $(elem_ref).css('border-color', '#bab8b8');
                            $(error_div).css('display', 'none');
                        }
                    }
                }
            });
        }

        function applyStyleToDatatable(button_html, placeholder = "Buscar...", type=0, sorting_type='asc', data=null){
            var dom_order;
            if(type==0)
                if ($(window).width() < 770) {
                    dom_order = '<"row" <"col-md-8 pull-right form-group" f><"col-sm-3">>rtip';
                }else{
                    dom_order = '<"row" <"col-md-9 pull-right form-group" f><"col-sm-3">>rtip';
                }
            else
                dom_order = 'frtip';


            var table=$('#custom_datatable').DataTable({
                rowReorder: {
                   selector: 'td:nth-child(2)'
                },
                aaSorting: [ [0,sorting_type] ],
                responsive: true,
                dom: dom_order,
                data: data,
                filter: true,
        		language: {
        			search: "Buscar:",
        			searchPlaceholder: placeholder
        		},
        		initComplete: function() {
        			$("div.col-sm-3").html(button_html);
        			var actual_column=0;
        			this.api().columns().every(function() {
        				var column = this;
        				var size = this.columns().header().length;
                        var size_str="";
                        if ($(window).width() < 400) {
                            size_str="font-size: 10px";
        				}
        				if(actual_column==size-1){
        					var select = $('').appendTo($(column.footer()).empty());
        				}else{
                            var select = $('<select class="form-control form-control-sm"><option value="">&nbsp;</option></select>')
        						.appendTo($(column.footer()).empty())
        						.on('change', function() {
        							var val = $.fn.dataTable.util.escapeRegex(
        								$(this).val()
        							);
        							column
        								.search(val ? '^' + val + '$' : '', true, false)
        								.draw();
        						});
        					column.data().unique().sort().each(function(d, j) {
                                if(!Number.isInteger(d)){
                                    if(d.includes('</i>')){
                                        {{--En caso de que se incluya un icono--}}
                                        d=d.substring(
                                            d.lastIndexOf("</i>")+4,
                                            d.length
                                        );
                                    }else if(d.includes('<img')){
                                        d="";
                                    }else{
                                        {{--En caso de que incluya un link este el eliminado de las opciones que se mostraran--}}
                                        if(d.includes('<')){
                                            d=d.substring(
                                                d.indexOf(">")+1,
                                                d.lastIndexOf("<")
                                            );
                                        }
                                    }
                                }
                                if(d.length>0)
        						    select.append('<option value="' + d + '">' + d + '</option>');
        					});
        				}
        				actual_column++;
        			});
                    $('#custom_datatable_filter').find(">:first-child").css('float','left');
                    if ($(window).width() < 770) {
    					$('#custom_datatable_filter').find(">:first-child").css('width','80%');
    				}else{
                        $('#custom_datatable_filter').find(">:first-child").css('width','93%');
                    }
        		}
        	});
        	$("#copy_btn").on("click", function() {
        		table.button(0).trigger('click');
        	});
        	$("#csv_btn").on("click", function() {
        		table.button(1).trigger('click');
        	});
        	$("#excel_btn").on("click", function() {
        		table.button(2).trigger('click');
        	});
        	$("#pdf_btn").on("click", function() {
                $("#loading_modal_pdf").removeClass('hide_block');
                $("#loading_modal_pdf").addClass('display_block');
                $("#loading_modal_pdf").show("fast");
                table.button(3).trigger('click');
        	});
        	$("#print_btn").on("click", function() {
        		table.button(4).trigger('click');
        	});
            $("#dataTables_info").css('width','50%');
            $(".ul").css('width','50%');
        }

        function getTitle(view_name){
            name=getName(view_name);
            if(name=="")
                name="SISTEMA DE APLICACIÓN DE EXAMENES";
            else
                name= name.toUpperCase();
            var title="REPORTE DE "+name;
            return title;
        }

        function getName(view_name){
            if(view_name=="log.movementslist")
                var name="movimientos";
            else
                var name=view_name.split(".")[0];
            switch(name){
                case 'users':
                    name="usuarios";
                    break;
            }
            return name;
        }

    	function checkIfChangesHaveBeenMadeIn(elements_id, unique_elements=null){
    		var original_values=[];
    		var default_border_colors=[];
            modifications_done=false;
    		for(i=0;i<elements_id.length;i++){
    			original_values[$(elements_id[i]).attr('name')]=elements_id[i].val();
    			default_border_colors[$(elements_id[i]).attr('name')]=elements_id[i].css("border-color");
                var verificate=false;
    			elements_id[i].keyup(function(e){
    				if($(this).val()!=original_values[$(this).attr('name')]){
                        modifications_done=true;
    					$(this).css("border-color",'#f4ad42');
                    }
    				else
    					$(this).css("border-color",default_border_colors[$(this).attr('name')]);
                    for(uei=0;uei<unique_elements.length;uei++){
                        if($(this).attr('name')==unique_elements[uei][0].attr('name')){
                            e.preventDefault();
                			verify_column(unique_elements[uei][0], unique_elements[uei][1], unique_elements[uei][2],
                                unique_elements[uei][3], unique_elements[uei][4], unique_elements[uei][5]);
                            break;
                        }
                    }
    			});
                elements_id[i].change(function(e){
    				if($(this).val()!=original_values[$(this).attr('name')]){
                        if($(this).is('select')) {
                            if($(this)!=null){
                                $(this).select2({
                                    containerCssClass: "changedType"
                                });
                            }
                        }else{
                            modifications_done=true;
		                    $(this).css("border-color",'#f4ad42');
                        }
                    }
    				else{
                        if($(this).is('select')) {
                            if($(this)!=null){
                                $(this).select2();
                            }
                        }else
    					    $(this).css("border-color",default_border_colors[$(this).attr('name')]);
                    }
                    for(uei=0;uei<unique_elements.length;uei++)
                        if($(this).attr('name')==unique_elements[uei][0].attr('name')){
                            e.preventDefault();
                			verify_column(unique_elements[uei][0], unique_elements[uei][1], unique_elements[uei][2],
                                unique_elements[uei][3], unique_elements[uei][4], unique_elements[uei][5]);
                            break;
                        }
    			});
    			elements_id[i].bind("DOMSubtreeModified",function(e){
    				if($(this).val()!=original_values[$(this).attr('name')]){
                        modifications_done=true;
    					$(this).css("border-color",'#f4ad42');
                    }
    				else
    					$(this).css("border-color",default_border_colors[$(this).attr('name')]);
                    for(uei=0;uei<unique_elements.length;uei++)
                        if($(this).attr('name')==unique_elements[uei][0].attr('name')){
                            e.preventDefault();
                			verify_column(unique_elements[uei][0], unique_elements[uei][1], unique_elements[uei][2],
                                unique_elements[uei][3], unique_elements[uei][4], unique_elements[uei][5]);
                            break;
                        }
    			});
    		}
    	}

        function confirmationOnReturn(previous_url){
            if(window.location.href==previous_url){
                var split=previous_url.split("/");
                if(split.length>3)
                    previous_url=split[0]+"/"+split[1]+"/"+split[2]+"/"+split[3];
                else
                    previous_url='{{ route('dashboard') }}';
            }
            if(modifications_done){
                swal({
                    title: "Atención",
                    text: "Todos los cambios que ha realizado seran descartados. ¿Esta seguro de que desea salir?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: false,
                    buttons: ["Cancelar", "Aceptar"],
                    closeModal: false
                }).then((result) => {
                    if (result) {
                        window.location.href = previous_url;
                    }
                });
            }else
                window.location.href = previous_url;
        }

        function returnURL(previous_url){
            if(window.location.href==previous_url){
                var split=previous_url.split("/");
                if(split.length>3)
                    previous_url=split[0]+"/"+split[1]+"/"+split[2]+"/"+split[3];
                else
                    previous_url='{{ route('dashboard') }}';
            }
            window.location.href = previous_url;
        }

    	function archiveFunction(entry_url, real_delete=false) {
            var view_name='{{$view_name}}';
    		event.preventDefault();
    		swal({
    			title: "Atención",
    			text: "¿Está seguro de eliminar el registro?",
    			icon: "warning",
    			buttons: true,
    			dangerMode: true,
    			buttons: ["Cancelar", "Eliminar"],
    			closeModal: false
    		}).then(function(result){
    			if (result) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}',
                        }
                    });
                    $.ajax({
                        url: entry_url,
                        type: 'POST',
                        data:{
                            _method:"DELETE"
                        },
                        success: function(result) {
                            swal({
                                icon: 'success',
                                title: 'Exito',
                                text: 'El registro fue eliminado correctamente.',
                                buttons: 'Aceptar',
                            });
                            setTimeout(function(){
                                if(!real_delete){
                                    @if(Auth::user()->type!=6)
                                        if(view_name.includes("list")){
                                            location.reload();
                                        }else{
                                            returnURL('{{ url()->previous() }}');
                                        }
                                    @else
                                        location.reload();
                                    @endif
                                }else{
                                    returnURL('{{ url()->previous() }}');
                                }
                            },900);
                        }
                    });
    			}
    		});
    	}

    	function restoreFunction(entry_url) {
    		event.preventDefault();
            var form = event.target.form;
            if(form==null){
                form = event.currentTarget.form;
            }
    		swal({
    			title: "Atención",
    			text: "¿Está seguro de restaurar el registro?",
    			icon: "warning",
    			buttons: true,
    			dangerMode: true,
    			buttons: ["Cancelar", "Restaurar"],
    			closeModal: false
    		}).then(function(result){
                if (result) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}',
                        }
                    });
                    $.ajax({
                        url: entry_url,
                        type: 'POST',
                        success: function(result) {
                            swal({
                                icon: 'success',
                                title: 'Exito',
                                text: 'El registro fue restaurado correctamente.',
                                buttons: 'Aceptar',
                            });
                            setTimeout(function(){
                                location.reload();
                            },900);
                        }
                    });
    	        }
            });
    	}
    </script>

    {{-- Codigo JavaScript agregado en la vista--}}
    @yield('javascriptcode')
</body>

</html>
