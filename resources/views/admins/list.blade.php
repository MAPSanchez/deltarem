@extends('layouts.app')

@section('title',"Listado de Administradores de Exámenes - SAE")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fa fa-users-cog" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Listado de Administradores de Exámenes</h4>
						<span style="text-transform: none;">Lista de todos los administradores de exámenes registrados en el sistema.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Administradores de Exámenes
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Zero config.table start -->
				<div class="card">
					<div class="card-block">
						<div class="dt-responsive table-responsive">
								@if($admins!=NULL)
                    <table style="width:100%" id="custom_datatable" class="table table-striped table-bordered">
                        <thead id="table_header">
                            <tr>
                                <th class="all" scope="col" style="width:10%;">ID</th>
                                <th scope="col">Username</th>
                                <th scope="col">E-mail</th>
                                <th scope="col">Institución Educativa</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                       
                    </table>
                @else
                    <center>
                        <div class="alert alert-warning icons-alert" id="alert_div">
                            <strong>Información</strong>
                            <p>Actualmente no hay ningún administrador de exámenes registrado en el sistema.</p>
                        </div>
                        <a href="{{ route('administrators.create') }}"><button class="btn btn-success" style="float:right;width:100%; min-width:150px"><i class="fas fa-user-plus"></i>Registrar Nuevo Administrador de Exámenes</button></a>
                    </center>
                @endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>
  var data = @php echo(json_encode($admins)); @endphp;
	var button='<a href="{{ route('administrators.create') }}"><button class="btn btn-success" style="float:right;width:100%; min-width:150px"><i class="fa fa-user-plus"></i>Registrar Administrador</button></a>';
  applyStyleToDatatable(button, 'Buscar en administradores de exámenes...',0,'asc',data);

</script>
@endsection
