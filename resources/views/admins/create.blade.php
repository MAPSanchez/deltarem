@extends('layouts.app')

@section('title',"Registrar Administrador de Exámenes - SAE")

@section('body')

<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fas fa-user-plus" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Registrar Administrador de Exámenes</h4>
						<span style="text-transform: none;">Llene los campos solicitados en la parte inferior para agregar un nuevo administrador de exámenes.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Administradores de Exámenes
						</li>
						<li class="breadcrumb-item">Registrar Administrador de Exámenes
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-block">
						<form id="form" method="POST" action="{{ route('administrators.store') }}" enctype="multipart/form-data">
							{!! csrf_field() !!}
							
							
                                
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" >Institución Educativa:</label>
                                <div class="col-sm-10">
                                    <select name="ies" class="select2_basic form-control" title="Institución Educativa">
                                        @foreach($instituciones as $i)
                                        <option value="{{$i->id}}" {{ (old("ies") == $i->id ? "selected":"") }}>{{$i->name}}</option>
                                        @endforeach
                                    </select>
								</div>
							</div>
                            
                            <div class="form-group row">
								<label class="col-sm-2 col-form-label" for="username">Username:</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="username" name="username" title="Username del Administrador de Exámenes" value="{{ old('username') }}">
									@if ($errors->has('username'))
										<div class="col-form-label" style="color:red;">{{$errors->first('username')}}</div>
									@endif
								</div>
                                <label class="col-sm-2 col-form-label" for="password">Contraseña:</label>
								<div class="col-sm-4">
									<input type="password" class="form-control" name="password" title="Contraseña del Administrador de Exámenes" value="{{ old('password') }}">
									@if ($errors->has('password'))
										<div class="col-form-label" style="color:red;">{{$errors->first('password')}}</div>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2 col-form-label" for="email">Email:</label>
								<div class="col-sm-10">
									<input type="email" class="form-control" id="email" name="email" title="Correo Electrónico del Administrador de Exámenes" value="{{ old('email') }}">
									@if ($errors->has('email'))
										<div class="col-form-label" style="color:red;">{{$errors->first('email')}}</div>
									@endif
								</div>
							</div>
							
						
							<br>
							<center>
								<a style="color:white" onclick="returnURL('{{ url()->previous() }}')" class="btn btn-primary btn-adjust-mobile"><i class="icofont icofont-arrow-left"></i>Regresar</a>
								<button type="submit" class="btn btn-success btn-adjust-mobile"><i class="icofont icofont-check-circled"></i>Guardar Administrador de Exámenes</button>
							</center>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>
	
</script>

@endsection
