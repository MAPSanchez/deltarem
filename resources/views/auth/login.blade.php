@extends('layouts.login')

@section('content')

<section class="login p-fixed d-flex text-center common-img-bg">
    <!-- Container-fluid starts -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <!-- Authentication card start -->
                <div class="login-card card-block auth-body mr-auto ml-auto">

                    <!--Login-->
                    <form class="md-float-material" method="POST" action="{{ route('login') }}">
                        {!! csrf_field() !!}
                        <div class="auth-box bg-light" style="width:120%; position:relative; right:10%">
                        <hr style="border-color:#636466" />
                            <!--<div class="text-center">
                                <img src="{{ asset('assets/images/auth/secretaria-logo.png') }}" style="width:450px;" alt="small-logo.png">
                                  </div>-->
                                 
                              
                                  <div class="text-center">
                                <img src="{{ asset('assets/images/2.png') }}" style="width:160px; margin: 0 0 0 20px;" alt="small-logo.png">
                                  </div>


                                  
                                <div style="float:center">
                                <h3 class="text-center txt-primary" style="color:#636466;">Sistema de Aplicación de</h3>
                                        <h1 class="text-center txt-primary" style="color:#636466;"><strong>Exámenes</strong></h1>

                                </div>
                               
                                <hr style="border-color:#636466" />
                                <div class="input-group">
                                    <input id="email" placeholder="Correo Electrónico" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if  ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="input-group">
                                    <input id="password" type="password" placeholder="Contraseña" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if  ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!--style="background-color:#4BBDFA" 97C93D,FCA247,4BBDFA-->
                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit"  class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">
                                            {{ __('Iniciar Sesión') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                    </form>
                    <!-- end of form -->
                </div>
                <!-- Authentication card end -->
            </div>

            <!-- end of col-sm-12 -->
        </div>
        <!-- end of row -->
    </div>
    <!-- end of container-fluid -->
</section>

@endsection
