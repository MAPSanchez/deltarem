@extends('layouts.app')

@section('title',"SAE - Principal")

@section('body')
    <script>
        function errorImg(image){
            $(image).attr('src','{{ asset('assets/images/white.png') }}')
        }
    </script>
    @switch(Auth::user()->type)
    @case(1)
    <div class="main-body">
        <!--<hr>
        <center><h2>¡Bienvenido al Sistema de Aplicación de Exámenes!</h2></center>AEBEF3 8AA2F0
        <hr>-->
        <div class="row">

            <div class="col-sm-4">
                <div class="card text-white widget-visitor-card" style="background-color:#7491F0">
                    <div class="card-block-small text-center">
                        <h2>{{$instituciones}}</h2>
                        <h6><strong> Instituciones Educativas</strong></h6>
                        <i class="fas fa-university"></i>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card text-white widget-visitor-card" style="background-color:#7491F0">
                    <div class="card-block-small text-center">
                        <h2>{{$admins}}</h2>
                        <h6><strong>Administradores de Exámenes</strong></h6>
                        <i class="fas fa-users-cog"></i>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card text-white widget-visitor-card" style="background-color:#7491F0">
                    <div class="card-block-small text-center">
                        <h2>{{$estudiantes}}</h2>
                        <h6><strong>Estudiantes</strong></h6>
                        <i class="fas fa-users"></i>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">información por institución educativa</h4>
                        <div class="row">
                            <div class="col-sm-12">
                            @if($data_instituciones->isNotEmpty())
                            <div class="dt-responsive table-responsive">
                                <table id="compact" class="table compact table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>Institución Educatica</th>
                                            <th>N° Administradores</th>
                                            <th>N° Estudiantes</th>
                                            <th>N° Exámenes</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                            @foreach($data_instituciones as $c)
                                            <tr>
                                                <td>{{$c->name}}</td>
                                                <td>{{$c->admins}}</td>
                                                <td>{{$c->estudiantes}}</td>
                                                <td>{{$c->examenes}}</td>
                                                </tr>
                                            @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                            <th>Institución Educatica</th>
                                            <th>N° Administradores</th>
                                            <th>N° Estudiantes</th>
                                            <th>N° Exámenes</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            @else

                                <div class="alert alert-primary icons-alert">
                                    <p><strong>Sin instituciones</strong></p>
                                    <p>No hay instituciones educativas registradas.</p>
                                </div>
                            @endif
                        </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @break
    @case(2)
    <div class="main-body">
        <!--<hr>
        <center><h2>¡Bienvenido al Sistema de Aplicación de Exámenes!</h2></center>AEBEF3 8AA2F0
        <hr>-->
        <div class="row">
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-block">
                        <div class="row align-items-end">
                            <div style="width:100%" class="page-header-title">
                                <img style="margin-left:14px; margin-right:25px; width:100%; max-width: 110px;" src="{{ asset($institution->logo_img) }}" onerror="errorImg(this);" />
                                <div class="d-inline" ><br>
                                    <h4 style="text-transform: none;">{{$institution->name}}</h4>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="card text-white widget-visitor-card" style="background-color:#7491F0">
                    <div class="card-block-small text-center">
                        <h2>{{$examenes}}</h2>
                        <h6><strong>Exámenes</strong></h6>
                        <i class="fas fa-list-alt"></i>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="card text-white widget-visitor-card" style="background-color:#7491F0">
                    <div class="card-block-small text-center">
                        <h2>{{$estudiantes}}</h2>
                        <h6><strong>Estudiantes</strong></h6>
                        <i class="fas fa-users"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                            @if($data_examenes->isNotEmpty())
                            <div class="dt-responsive table-responsive">
                                <table id="compact" class="table compact table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>Examen</th>
                                            <th>Estudiantes Asignados</th>
                                            <th>Estudiantes Calificados</th>
                                            <th>Estudiantes Faltantes</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data_examenes as $c)
                                            <tr>
                                                <td>{{$c->description}}</td>
                                                <td>{{$c->asignados}}</td>
                                                <td>{{$c->calificados}}</td>
                                                <td>{{$c->faltantes}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Examen</th>
                                            <th>Estudiantes Asignados</th>
                                            <th>Estudiantes Calificados</th>
                                            <th>Estudiantes Faltantes</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            @else
                                <div class="alert alert-primary icons-alert">
                                    <p><strong>Sin Exámenes</strong></p>
                                    <p>No existen exámenes creados.</p>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- <button class="btn btn-default btn-bg-c-pink btn-outline-default btn-round btn-action">Closed</button> -->


    </div>
    @break
    @case(3)
        <div class="main-body">
            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <div class="row">
                                    <div style="width:100%">
                                    <center>
                                        <h3>¡Bienvenido(a) <strong> {{$student->full_name}}</strong> al</h3>
                                        <h3>Sistema de Aplicación de Exámenes!</h3>
                                    </center>
                    				</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    @if($count_test == 0)
                    <div class="col-md-12 col-xl-12">
                        <div class="card curr-plan-card">
                            <div class="card-block text-center">
                                <div class="bg-c-green plan-icon d-inline-block">
                                    <i class="fas fa-check-circle"></i>
                                </div>
                                <h4 class="p-t-25 m-0 p-b-40">Sin exámenes pendientes</h4>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="d-inline-block">
                                            <h5 class="text-muted d-inline-block">El estudiante {{$student->full_name}} no posee algún examen pendiente por contestar en el Sistema de Aplicación de Exámenes.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="col-md-12 col-xl-12">
                        <div class="card curr-plan-card">
                            <div class="card-block text-center">
                                <div class="bg-c-yellow plan-icon d-inline-block">
                                    <i class="fas fa-exclamation-circle"></i>
                                </div>
                                <h4 class="p-t-25 m-0 p-b-40">Posees exámenes pendientes</h4>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="d-inline-block">
                                            <h5 class="text-muted d-inline-block">El estudiante {{$student->full_name}} posee {{$count_test}} 
                                            @if($count_test == 1)
                                                examen pendiente
                                            @else
                                                exámenes pendientes
                                            @endif
                                             por contestar en el Sistema de Aplicación de Exámenes.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    @break
    @default
        <span>Error</span>
    @endswitch

@endsection

@section('javascriptcode')
<script src="{{ asset('bower_components/chartist/js/chartist.js')}}"></script>
<script src="{{ asset('assets/pages/chart/chartlist/js/chartist-plugin-threshold.js')}}"></script>
<script src="{{ asset('bower_components/raphael/js/raphael.min.js')}}"></script>
<script src="{{ asset('bower_components/morris.js/js/morris.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>

<script>

    $("#remaining_time_span").text("10");
    var current_datetime;     //get from server
    var time_limit=70;         //minutes

    $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': '{{ csrf_token() }}'
		}
	});
	$.ajax({
		url: '{{ route('util.get_current_time') }}',
		method: 'post',
		success: function(result) {
            current_datetime = new Date(result['response'])
            limit_datetime = new Date(result['response'])
            limit_datetime.setMinutes(current_datetime.getMinutes() + time_limit);

            remaining_time = limit_datetime-current_datetime;

            var timer = setInterval(function() {
                remaining_time=remaining_time-1000;
                $("#remaining_time_span").text(milliseconds_to_datetime_str(remaining_time));
            }, 1000);
		}
	});

    function milliseconds_to_datetime_str(duration) {
        var milliseconds = parseInt((duration % 1000) / 100),
            seconds = Math.floor((duration / 1000) % 60),
            minutes = Math.floor((duration / (1000 * 60)) % 60),
            hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + ":" + minutes + ":" + seconds;
    }

</script>


@switch(Auth::user()->type)
    @case(1)
    <script>


    </script>
@break
    @case(2)
    <script>

        let hoy = new Date();
        let dieciseisDias = 1000 * 60 * 30;


        let resta = hoy.getTime() - dieciseisDias;
        let tiempo_transcurrido = new Date(resta);


        console.log(tiempo_transcurrido);
        console.log(moment(tiempo_transcurrido).format('hh:mm:ss'));

    </script>
 @break

@endswitch
@endsection
