@extends('layouts.app')

@section('title',"Detalles del Registro: {$registro->id}")

@section('body')
<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="icofont icofont-eye-alt" style="min-width:50px; background-color:#43B4E4"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Detalles del Registro: {{$registro->id}}</h4>
						<span style="text-transform: none;">Mostrando todos los detalles del registro seleccionado.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item"><a href="{{ route('capturistas.list') }}">Registros</a>
						</li>
						<li class="breadcrumb-item">Detalles de Registro
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<!-- Basic Form Inputs card start -->
				<div class="card">
					<div class="card-block">

						<div class="page-body">
							<div class="row">
								<div class="col-md-12 col-xl-12 ">
									<div class="card-block user-detail-card">

										<h4 class="sub-title">Información General</h4>
										<div class="row">

											<div class="col-sm-12 user-detail">
                                                <div class="row">
													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30" ><i class="fab fa-leanpub"></i>Libro:</h6>
													</div>
													<div class="col-sm-3">
														<h6 class="m-b-30" style="font-weight:bold; font-size:17px">{{$registro->libro}}</h6>
													</div>
													
												</div>
												<div class="row">
													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30" ><i class="fab fa-leanpub"></i>Foja:</h6>
													</div>
													<div class="col-sm-3">
														<h6 class="m-b-30" style="font-weight:bold; font-size:17px">{{$registro->foja}}</h6>
													</div>
													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-id-card"></i>Folio:</h6>
													</div>
													<div class="col-sm-3">
														<h6 class="m-b-30" style="font-weight:bold; font-size:17px;">{{$registro->folio}}</h6>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Nombre(s) del Egresado:</h6>
													</div>
													<div class="col-sm-3">
														<h6 class="m-b-30">{{$registro->nombre_egresado}}</h6>
													</div>
													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Apellido Paterno:</h6>
													</div>
													<div class="col-sm-3">
														<h6 class="m-b-30">{{$registro->apellido_paterno}}</h6>
													</div>

												</div>
												<div class="row">
													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Apellido Materno:</h6>
													</div>
													<div class="col-sm-3">
														<h6 class="m-b-30">{{$registro->apellido_materno}}</h6>
													</div>
													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-calendar"></i>Fecha de Examen:</h6>
													</div>
													<div class="col-sm-3">
														<h6 class="m-b-30">{{$registro->fecha_examen}}</h6>
													</div>
												</div>
												<br /><br />
												<div class="row">
													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-school"></i>IES:</h6>
													</div>
													<div class="col-sm-6">
														<h6 class="m-b-30">{{$registro->nombre_ies}}</h6>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-graduation-cap"></i>Profesión:</h6>
													</div>
													<div class="col-sm-6">
														<h6 class="m-b-30">{{$registro->profesion}}</h6>
													</div>
												</div>
												<br /><br />
												<div class="row">
													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30"><i class="icofont icofont-ui-user"></i>Registrado por:</h6>
													</div>
													<div class="col-sm-6">
														<h6 class="m-b-30"><a href="{{route("capturistas.show", ['id'=>$registrado_por->id])}}">{{$registrado_por->first_name}} {{$registrado_por->last_name}} {{$registrado_por->second_last_name}}</a></h6>
													</div>
												</div>
												<div class="row">

													<div class="col-sm-3">
														<h6 class="f-w-400 m-b-30"><i class="fas fa-calendar"></i>Fecha de Registro:</h6>
													</div>
													<div class="col-sm-6">
														<h6 class="m-b-30">{{$registro->fecha_registro}}</h6>
													</div>
												</div>
												<br />
												<center>
													<a style="color:white" onclick="returnURL('{{ url()->previous() }}')" class="btn btn-primary btn-adjust-mobile"><i class="icofont icofont-arrow-left"></i>Regresar</a>
												</center>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>

</script>
@endsection
