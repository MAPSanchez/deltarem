@extends('layouts.app')

@section('title',"Modificar registro {$registro->id}- SRT")

@section('body')

<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fas fa-plus" style="min-width:50px; background-color:#43B4E4"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Modificar Registro {{$registro->id}}</h4>
						<span style="text-transform: none;">Llene los campos solicitados en la parte inferior para modificar registro.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Modificar Registro
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-block">
            <div class="row">
                <div class="col-sm-2">
                  <h5><strong>Libro: </strong></h5>
                </div>
                <div class="col-sm-2">
                  <h5>{{$registro->libro}}</h5>
                </div>
                <div class="col-sm-2">
                  <h5><strong>Foja: </strong></h5>
                </div>
                <div class="col-sm-2">
                  <h5>{{$registro->foja}}</h5>
                </div>
                <div class="col-sm-2">
                  <h5><strong>Folio: </strong></h5>
                </div>
                <div class="col-sm-2">
                  <h5>{{$registro->folio}}</h5>
                </div>
            </div><hr>
						<form id="form" method="POST" action="{{ route('registros.update', ['registro'=>$registro->id]) }}" enctype="multipart/form-data">
              {{ method_field('PUT') }}
							{!! csrf_field() !!}

							<div class="form-group row">
								<label class="col-sm-2 col-form-label" for="name">Nombre(s):</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" name="name" title="Nombre del Egresado" value="{{ old('name',$registro->nombre_egresado) }}" required>
									@if ($errors->has('name'))
										<div class="col-form-label" style="color:red;">{{$errors->first('name')}}</div>
									@endif
								</div>
								<label class="col-sm-2 col-form-label" for="last_name">Apellido Paterno:</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" name="last_name" title="Apellido del Egresado" value="{{ old('last_name',$registro->apellido_paterno) }}" required>
									@if ($errors->has('last_name'))
										<div class="col-form-label" style="color:red;">{{$errors->first('last_name')}}</div>
									@endif
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label" for="second_last_name">Apellido Materno:</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" name="second_last_name" title="Apellido Materno de Egresado" value="{{ old('second_last_name', $registro->apellido_materno) }}">
									@if ($errors->has('second_last_name'))
										<div class="col-form-label" style="color:red;">{{$errors->first('second_last_name')}}</div>
									@endif
								</div>
								<label class="col-sm-2 col-form-label" for="fecha">Fecha de Examen:</label>
								<div class="col-sm-3">
									<input type="date" class="form-control" name="fecha" title="Fecha de Examen" value="{{ old('fecha',$registro->fecha_examen) }}" required>
									@if ($errors->has('fecha'))
										<div class="col-form-label" style="color:red;">{{$errors->first('fecha')}}</div>
									@endif
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label" for="ies">Institución de Educación Superior:</label>
								<div class="col-sm-4">
									<select name="ies" id="ies" onchange="CargarProfesiones(this.value);" class="select2_basic">
										<option value="0">Seleccionar Institución Educativa Superior</option>
										@foreach($IES as $i)
											<option value="{{$i->id_ies}}" {{ (old("ies", $profesion->id_ies) == $i->id_ies ? "selected":"") }}>{{$i->nombre_ies}}</option>
										@endforeach
									</select>
								</div>
								<label class="col-sm-2 col-form-label" for="profesion">Profesión:</label>
								<div class="col-sm-4">
									<select name="profesion" id="profesion" onchange="LiberarBoton(this.value);" class="select2_basic">
										<option value="0">Seleccionar Profesión</option>
									</select>
								</div>
							</div>
							<br>
							<center>
								<a style="color:white" onclick="returnURL('{{ url()->previous() }}')" class="btn btn-primary btn-adjust-mobile"><i class="icofont icofont-arrow-left"></i>Regresar</a>
								<button type="submit" id="guardar_registro" class="btn btn-success btn-adjust-mobile"><i class="icofont icofont-refresh"></i>Actualizar Registro</button>
							</center>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>
	$(document).ready(function() {
	  $('#guardar_registro').attr("disabled", true);
	  CargarProfesiones(@php echo(old('ies',$profesion->id_ies)) @endphp);
	});
  function LiberarBoton(id){
    if(id==0){
      $('#guardar_registro').attr("disabled", true);
    }else{
      $('#guardar_registro').attr("disabled", false);
    }
  }

    function CargarProfesiones(id_ies){

      if(id_ies == 0){
        swal({
            icon: 'warning',
            title: 'Seleccionar IES',
            text: 'Favor de seleccionar una institución de educación superior (IES).',
            buttons: 'Aceptar',
        })
        $('#profesion').children('option:not(:first)').remove();
        $('#guardar_registro').attr("disabled", true);
      }else{
		   console.log(id_ies);
        $('#guardar_registro').attr("disabled", true);
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': '{{ csrf_token() }}',
              }
          });
          $.ajax({
              url: '{{ route('registros.cargar_profesiones') }}',
              type: 'POST',
              data: {
                  id: id_ies
              },
              success: function(result) {
                  $('#profesion').children('option:not(:first)').remove();
                  profesiones = result['profesiones'];

  				@if(old("profesion", $profesiones[0]->id_profesion)!=NULL)
  					var old_profesion=@php echo(old("profesion", $profesiones[0]->id_profesion)) @endphp;
  				@else
  					var old_profesion=null
  				@endif

				console.log(profesiones);

                  profesiones.forEach(function(profesion){
  					var o = new Option(profesion.profesion, profesion.id_profesion);
                    	$("#profesion").append(o);
                  });

  				console.log(old_profesion);

  				$("#profesion").select2();
  				if(old_profesion!=null)
  					$('#profesion').val(old_profesion).trigger('change.select2');
  				else
  					$('#profesion').val(0).trigger('change.select2');
  				//$('#profesion').trigger('select2:change');
              }
          });
      }
  }


</script>
@endsection
