@extends('layouts.app')

@section('title',"Registrar Institución Educativa - SAE")

@section('body')

<!-- Main-body start -->
<div class="main-body">
	<!-- Page-header start -->
	<div class="page-header card">
		<div class="row align-items-end">
			<div class="col-lg-8">
				<div class="page-header-title">
					<i class="fas fa-plus" style="min-width:50px; background-color:#7491F0"></i>
					<div class="d-inline">
						<h4 style="text-transform: none;">Registrar Institución Educativa</h4>
						<span style="text-transform: none;">Llene los campos solicitados en la parte inferior para agregar una nueva institución educativa.</span>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="page-header-breadcrumb">
					<ul class="breadcrumb-title">
						<li class="breadcrumb-item">
							<a href="{{ route('dashboard') }}">
								<i class="icofont icofont-home"></i>
							</a>
						</li>
						<li class="breadcrumb-item">Instituciones Educativas
						</li>
						<li class="breadcrumb-item">Registrar Institución Educativa
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Page-header end -->

	<!-- Page-body start -->
	<div class="page-body">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-block">
						<form id="form" method="POST" action="{{ route('ies.store') }}" enctype="multipart/form-data">
							{!! csrf_field() !!}
							
							
                            <div class="form-group row">
								<label class="col-sm-2 col-form-label" for="ies">Institución Educativa:</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="ies" name="ies" title="Nombre de Institución Educativa" value="{{ old('ies') }}" required>
									@if ($errors->has('ies'))
										<div class="col-form-label" style="color:red;">{{$errors->first('ies')}}</div>
									@endif
								</div>
							</div>
							


							<div class="form-group row">
								<label class="col-sm-2 col-form-label" for="email">Email:</label>
								<div class="col-sm-10">
									<input type="email" class="form-control" id="email" name="email" title="Correo Electrónico del institución educativa" value="{{ old('email') }}" required>
									@if ($errors->has('email'))
										<div class="col-form-label" style="color:red;">{{$errors->first('email')}}</div>
									@endif
								</div>
							</div>

                            <div class="form-group row">
								<label class="col-sm-2 col-form-label">Logo de Institución Educativa:</label>
								<div class="col-sm-10">
									<div class="file-upload">
										<div class="image-upload-wrap">
											<input id="image_input" class="file-upload-input" type='file' required name="file" onchange="readURLForComponents(this);" accept="image/*" />
											<div style="padding-top:40px" onclick="$('.file-upload-input').trigger('click' )">
												<center>
													<i style="font-size: 60px;" class="fas fa-cloud-upload-alt drag-icon"></i>
												</center>
											</div>
											<div class="drag-text">
												<span>Arrastre y suelte el archivo <span style="font-weight: bold; font-size:16px;"> aquí</span> o haga clic <span style="font-weight: bold; font-size:16px;"> aquí</span> para buscarlo en su equipo.</span>
											</div>
										</div>
										<div class="file-upload-content">
											<img class="file-upload-image" src="#" alt="your image" />
											<div class="image-title-wrap">
												<button type="button" onclick="removeUpload()" class="remove-image">Remover Archivo</button>
											</div>
										</div>
									</div>
                                    @if ($errors->has('file'))
										<div class="col-form-label" style="color:red;">{{$errors->first('file')}}</div>
									@endif
								</div>
                
							</div>
                            
							
							
							<br>
							<center>
								<a style="color:white" onclick="returnURL('{{ url()->previous() }}')" class="btn btn-primary btn-adjust-mobile"><i class="icofont icofont-arrow-left"></i>Regresar</a>
								<button type="submit" class="btn btn-success btn-adjust-mobile"><i class="icofont icofont-check-circled"></i>Guardar Institución Educativa</button>
							</center>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javascriptcode')
<script>
	
    function readURLForComponents(input) {
    
    var split = input.files[0]['name'].split(".");
    var extension = split[split.length - 1];
    var accepted_extentions = ['pdf', 'PDF', 'jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG', 'bmp', 'BMP'];
    var accepted_file = false;
    if (accepted_extentions.includes(extension)) {
        accepted_file = true;
    }
    if (accepted_file) {
        if (input.files && input.files[0]) {
            if(input.files[0]['size']<5242880){
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('.image-upload-wrap').hide();
                    if(extension=="pdf" || extension=="PDF"){
                        $('.file-upload-image').height(100);
                        $('.file-upload-image').width(75);
                        $('.file-upload-image').attr('src', window.location.href.split('/').slice(0, 3).join('/')+"/img/pdf_icon.png");
                    }else{
                        $('.file-upload-image').css('height', 'auto');
                        $('.file-upload-image').css('width', 'auto');
                        $('.file-upload-image').attr('src', e.target.result);
                    }
                    $('.file-upload-content').show();
                    $('.image-title').html(input.files[0].name);
                };
                reader.readAsDataURL(input.files[0]);
                console.log(reader);
            }else{
                $("#image_input").val("");
                removeUpload();
                swal({
                    icon: 'error',
                    title: 'Archivo demasiado grande',
                    text: 'El archivo supera los 5 MB de tamaño, por favor seleccione un archivo que no sobrepase los 5 MB de tamaño.',
                    buttons: 'Aceptar',
                });
            }
        } else {
            removeUpload();
        }
    }else{
        $("#image_input").val("");
        swal({
            icon: 'error',
            title: 'Formato de archivo no valido',
            text: 'Por favor solo agregue archivos con los siguientes formatos validos: pdf, jpg, png, jpeg, bmp.',
            buttons: 'Aceptar',
        })
    }
     
}
    
function removeUpload() {
    $('.file-upload-input').replaceWith($('.file-upload-input').clone());
    $("#image_input").val("");
    $('.file-upload-content').hide();
    $('.image-upload-wrap').show();
    $('.image-title').html('Ningún archivo cargado');
}
$('.image-upload-wrap').bind('dragover', function() {
    $('.image-upload-wrap').addClass('image-dropping');
});
$('.image-upload-wrap').bind('dragleave', function() {
    $('.image-upload-wrap').removeClass('image-dropping');
});
</script>

@endsection
